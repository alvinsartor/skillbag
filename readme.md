# Skillbag

SkillBag is the Wikipedia of skills: a 360 degrees user-based platform.

Here anyone can:

* **Define skills** by adding descriptions, resources and links. In this way anyone interested can get to learn them
* **Track skills** by marking each skill as "learned" or "want to learn". In this way users can easily monitor their own personal growth
* **Setup a team** and invite members. By doing so it will be possible to analyse the skills of all participants, spot strong suits and weak spots, plan for future team additions
* **See the trends**, which skills are needed and which ones are superfluous or obsolete


Register in a second and you're ready to try it out!

![registration](assets/s1.png)


Once inside, you can explore it by category or using the search function:

![search](assets/s2.png)

![categories](assets/sz.gif)


Don't forget to check out the teams functionality:

![team1](assets/s4.png)

![team2](assets/s5.png)
