

from django.db import models
from nexus.models.abstract import TimeStampMixin
from categories.models.category import Category
from nexus.models.abstract import DescriptableMixin

class Skill(TimeStampMixin, DescriptableMixin):
    name = models.CharField("Name", max_length=50, blank=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    superskills = models.ManyToManyField("self", blank=True, symmetrical=False, related_name="lesser_skills")   

    def __str__(self):
        return self.name
