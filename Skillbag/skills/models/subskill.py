from datetime import timedelta

from django.db import models
from django.utils import timezone
from django.db.models import Case, When, Value, F
from django.utils.translation import gettext_lazy as _

from skills.models.skill import Skill
from nexus.models.abstract import TimeStampMixin
from nexus.models.abstract import DescriptableMixin

class SkillLevels(models.TextChoices):
    HOBBYIST = '0', _('Hobbyist')
    BEGINNER = '1', _('Beginner')
    EXPERIENCED = '2', _('Experienced')
    PRO = '3', _('Pro')
    MASTER = '4', _('Master')


class Subskill(TimeStampMixin, DescriptableMixin): 
    name = models.CharField(_("Name"), max_length=50, blank=False)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    level = models.CharField(_("Level"), max_length=1, choices=SkillLevels.choices, default=SkillLevels.HOBBYIST, blank=False)

    def __str__(self):
        return self.name