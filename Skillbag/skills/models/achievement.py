
from django.db import models
from skills.models.subskill import Subskill
from nexus.models.abstract import TimeStampMixin
from accounts.models import User
from django.utils.translation import gettext_lazy as _

class AchievementType(models.TextChoices):
    PLANNED = 'planned', _('Planned')
    ACHIEVED = 'achieved', _('Achieved')    

class Achievement(TimeStampMixin):     
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    subskill = models.ForeignKey(Subskill, on_delete=models.CASCADE)
    type = models.CharField(_('Type'), max_length=8, choices=AchievementType.choices, blank=False)

    class Meta:
        unique_together = ['user', 'subskill']