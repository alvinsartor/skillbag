
from django.db import models
from skills.models.subskill import Subskill
from accounts.models import User
from django.utils.translation import gettext_lazy as _

class Resource(models.Model):
    subskill = models.ForeignKey(Subskill, on_delete=models.CASCADE)
    adder = models.ForeignKey(User, on_delete=models.CASCADE)
    link = models.CharField(_("Resource Link"), max_length=200, blank=False, null=False)