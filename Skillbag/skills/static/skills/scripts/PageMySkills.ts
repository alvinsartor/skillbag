class PageMySkills extends PageSkills {

    /** @inheritdoc */
    protected get url(): string { return "/skills/my-skills/"; }

    /** @inheritdoc  */
    public async initialize(...args: any[]): Promise<void> {
        await this.loadPage();
    }
}