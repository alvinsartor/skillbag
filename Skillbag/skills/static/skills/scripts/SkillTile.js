"use strict";
class SkillTile {
    constructor(pk) {
        this._subskills = {};
        this._skillEditor = null;
        this._subskillAdder = null;
        this.pk = pk;
        this._tile = document.getElementById(`skill-tile-${pk}`);
        this.name = this._tile.querySelector(".tile-skill__title").querySelector("span").textContent;
        this._isFavorite = false;
        Array.from(this._tile.querySelectorAll('div[name="subskill"]'))
            .map(tile => tile)
            .forEach(tile => {
            const subskill = new SubskillTile(tile);
            this._subskills[subskill.pk] = subskill;
        });
        this.bindActionButton();
    }
    bindActionButton() {
        const actionButton = this._tile.querySelector(".tile_skill__action-button");
        if (!actionButton)
            return;
        actionButton.onclick = (e) => {
            e.stopPropagation();
            SkillsActionsLoader.instance().activate(this, actionButton);
        };
    }
    /** Opens the editor to edit the current skill. */
    get favorite() { return this._isFavorite; }
    set favorite(value) {
        this._isFavorite = value;
        alert("I am asking...");
    }
    /** Opens the editor to edit the current skill. */
    openSkillEditor() {
        this._skillEditor = new SkillEditor(this.pk);
        this._skillEditor.loadEditorForm();
    }
    /** Opens the editor to add a new subskill. */
    openSubskillAdder() {
        this._subskillAdder = new SubskillAdder(this.pk);
        this._subskillAdder.loadEditorForm();
    }
    subskillMatchesQuery(key, query) {
        return this._subskills[key].name.toLowerCase().includes(query);
    }
    toggleVisibilityIfContains(query) {
        const matchesQuery = this.name.toLowerCase().includes(query)
            || Object.keys(this._subskills).some(key => this.subskillMatchesQuery(key, query));
        this._tile.style.display = matchesQuery ? "block" : "none";
    }
}
