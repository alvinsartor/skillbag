class PageCategorySkills extends PageSkills {

    private _categoryPk: string = "-1";
    private _categoryEditor: CategoryEditor | null = null;
    private _skillAdder: SkillAdder | null = null;

    /** @inheritdoc */
    protected get url(): string { return `/categories/${this._categoryPk}/`; }

    /** @inheritdoc  */
    public async initialize(...args: any[]): Promise<void> {
        [this._categoryPk] = args;
        await this.loadPage();
    }

    /** @inheritdoc  */
    protected async initializeScripts(): Promise<void> {
        await super.initializeScripts();
        this.adjustDescriptionBoxDependingOnContent();
        this.addActionsListeners();
    }

    private adjustDescriptionBoxDependingOnContent(): void {
        const descriptionTile = document.getElementById("description-tile") as HTMLDivElement;

        const descriptionPanel = descriptionTile.querySelector(".description-displayer__text") as HTMLElement;
        const padding = descriptionPanel.innerText.length ? "50px 25px" : "10px 25px";
        descriptionTile.style.padding = padding;
    }

    /** @inheritdoc */
    public addActionsListeners(): void {
        (document.getElementById("edit-action") as HTMLElement).onclick = () =>
        {
            this._categoryEditor = new CategoryEditor(this._categoryPk);
            this._categoryEditor!.loadEditorForm();
        };

        (document.getElementById("add-new-action") as HTMLElement).onclick = () =>
        {
            this._skillAdder = new SkillAdder(this._categoryPk);
            this._skillAdder!.loadEditorForm();
        };
    }
}