class SkillTile {

    public readonly pk: string;
    public readonly name: string;
    private readonly _tile: HTMLElement;
    private readonly _subskills: { [id: string]: SubskillTile; } = {}
    private _isFavorite: boolean;

    private _skillEditor: SkillEditor | null = null;
    private _subskillAdder: SubskillAdder | null = null;

    public constructor(pk: string) {
        this.pk = pk;
        this._tile = document.getElementById(`skill-tile-${pk}`) as HTMLElement;
        this.name = this._tile.querySelector(".tile-skill__title")!.querySelector("span")!.textContent!;
        this._isFavorite = false;
        Array.from(this._tile.querySelectorAll('div[name="subskill"]'))
            .map(tile => tile as HTMLElement)
            .forEach(tile =>
            {
                const subskill = new SubskillTile(tile);
                this._subskills[subskill.pk] = subskill;
            });

        this.bindActionButton();
    }

    private bindActionButton(): void {
        const actionButton = this._tile.querySelector(".tile_skill__action-button") as HTMLElement;
        if (!actionButton) return;
        actionButton.onclick = (e) =>
        {
            e.stopPropagation();
            SkillsActionsLoader.instance().activate(this, actionButton);
        };
    }

    /** Opens the editor to edit the current skill. */
    public get favorite(): boolean { return this._isFavorite; }
    public set favorite(value: boolean) {
        this._isFavorite = value;
        alert("I am asking...");
    }

    /** Opens the editor to edit the current skill. */
    public openSkillEditor(): void {
        this._skillEditor = new SkillEditor(this.pk);
        this._skillEditor!.loadEditorForm();
    }

    /** Opens the editor to add a new subskill. */
    public openSubskillAdder(): void {
        this._subskillAdder = new SubskillAdder(this.pk);
        this._subskillAdder!.loadEditorForm();
    }

    private subskillMatchesQuery(key: string, query: string): any {
        return this._subskills[key].name.toLowerCase().includes(query);
    }

    public toggleVisibilityIfContains(query: string): void {
        const matchesQuery = this.name.toLowerCase().includes(query)
                || Object.keys(this._subskills).some(key => this.subskillMatchesQuery(key, query));

        this._tile.style.display = matchesQuery ? "block" : "none";
    }
}