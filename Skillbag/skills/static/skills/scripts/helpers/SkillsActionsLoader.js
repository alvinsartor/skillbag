"use strict";
class SkillsActionsLoader {
    constructor() {
        this._actionMenu = null;
        this._iconsHaveBeenFetched = false;
        this._editSkillIcon = null;
        this._addSubskillIcon = null;
        this._favoriteSkillIcon = null;
        this._unFavoriteSkillIcon = null;
        document.addEventListener("click", () => SkillsActionsLoader.instance().destroyCurrentMenu());
    }
    static instance() {
        if (!this.inst)
            this.inst = new SkillsActionsLoader();
        return this.inst;
    }
    activate(skill, button) {
        this.destroyCurrentMenu();
        this.fetchIconsFromHtml();
        this.createMenu(skill, button);
    }
    /** Removes the currently active action-menu. */
    destroyCurrentMenu() {
        if (!this._actionMenu)
            return;
        this._actionMenu.remove();
        this._actionMenu = null;
    }
    fetchIconsFromHtml() {
        if (this._iconsHaveBeenFetched)
            return;
        const icons = Array
            .from(document.querySelectorAll(".js-skill-action-icon"))
            .map(skillActionIcons => skillActionIcons);
        this._editSkillIcon = icons.filter(icon => icon.dataset.action === "edit")[0].dataset.icon || null;
        this._addSubskillIcon = icons.filter(icon => icon.dataset.action === "add")[0].dataset.icon || null;
        this._favoriteSkillIcon = icons.filter(icon => icon.dataset.action === "fav")[0].dataset.icon || null;
        this._unFavoriteSkillIcon = icons.filter(icon => icon.dataset.action === "unFav")[0].dataset.icon || null;
        this._iconsHaveBeenFetched = true;
    }
    createMenu(skill, menuContainer) {
        if (this._actionMenu)
            throw Error("Trying to recreate already-initialized action menu.");
        this._actionMenu = document.createElement("div");
        this._actionMenu.classList.add("skill-actions__menu");
        menuContainer.appendChild(this._actionMenu);
        this._actionMenu.appendChild(this.createEntry(this._editSkillIcon, `Edit '${skill.name}'`, () => skill.openSkillEditor()));
        this._actionMenu.appendChild(this.createEntry(this._addSubskillIcon, `Add new subskill`, () => skill.openSubskillAdder()));
        //const favoriteIcon = skill.favorite ? this._favoriteSkillIcon! : this._unFavoriteSkillIcon!;
        //const favoriteText = skill.favorite ? `Add '${skill.name}' to favorites` : `Remove '${skill.name}' from favorites`;
        //this._actionMenu!.appendChild(this.createEntry(favoriteIcon,
        //    favoriteText,
        //    () => skill.favorite = !skill.favorite));
    }
    createEntry(icon, text, action) {
        const entry = document.createElement("div");
        entry.classList.add("skill-actions__menu-entry");
        entry.innerHTML = `${icon}\n<span>${text}</span>`;
        entry.onclick = (e) => {
            e.stopPropagation();
            action();
            SkillsActionsLoader.instance().destroyCurrentMenu();
        };
        return entry;
    }
}
SkillsActionsLoader.inst = null;
