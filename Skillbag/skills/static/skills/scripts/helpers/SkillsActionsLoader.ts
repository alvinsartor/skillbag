

class SkillsActionsLoader {

    private static inst: SkillsActionsLoader | null = null;

    public static instance(): SkillsActionsLoader {
        if (!this.inst) this.inst = new SkillsActionsLoader();
        return this.inst;
    }

    private constructor() {
        document.addEventListener("click", () => SkillsActionsLoader.instance().destroyCurrentMenu());
    }

    private _actionMenu: HTMLDivElement | null = null;
    private _iconsHaveBeenFetched: boolean = false;
    private _editSkillIcon: string | null = null;
    private _addSubskillIcon: string | null = null;
    private _favoriteSkillIcon: string | null = null;
    private _unFavoriteSkillIcon: string | null = null;

    public activate(skill: SkillTile, button: HTMLElement)
    {
        this.destroyCurrentMenu();
        this.fetchIconsFromHtml();
        this.createMenu(skill, button);
    }

    /** Removes the currently active action-menu. */
    public destroyCurrentMenu(): void {
        if (!this._actionMenu) return;

        this._actionMenu.remove();
        this._actionMenu = null;
    }

    private fetchIconsFromHtml(): void {
        if (this._iconsHaveBeenFetched) return;

        const icons = Array
            .from(document.querySelectorAll(".js-skill-action-icon"))
            .map(skillActionIcons => skillActionIcons as HTMLElement);

        this._editSkillIcon = icons.filter(icon => icon.dataset.action === "edit")[0].dataset.icon || null;
        this._addSubskillIcon = icons.filter(icon => icon.dataset.action === "add")[0].dataset.icon || null;
        this._favoriteSkillIcon = icons.filter(icon => icon.dataset.action === "fav")[0].dataset.icon || null;
        this._unFavoriteSkillIcon = icons.filter(icon => icon.dataset.action === "unFav")[0].dataset.icon || null;

        this._iconsHaveBeenFetched = true;
    }

    private createMenu(skill: SkillTile, menuContainer: HTMLElement): void {
        if (this._actionMenu) throw Error("Trying to recreate already-initialized action menu.");

        this._actionMenu = document.createElement("div");
        this._actionMenu.classList.add("skill-actions__menu");
        menuContainer.appendChild(this._actionMenu);

        this._actionMenu!.appendChild(this.createEntry(this._editSkillIcon!,
            `Edit '${skill.name}'`,
            () => skill.openSkillEditor()));
        this._actionMenu!.appendChild(this.createEntry(this._addSubskillIcon!,
            `Add new subskill`,
            () => skill.openSubskillAdder()));

        //const favoriteIcon = skill.favorite ? this._favoriteSkillIcon! : this._unFavoriteSkillIcon!;
        //const favoriteText = skill.favorite ? `Add '${skill.name}' to favorites` : `Remove '${skill.name}' from favorites`;
        //this._actionMenu!.appendChild(this.createEntry(favoriteIcon,
        //    favoriteText,
        //    () => skill.favorite = !skill.favorite));
    }

    private createEntry(icon: string, text: string, action: () => void): HTMLDivElement {
        const entry = document.createElement("div");
        entry.classList.add("skill-actions__menu-entry");
        entry.innerHTML = `${icon}\n<span>${text}</span>`;
        entry.onclick = (e) => {
            e.stopPropagation();
            action();
            SkillsActionsLoader.instance().destroyCurrentMenu();
        }
        return entry;
    }
}