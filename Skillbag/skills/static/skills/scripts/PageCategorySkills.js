"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageCategorySkills extends PageSkills {
    constructor() {
        super(...arguments);
        this._categoryPk = "-1";
        this._categoryEditor = null;
        this._skillAdder = null;
    }
    /** @inheritdoc */
    get url() { return `/categories/${this._categoryPk}/`; }
    /** @inheritdoc  */
    initialize(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            [this._categoryPk] = args;
            yield this.loadPage();
        });
    }
    /** @inheritdoc  */
    initializeScripts() {
        const _super = Object.create(null, {
            initializeScripts: { get: () => super.initializeScripts }
        });
        return __awaiter(this, void 0, void 0, function* () {
            yield _super.initializeScripts.call(this);
            this.adjustDescriptionBoxDependingOnContent();
            this.addActionsListeners();
        });
    }
    adjustDescriptionBoxDependingOnContent() {
        const descriptionTile = document.getElementById("description-tile");
        const descriptionPanel = descriptionTile.querySelector(".description-displayer__text");
        const padding = descriptionPanel.innerText.length ? "50px 25px" : "10px 25px";
        descriptionTile.style.padding = padding;
    }
    /** @inheritdoc */
    addActionsListeners() {
        document.getElementById("edit-action").onclick = () => {
            this._categoryEditor = new CategoryEditor(this._categoryPk);
            this._categoryEditor.loadEditorForm();
        };
        document.getElementById("add-new-action").onclick = () => {
            this._skillAdder = new SkillAdder(this._categoryPk);
            this._skillAdder.loadEditorForm();
        };
    }
}
