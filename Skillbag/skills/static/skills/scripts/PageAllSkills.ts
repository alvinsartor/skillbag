class PageAllSkills extends PageSkills {

    /** @inheritdoc */
    protected get url(): string { return "/skills/"; }

    /** @inheritdoc  */
    public async initialize(...args: any[]): Promise<void>
    {
        await this.loadPage();
    }
}