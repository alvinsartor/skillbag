"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageSkills extends Page {
    constructor() {
        super(...arguments);
        /** The skills loaded by the page. */
        this.initializedSkills = {};
        /** The masonry script, if this is loaded. */
        this.masonry = null;
    }
    /** @inheritdoc  */
    loadHtml() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.get(this.url);
        });
    }
    /** @inheritdoc  */
    initializeScripts() {
        const skillsListContainer = document.querySelector('[name="loaded-skills"]');
        const skillsList = JSON.parse(skillsListContainer.innerText);
        skillsList.forEach((pk) => {
            if (!this.initializedSkills[pk]) {
                this.initializedSkills[pk] = new SkillTile(pk);
            }
        });
        // on mobile we do not load masonry as the tiles are just displayed in column.
        if (window.matchMedia("(min-width: 640px)").matches) {
            this.masonry = new Masonry(".skills-container", {
                itemSelector: ".tile-skill",
                fitWidth: true,
                horizontalOrder: true,
                transitionDuration: "0.2s",
            });
        }
        this.initializePersonalizedSkillsActions();
        return Promise.resolve();
    }
    /**
     * Function used to assign actions to the action entries of the action menu.
     * Override if necessary.
     */
    initializePersonalizedSkillsActions() { }
    /** @inheritdoc  */
    internalDispose() {
        Object.keys(this.initializedSkills).forEach(k => delete this.initializedSkills[k]);
        return Promise.resolve();
    }
}
