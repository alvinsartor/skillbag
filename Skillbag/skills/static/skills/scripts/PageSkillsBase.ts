abstract class PageSkills extends Page {

    /** The url used to load the skills. */
    protected abstract get url(): string;
    
    /** The skills loaded by the page. */
    protected readonly initializedSkills: { [pk: string]: SkillTile } = {};

    /** The masonry script, if this is loaded. */
    protected masonry: Masonry | null = null;

    /** @inheritdoc  */
    protected async loadHtml(): Promise<string> {
        return await $.get(this.url);
    }

    /** @inheritdoc  */
    protected initializeScripts(): Promise<void> {
        const skillsListContainer = document.querySelector('[name="loaded-skills"]') as HTMLElement;
        const skillsList = JSON.parse(skillsListContainer.innerText) as string[];
        skillsList.forEach((pk: string) => {
            if (!this.initializedSkills[pk]) {
                this.initializedSkills[pk] = new SkillTile(pk);
            }
        });

        // on mobile we do not load masonry as the tiles are just displayed in column.
        if (window.matchMedia("(min-width: 640px)").matches) {
            this.masonry = new Masonry(".skills-container",
                {
                    itemSelector: ".tile-skill",
                    fitWidth: true,
                    horizontalOrder: true,
                    transitionDuration: "0.2s",
                });
        }

        this.initializePersonalizedSkillsActions();
        return Promise.resolve();
    }

    /**
     * Function used to assign actions to the action entries of the action menu.
     * Override if necessary.
     */
    protected initializePersonalizedSkillsActions(): void { }

    /** @inheritdoc  */
    public internalDispose(): Promise<void> {
        Object.keys(this.initializedSkills).forEach(k => delete this.initializedSkills[k]);
        return Promise.resolve();
    }
}