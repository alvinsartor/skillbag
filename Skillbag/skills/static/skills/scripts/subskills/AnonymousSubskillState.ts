class AnonymousSubskillState extends BaseSubskillState {

    /** @inheritdoc */
    public get tileClass(): string { return ""; }

    /** @inheritdoc */
    public get mainButtonClass(): string { return "button--green"; }

    /** @inheritdoc */
    public get stateAfterSuccessfulMainButton(): BaseSubskillState { return new AnonymousSubskillState(); }

    /** @inheritdoc */
    public async actionOnMainButton(subskillPk: string): Promise<boolean> {
        location.href = "/accounts/login";
        return false;
    }

    /** @inheritdoc */
    public get planButtonClass(): string { return "button--yellow"; }

    /** @inheritdoc */
    public get stateAfterSuccessfulPlanButton(): BaseSubskillState { return new AnonymousSubskillState(); }

    /** @inheritdoc */
    public async actionOnPlanButton(subskillPk: string): Promise<boolean> {
        location.href = "/accounts/login";
        return false;
    }
}