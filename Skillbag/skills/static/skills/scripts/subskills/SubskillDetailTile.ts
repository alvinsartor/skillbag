
/**
 * Class managing a tile with details and actions relative to a subskill.
 * This object will be re-used across all subskills.
 */
class SubskillDetailTile {
    private static inst: SubskillDetailTile;

    private constructor() {
        document.getElementById("body-content")!.onclick = () => SubskillDetailTile.instance().detach();
    }

    /** The singleton instance of SubskillDetailTile. */
    public static instance() {
        if (!this.inst) this.inst = new SubskillDetailTile();
        return this.inst;
    }

    private _panel: HTMLDivElement | null = null;
    private _subskillEditor: SubskillEditor | null = null;
    private _resourcesEditor: ResourcesEditor | null = null;

    /**
     * Given a subskill tile, it retrieves the data to show, adds the panel on the DOM and display the subskill details.
     * @param subskill the subskill to attach to.
     */
    public async attach(subskill: SubskillTile): Promise<void> {
        this.detach();
        await this.buildPanel(subskill.pk, subskill.tile);
        this.attachEditors(subskill.pk);
        this.adaptTileToState(subskill);
    }

    /** Discretely removes any sign of its existence. */
    public detach(): void {
        if (!this._panel) return;

        this._panel.remove();
        this._panel = null;
    }

    private async buildPanel(subskillPk: string, subskillTile: HTMLElement): Promise<void> {
        if (this._panel) throw Error("You forgot to detach the detail panel!");

        const rawDetailTile = (await $.get(`/skills/subskills/${subskillPk}/`)) as string;
        this._panel = document.createElement("div");
        this._panel!.classList.add("tile-subskill-detail");
        this._panel.innerHTML = rawDetailTile;

        subskillTile.appendChild(this._panel!);
        this._panel.onclick = (e) => { e.stopPropagation(); };
        this._panel.onblur = () => { this.detach(); };
    }

    private attachEditors(subskillPk: string): void {
        if (!this._panel) throw Error("The detail panel has not been initialized yet!");

        const editButton = this._panel.querySelector("div[name='edit_button']") as HTMLDivElement;
        const resourcesButton = this._panel.querySelector("div[name='resources_button']") as HTMLDivElement;
        if (!editButton || ! resourcesButton) return;

        editButton.setAttribute("class", `${editButton.classList.item(0)} button--grey`);
        editButton.onclick = (e) => {
            e.stopPropagation();
            this._subskillEditor = new SubskillEditor(subskillPk);
            this._subskillEditor!.loadEditorForm();
        };

        resourcesButton.setAttribute("class", `${resourcesButton.classList.item(0)} button--grey`);
        resourcesButton.onclick = (e) => {
            e.stopPropagation();
            this._resourcesEditor = new ResourcesEditor(subskillPk);
            this._resourcesEditor!.loadEditorForm();
        };
    }

    private adaptTileToState(subskill: SubskillTile): void {
        if (!this._panel) throw Error("The detail panel has not been initialized yet!");
        const state = subskill.state;

        this._panel.setAttribute("class", `${this._panel.classList.item(0)} ${state.tileClass}`);

        const achieveButton = this._panel.querySelector("div[name='achieved_button']") as HTMLDivElement;
        achieveButton.setAttribute("class", `${achieveButton.classList.item(0)} ${state.mainButtonClass}`);
        this.attachButtonAction(achieveButton,
            state.actionOnMainButton,
            () => state.stateAfterSuccessfulMainButton,
            subskill);

        const planButton = this._panel.querySelector("div[name='plan_button']") as HTMLDivElement;
        planButton.setAttribute("class", `${planButton.classList.item(0)} ${state.planButtonClass}`);
        this.attachButtonAction(planButton,
            state.actionOnPlanButton,
            () => state.stateAfterSuccessfulPlanButton,
            subskill);
    }

    private attachButtonAction(
        button: HTMLElement,
        action: (subskillPk: string) => Promise<boolean>,
        newState: () => BaseSubskillState,
        subskill: SubskillTile): void {
        button.onclick = async (e: UIEvent) => {
            e.stopPropagation();
            const currentState = subskill.state;
            subskill.state = newState();
            subskill.adaptTileToState();
            SubskillDetailTile.instance().detach();
            if (!await action(subskill.pk)) {
                //something went wrong. Revert changes.
                subskill.state = currentState;
                subskill.adaptTileToState();
            }
        };
    }
}