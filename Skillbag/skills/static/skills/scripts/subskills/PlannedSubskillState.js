"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PlannedSubskillState extends BaseSubskillState {
    /** @inheritdoc */
    get tileClass() { return "tile-subskill--planned"; }
    /** @inheritdoc */
    get mainButtonClass() { return "button--green"; }
    /** @inheritdoc */
    get stateAfterSuccessfulMainButton() { return new AchievedSubskillState(); }
    /** @inheritdoc */
    actionOnMainButton(subskillPk) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield BaseSubskillState.addNewAchievement(subskillPk);
        });
    }
    /** @inheritdoc */
    get planButtonClass() { return "button--grey"; }
    /** @inheritdoc */
    get stateAfterSuccessfulPlanButton() { return new EmptySubskillState(); }
    /** @inheritdoc */
    actionOnPlanButton(subskillPk) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield BaseSubskillState.deleteAchievement(subskillPk);
        });
    }
}
