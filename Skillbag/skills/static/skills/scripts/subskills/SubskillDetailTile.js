"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
/**
 * Class managing a tile with details and actions relative to a subskill.
 * This object will be re-used across all subskills.
 */
class SubskillDetailTile {
    constructor() {
        this._panel = null;
        this._subskillEditor = null;
        this._resourcesEditor = null;
        document.getElementById("body-content").onclick = () => SubskillDetailTile.instance().detach();
    }
    /** The singleton instance of SubskillDetailTile. */
    static instance() {
        if (!this.inst)
            this.inst = new SubskillDetailTile();
        return this.inst;
    }
    /**
     * Given a subskill tile, it retrieves the data to show, adds the panel on the DOM and display the subskill details.
     * @param subskill the subskill to attach to.
     */
    attach(subskill) {
        return __awaiter(this, void 0, void 0, function* () {
            this.detach();
            yield this.buildPanel(subskill.pk, subskill.tile);
            this.attachEditors(subskill.pk);
            this.adaptTileToState(subskill);
        });
    }
    /** Discretely removes any sign of its existence. */
    detach() {
        if (!this._panel)
            return;
        this._panel.remove();
        this._panel = null;
    }
    buildPanel(subskillPk, subskillTile) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this._panel)
                throw Error("You forgot to detach the detail panel!");
            const rawDetailTile = (yield $.get(`/skills/subskills/${subskillPk}/`));
            this._panel = document.createElement("div");
            this._panel.classList.add("tile-subskill-detail");
            this._panel.innerHTML = rawDetailTile;
            subskillTile.appendChild(this._panel);
            this._panel.onclick = (e) => { e.stopPropagation(); };
            this._panel.onblur = () => { this.detach(); };
        });
    }
    attachEditors(subskillPk) {
        if (!this._panel)
            throw Error("The detail panel has not been initialized yet!");
        const editButton = this._panel.querySelector("div[name='edit_button']");
        const resourcesButton = this._panel.querySelector("div[name='resources_button']");
        if (!editButton || !resourcesButton)
            return;
        editButton.setAttribute("class", `${editButton.classList.item(0)} button--grey`);
        editButton.onclick = (e) => {
            e.stopPropagation();
            this._subskillEditor = new SubskillEditor(subskillPk);
            this._subskillEditor.loadEditorForm();
        };
        resourcesButton.setAttribute("class", `${resourcesButton.classList.item(0)} button--grey`);
        resourcesButton.onclick = (e) => {
            e.stopPropagation();
            this._resourcesEditor = new ResourcesEditor(subskillPk);
            this._resourcesEditor.loadEditorForm();
        };
    }
    adaptTileToState(subskill) {
        if (!this._panel)
            throw Error("The detail panel has not been initialized yet!");
        const state = subskill.state;
        this._panel.setAttribute("class", `${this._panel.classList.item(0)} ${state.tileClass}`);
        const achieveButton = this._panel.querySelector("div[name='achieved_button']");
        achieveButton.setAttribute("class", `${achieveButton.classList.item(0)} ${state.mainButtonClass}`);
        this.attachButtonAction(achieveButton, state.actionOnMainButton, () => state.stateAfterSuccessfulMainButton, subskill);
        const planButton = this._panel.querySelector("div[name='plan_button']");
        planButton.setAttribute("class", `${planButton.classList.item(0)} ${state.planButtonClass}`);
        this.attachButtonAction(planButton, state.actionOnPlanButton, () => state.stateAfterSuccessfulPlanButton, subskill);
    }
    attachButtonAction(button, action, newState, subskill) {
        button.onclick = (e) => __awaiter(this, void 0, void 0, function* () {
            e.stopPropagation();
            const currentState = subskill.state;
            subskill.state = newState();
            subskill.adaptTileToState();
            SubskillDetailTile.instance().detach();
            if (!(yield action(subskill.pk))) {
                //something went wrong. Revert changes.
                subskill.state = currentState;
                subskill.adaptTileToState();
            }
        });
    }
}
