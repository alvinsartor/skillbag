"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class BaseSubskillState {
    static addNewAchievement(pk, type = "achieved") {
        return __awaiter(this, void 0, void 0, function* () {
            const data = Object.assign(Object.assign({}, Poster.getBasicPostData()), { subskill: pk, type: type });
            try {
                yield $.post("/skills/achievements/add/", data);
                return true;
            }
            catch (err) {
                MessageManager.error("Something went wrong while adding this achievement.");
                console.log(`location: addAchievement, status: ${err.status}, text: ${err.statusText}`);
                return false;
            }
        });
    }
    static deleteAchievement(pk) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = Object.assign(Object.assign({}, Poster.getBasicPostData()), { subskill: pk });
            try {
                yield $.post("/skills/achievements/remove/", data);
                return true;
            }
            catch (err) {
                MessageManager.error("Something went wrong while removing this achievement.");
                console.log(`location: addAchievement, status: ${err.status}, text: ${err.statusText}`);
                return false;
            }
        });
    }
}
