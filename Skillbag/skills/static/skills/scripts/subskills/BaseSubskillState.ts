abstract class BaseSubskillState {

    /** The partial class to be assigned to the subskill tile. */
    public abstract get tileClass(): string;

    /** The partial class to be assigned to the main subskill button. */
    public abstract get mainButtonClass(): string;

    /** The state in case of having successfully executed the main subskill button action. */
    public abstract get stateAfterSuccessfulMainButton(): BaseSubskillState;

    /** The action associated to the main subskill tile button. */
    public abstract async actionOnMainButton(subskillPk: string): Promise<boolean>;

    /** The partial class to be assigned to the plan-button. */
    public abstract get planButtonClass(): string;

    /** The state in case of having successfully executed the plan-button action. */
    public abstract get stateAfterSuccessfulPlanButton(): BaseSubskillState;

    /** The action associated to the plan-button. */
    public abstract async actionOnPlanButton(subskillPk: string): Promise<boolean>;


    protected static async addNewAchievement(pk: string, type: string ="achieved"): Promise<boolean> {
        const data = {
            ...Poster.getBasicPostData(),
            subskill: pk,
            type: type,
        };

        try {
            await $.post("/skills/achievements/add/", data);
            return true;
        } catch (err) {
            MessageManager.error("Something went wrong while adding this achievement.");
            console.log(`location: addAchievement, status: ${err.status}, text: ${err.statusText}`);
            return false;
        }
    }

    protected static async deleteAchievement(pk: string): Promise<boolean> {
        const data = {
            ...Poster.getBasicPostData(),
            subskill: pk,
        };

        try {
            await $.post("/skills/achievements/remove/", data);
            return true;
        } catch (err) {
            MessageManager.error("Something went wrong while removing this achievement.");
            console.log(`location: addAchievement, status: ${err.status}, text: ${err.statusText}`);
            return false;
        }
    }
}