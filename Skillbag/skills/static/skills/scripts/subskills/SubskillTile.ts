class SubskillTile {

    public readonly pk: string;
    public readonly name: string; 
    public state: BaseSubskillState;

    public readonly tile: HTMLElement;
    private readonly _achievementButton: HTMLElement;
    
    public constructor(tile: HTMLElement) {
        this.tile = tile;
        this.pk = (tile.querySelector('[name="subskill_id"]') as HTMLInputElement).value;
        this.name = (tile.querySelector('[name="subskill_name"]') as HTMLDivElement).innerText;
        this._achievementButton = this.tile.querySelector("div[name='achievement_button']") as HTMLElement;

        const achievementType = (tile.querySelector('[name="subskill_achievement_type"]') as HTMLInputElement).value;
        this.state = this.getStateFromTile(achievementType);
        this.adaptTileToState();

        this.tile.onclick = (e) => {
            e.stopPropagation();
            SubskillDetailTile.instance().attach(this);
        };
    }

    private getStateFromTile(achievementType: string): BaseSubskillState {
        switch (achievementType) {
        case "anonymous":
            return new AnonymousSubskillState();
        case "none":
            return new EmptySubskillState();
        case "achieved":
            return new AchievedSubskillState();
        case "planned":
            return new PlannedSubskillState();
        default:
            throw new Error(`could not match the achievement type of the subskill tile: ${this.pk}`);
        }
    }

    public adaptTileToState(): void {
        this.tile.setAttribute("class", `${this.tile.classList.item(0)} ${this.state.tileClass}`);

        if (!this._achievementButton) return;
        this._achievementButton.setAttribute("class", `${this._achievementButton.classList.item(0)} ${this.state.mainButtonClass}`);
        this._achievementButton.onclick = async (e: UIEvent) => {
            e.stopPropagation();
            const oldState = this.state;
            this.state = this.state.stateAfterSuccessfulMainButton;
            this.adaptTileToState();

            if (!await oldState.actionOnMainButton(this.pk)) {
                //something went wrong. Revert changes.
                this.state = oldState;
                this.adaptTileToState();
            }
        };
    }

}