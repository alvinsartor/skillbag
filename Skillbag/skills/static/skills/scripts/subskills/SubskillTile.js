"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class SubskillTile {
    constructor(tile) {
        this.tile = tile;
        this.pk = tile.querySelector('[name="subskill_id"]').value;
        this.name = tile.querySelector('[name="subskill_name"]').innerText;
        this._achievementButton = this.tile.querySelector("div[name='achievement_button']");
        const achievementType = tile.querySelector('[name="subskill_achievement_type"]').value;
        this.state = this.getStateFromTile(achievementType);
        this.adaptTileToState();
        this.tile.onclick = (e) => {
            e.stopPropagation();
            SubskillDetailTile.instance().attach(this);
        };
    }
    getStateFromTile(achievementType) {
        switch (achievementType) {
            case "anonymous":
                return new AnonymousSubskillState();
            case "none":
                return new EmptySubskillState();
            case "achieved":
                return new AchievedSubskillState();
            case "planned":
                return new PlannedSubskillState();
            default:
                throw new Error(`could not match the achievement type of the subskill tile: ${this.pk}`);
        }
    }
    adaptTileToState() {
        this.tile.setAttribute("class", `${this.tile.classList.item(0)} ${this.state.tileClass}`);
        if (!this._achievementButton)
            return;
        this._achievementButton.setAttribute("class", `${this._achievementButton.classList.item(0)} ${this.state.mainButtonClass}`);
        this._achievementButton.onclick = (e) => __awaiter(this, void 0, void 0, function* () {
            e.stopPropagation();
            const oldState = this.state;
            this.state = this.state.stateAfterSuccessfulMainButton;
            this.adaptTileToState();
            if (!(yield oldState.actionOnMainButton(this.pk))) {
                //something went wrong. Revert changes.
                this.state = oldState;
                this.adaptTileToState();
            }
        });
    }
}
