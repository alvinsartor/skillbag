class AchievedSubskillState extends BaseSubskillState {

    /** @inheritdoc */
    public get tileClass(): string { return "tile-subskill--achieved"; }

    /** @inheritdoc */
    public get mainButtonClass(): string { return "button--grey"; }

    /** @inheritdoc */
    public get stateAfterSuccessfulMainButton(): BaseSubskillState { return new EmptySubskillState(); }

    /** @inheritdoc */
    public async actionOnMainButton(subskillPk: string): Promise<boolean> {
        return await BaseSubskillState.deleteAchievement(subskillPk);
    }

    /** @inheritdoc */
    public get planButtonClass(): string { return "button--yellow"; }

    /** @inheritdoc */
    public get stateAfterSuccessfulPlanButton(): BaseSubskillState { return new PlannedSubskillState(); }

    /** @inheritdoc */
    public async actionOnPlanButton(subskillPk: string): Promise<boolean> {
        return await BaseSubskillState.addNewAchievement(subskillPk, "planned");
    }
}