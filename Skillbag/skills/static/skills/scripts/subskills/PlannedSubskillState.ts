class PlannedSubskillState extends BaseSubskillState {

    /** @inheritdoc */
    public get tileClass(): string { return "tile-subskill--planned"; }

    /** @inheritdoc */
    public get mainButtonClass(): string { return "button--green"; }

    /** @inheritdoc */
    public get stateAfterSuccessfulMainButton(): BaseSubskillState { return new AchievedSubskillState(); }

    /** @inheritdoc */
    public async actionOnMainButton(subskillPk: string): Promise<boolean> {
        return await BaseSubskillState.addNewAchievement(subskillPk);
    }

    /** @inheritdoc */
    public get planButtonClass(): string { return "button--grey"; }

    /** @inheritdoc */
    public get stateAfterSuccessfulPlanButton(): BaseSubskillState { return new EmptySubskillState(); }

    /** @inheritdoc */
    public async actionOnPlanButton(subskillPk: string): Promise<boolean> {
        return await BaseSubskillState.deleteAchievement(subskillPk);
    }
}