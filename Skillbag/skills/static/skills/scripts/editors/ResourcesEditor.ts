
class ResourcesEditor extends BaseEditor
{
    /** @inheritdoc */
    protected title: string = "Edit Resources";
    /** @inheritdoc */
    protected get formUrl() { return `/skills/subskills/${this._subskillPk}/editresources/`; }

    private readonly _subskillPk: string;

    public constructor(pk: string) {
        super();
        this._subskillPk = pk;
    }

    /** @inheritdoc  */
    protected setAdditionalListeners(form: HTMLFormElement): void {
        const formsetHandler = new FormSetHandler(form);
        (form.querySelector(".resource-formset__add-button") as HTMLElement).onclick = () => formsetHandler.addFormsetLine();
    }
}