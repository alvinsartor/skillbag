"use strict";
class SubskillAdder extends BaseEditor {
    constructor(pk) {
        super();
        /** @inheritdoc */
        this.title = "Add Subskill";
        this._skillPk = pk;
    }
    /** @inheritdoc */
    get formUrl() { return `/skills/${this._skillPk}/add/`; }
}
