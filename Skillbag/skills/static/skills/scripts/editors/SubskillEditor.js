"use strict";
class SubskillEditor extends BaseEditor {
    constructor(pk) {
        super();
        /** @inheritdoc */
        this.title = "Edit Subskill";
        this._subskillPk = pk;
    }
    /** @inheritdoc */
    get formUrl() { return `/skills/subskills/${this._subskillPk}/edit/`; }
}
