"use strict";
class SkillAdder extends BaseEditor {
    constructor(categoryPk) {
        super();
        /** @inheritdoc */
        this.title = "Add Skill";
        this._categoryPk = categoryPk;
    }
    /** @inheritdoc */
    get formUrl() { return `/categories/${this._categoryPk}/add/`; }
}
