"use strict";
class FormSetHandler {
    constructor(form) {
        this._form = form;
        this._formsetContainer = form.querySelector(".resources-formset");
        this._formStats = this.extractFormSetStats(form);
    }
    extractFormSetStats(form) {
        return {
            totalForms: Number(form.querySelector("[name='form-TOTAL_FORMS']").value),
            initialForms: Number(form.querySelector("[name='form-INITIAL_FORMS']").value),
            minForms: Number(form.querySelector("[name='form-MIN_NUM_FORMS']").value),
            maxForms: Number(form.querySelector("[name='form-MAX_NUM_FORMS']").value),
        };
    }
    writeFormsetStats() {
        this._form.querySelector("[name='form-TOTAL_FORMS']").value = this._formStats.totalForms.toString();
        this._form.querySelector("[name='form-INITIAL_FORMS']").value = this._formStats.initialForms.toString();
        this._form.querySelector("[name='form-MIN_NUM_FORMS']").value = this._formStats.minForms.toString();
        this._form.querySelector("[name='form-MAX_NUM_FORMS']").value = this._formStats.maxForms.toString();
    }
    addFormsetLine() {
        if (this._formStats.totalForms === this._formStats.maxForms)
            return;
        const index = this._formStats.totalForms;
        const lineContainer = document.createElement("div");
        lineContainer.classList.add("resource-formset__single-form");
        lineContainer.innerHTML = `
                <div class="editor-form__input-group editor-form__input-group--inline">
                    <input type="text" name="form-${index}-link" maxlength="200" class="editor-form__text-field" placeholder="Resource Link" id="id_form-${index}-link">
                </div>
                <input type="hidden" name="form-${index}-id" id="id_form-${index}-id">`;
        const addButton = this._formsetContainer.querySelector(".resource-formset__add-button");
        this._formsetContainer.insertBefore(lineContainer, addButton);
        this._formStats.totalForms++;
        this.writeFormsetStats();
    }
}
