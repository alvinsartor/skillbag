
class SkillEditor extends BaseEditor {

    /** @inheritdoc */
    protected title: string = "Edit Skill";

    private readonly _skillPk: string;

    /** @inheritdoc */
    protected get formUrl() { return `/skills/${this._skillPk}/edit/`; }

    public constructor(pk: string)
    {
        super();
        this._skillPk = pk;
    }
}