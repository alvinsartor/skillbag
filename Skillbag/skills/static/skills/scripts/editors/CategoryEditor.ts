
class CategoryEditor extends BaseEditor {

    /** @inheritdoc */
    protected title: string = "Edit Category";

    private readonly _categoryPk: string;

    /** @inheritdoc */
    protected get formUrl() { return `/categories/${this._categoryPk}/edit/`; }

    public constructor(categoryPk: string) {
        super();
        this._categoryPk = categoryPk;
    }
}