
class SubskillEditor extends BaseEditor {

    /** @inheritdoc */
    protected title: string = "Edit Subskill";

    private readonly _subskillPk: string;

    /** @inheritdoc */
    protected get formUrl() { return `/skills/subskills/${this._subskillPk}/edit/`; }

    public constructor(pk: string) {
        super();
        this._subskillPk = pk;
    }
}