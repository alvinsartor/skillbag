"use strict";
class CategoryEditor extends BaseEditor {
    constructor(categoryPk) {
        super();
        /** @inheritdoc */
        this.title = "Edit Category";
        this._categoryPk = categoryPk;
    }
    /** @inheritdoc */
    get formUrl() { return `/categories/${this._categoryPk}/edit/`; }
}
