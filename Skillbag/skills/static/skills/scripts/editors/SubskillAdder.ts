
class SubskillAdder extends BaseEditor {

    /** @inheritdoc */
    protected title: string = "Add Subskill";

    private readonly _skillPk: string;

    /** @inheritdoc */
    protected get formUrl() { return `/skills/${this._skillPk}/add/`; }

    public constructor(pk: string) {
        super();
        this._skillPk = pk;
    }
}