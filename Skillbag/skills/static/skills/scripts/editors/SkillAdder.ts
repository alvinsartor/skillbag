
class SkillAdder extends BaseEditor {

    /** @inheritdoc */
    protected title: string = "Add Skill";

    private readonly _categoryPk: string;

    /** @inheritdoc */
    protected get formUrl() { return `/categories/${this._categoryPk}/add/`; }

    public constructor(categoryPk: string) {
        super();
        this._categoryPk = categoryPk;
    }
}