"use strict";
class SkillEditor extends BaseEditor {
    constructor(pk) {
        super();
        /** @inheritdoc */
        this.title = "Edit Skill";
        this._skillPk = pk;
    }
    /** @inheritdoc */
    get formUrl() { return `/skills/${this._skillPk}/edit/`; }
}
