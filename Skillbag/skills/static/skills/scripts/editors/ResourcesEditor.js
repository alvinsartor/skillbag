"use strict";
class ResourcesEditor extends BaseEditor {
    constructor(pk) {
        super();
        /** @inheritdoc */
        this.title = "Edit Resources";
        this._subskillPk = pk;
    }
    /** @inheritdoc */
    get formUrl() { return `/skills/subskills/${this._subskillPk}/editresources/`; }
    /** @inheritdoc  */
    setAdditionalListeners(form) {
        const formsetHandler = new FormSetHandler(form);
        form.querySelector(".resource-formset__add-button").onclick = () => formsetHandler.addFormsetLine();
    }
}
