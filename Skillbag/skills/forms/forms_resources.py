
# Python
import re

# Django
from django import forms
from django.forms import ModelForm
from django.forms import BaseModelFormSet
from django.forms import modelformset_factory
from django.utils.translation import gettext as _
from django.core.validators import URLValidator

# Local
from skills.models import Resource

class ResourceForm(ModelForm):   

    class Meta:
        model = Resource
        fields = ['link']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # this allows to remove resources simply by deleting the link
        self.fields['link'].required = False 
        
        self.fields['link'].widget.attrs['class'] = "editor-form__text-field"
        self.fields['link'].widget.attrs['placeholder'] = _('Resource Link')
        self.fields['link'].widget.attrs['maxlength'] = "200"

    def clean_link(self):
        link = self.cleaned_data.get('link', None)

        # if empty, it is ok, it will be deleted on save
        if not link:
            return link
        
        # otherwise, we search a URL (and extract only the first, in case multiple were added)
        validator = URLValidator()
        validator(link)
        
        return link

    def save(self, editing_user, subskill):
        if not self.has_changed:
            return self.instance

        is_empty = self.cleaned_data.get('link', '').strip() == ''
        
        if is_empty:
            if self.instance.pk:
                self.instance.delete()
            return None

        if not self.instance.pk:
            self.instance.adder = editing_user
            self.instance.subskill = subskill
        
        return super().save()



class BaseResourceFormSet(BaseModelFormSet):
    
    def clean(self):
        """Checks that no two resources have the same link."""
        
        # Don't bother validating the formset unless each form is valid on its own
        if any(self.errors):
            return
        
        links = []
        for form in self.forms:            
            if self.can_delete and self._should_delete_form(form):
                continue
            link = form.cleaned_data.get('link')
            if link and link in links:
                raise forms.ValidationError(_("'{}' has been added multiple times.").format(link))
            links.append(link)


ResourceFormSet = modelformset_factory(Resource, form=ResourceForm, formset=BaseResourceFormSet, extra=2, max_num=10)
