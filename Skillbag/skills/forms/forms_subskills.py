
# Django
from django import forms

# Local
from nexus.forms import DescriptableForm
from skills.models import Subskill, Skill

class SubskillForm(DescriptableForm):   

    skill = forms.ModelChoiceField(queryset=None, empty_label=None)

    class Meta(DescriptableForm.Meta):
        model = Subskill
        fields = DescriptableForm.Meta.fields + ['skill', 'level']

    def __init__(self, skill_pk, *args, **kwargs):
        super().__init__(*args, **kwargs)
        skill = Skill.objects.get(pk=skill_pk)

        self.fields['skill'].widget.attrs['class'] = "editor-form__combobox"  
        self.fields['skill'].queryset = Skill.objects.filter(category=skill.category).order_by('name')
        
        self.fields['level'].widget.attrs['class'] = "editor-form__combobox"  
        