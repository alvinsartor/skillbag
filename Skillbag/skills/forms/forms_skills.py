
# Django
from django import forms

# Local
from nexus.forms import DescriptableForm
from skills.models import Skill
from categories.models import Category

class SkillForm(DescriptableForm):   

    category = forms.ModelChoiceField(queryset=Category.objects.all().select_related('macrocategory').order_by('macrocategory__name'), empty_label=None)
    
    class Meta(DescriptableForm.Meta):
        model = Skill
        fields = DescriptableForm.Meta.fields + ['category']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].widget.attrs['class'] = "editor-form__combobox"       