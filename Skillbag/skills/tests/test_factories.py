
from django.test import TestCase

from accounts.models import User
from categories.models import Macrocategory, Category
from skills.models import Skill, Subskill, AchievementType, SkillLevels
from categories.factories import MacrocategoryFactory, CategoryFactory
from skills.factories import SkillFactory, SubskillFactory, AchievementFactory


class SkillFactoryTest(TestCase):
    def test_factory_create_proper_skill(self):
        skill = SkillFactory()

        self.assertNotEqual(skill.name, '')
        self.assertIsNotNone(skill.description) 
        self.assertNotEqual(skill.description, '')
        self.assertIsNotNone(skill.wikipedia_link) 
        self.assertNotEqual(skill.wikipedia_link, '')
        self.assertIsNotNone(skill.category)        
        
        self.assertEqual(Skill.objects.all().count(), 1)
        self.assertEqual(Category.objects.all().count(), 1)
        self.assertEqual(Macrocategory.objects.all().count(), 1)


class SubskillFactoryTest(TestCase):
    def test_factory_create_proper_subskill(self):
        subskill = SubskillFactory()

        self.assertNotEqual(subskill.name, '')
        self.assertIsNotNone(subskill.description) 
        self.assertNotEqual(subskill.description, '')
        self.assertIsNotNone(subskill.wikipedia_link)
        self.assertIsNotNone(subskill.skill) 
        self.assertIn(subskill.level, SkillLevels.values)
        
        self.assertEqual(Subskill.objects.all().count(), 1)
        self.assertEqual(Skill.objects.all().count(), 1)
        self.assertEqual(Category.objects.all().count(), 1)
        self.assertEqual(Macrocategory.objects.all().count(), 1)


class AchievementFactoryTest(TestCase):
    def test_factory_create_proper_achievement(self):
        achievement = AchievementFactory()

        self.assertIn(achievement.type, AchievementType.values)
        self.assertEqual(User.objects.all().count(), 1)
        self.assertEqual(Subskill.objects.all().count(), 1)

