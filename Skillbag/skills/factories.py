# Python
import random

# 3rd Party
import factory
from factory.django import DjangoModelFactory

# Local
from categories.models import Macrocategory, Category
from categories.factories import CategoryFactory
from skills.models import Skill, Subskill, SkillLevels
from skills.models import Achievement, AchievementType
from accounts.models import User
from accounts.factories import UserFactory


class SkillFactory(DjangoModelFactory):
    class Meta:
        model = Skill

    name = factory.Faker('word')
    description = factory.Faker('sentence')
    wikipedia_link = factory.Faker('url')
    category = factory.SubFactory(CategoryFactory)

class SubskillFactory(DjangoModelFactory):
    class Meta:
        model = Subskill

    name = factory.Faker('sentence', nb_words=2)
    description = factory.Faker('sentence', nb_words=10)
    wikipedia_link = factory.Faker('url')
    skill = factory.SubFactory(SkillFactory)
    level = factory.LazyFunction(lambda : random.choice(SkillLevels.values))

class AchievementFactory(DjangoModelFactory):
    class Meta:
        model = Achievement

    user = factory.SubFactory(UserFactory)
    subskill = factory.SubFactory(SubskillFactory)
    type =  factory.LazyFunction(lambda : random.choice(AchievementType.values))