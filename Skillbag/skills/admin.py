
from django.contrib import admin
from skills.models import Skill, Subskill, Achievement, Resource
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin


@admin.register(Skill)
class SkillAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Basic Info', {'fields': ['name', 'category', 'description', 'wikipedia_link']}),
        ('connections', {'fields': ['superskills']}),       
    ]
    list_display = ['name']
    search_fields = ['name']


class ResourceAdmin(admin.TabularInline):
    model = Resource


@admin.register(Subskill)
class SubskillAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Basic Info', {'fields': ['name', 'skill', 'level', 'description', 'wikipedia_link']}),  
    ]
    list_display = ['name']
    search_fields = ['name']
    inlines = [ResourceAdmin]


@admin.register(Achievement)
class AchievementAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Basic Info', {'fields': ['user', 'subskill', 'type']}),  
    ]
    list_display = ['user', 'subskill', 'type']
    search_fields = ['user__email', 'subskill__name', 'type']