
from django.urls import path
from skills import views

app_name = 'skills'

urlpatterns = [
    # Skills
    path('', views.PopularSkillsView.as_view(), name='all-skills'),    
    path('my-skills/', views.MySkillsView.as_view()),    
    path('<int:pk>/edit/', views.EditSkilsFormView.as_view()),    
    path('<int:pk>/add/', views.AddSubskillFormView.as_view()),    
    path('search/', views.SkillsSearchView.as_view()),    
    path('search/<str:query>/', views.SkillsSearchView.as_view()),    
    
    # SubSkills
    path('subskills/<int:pk>/', views.SubskillDetailView.as_view()),    
    path('subskills/<int:pk>/edit/', views.EditSubskilsFormView.as_view()),    
    path('subskills/<int:pk>/editresources/', views.ResourcesFormView.as_view()),    
    path('subskills/search/', views.SubskillsSearchView.as_view()),    
    path('subskills/search/<str:query>/', views.SubskillsSearchView.as_view()),    
    
    # Achievements
    path('achievements/add/', views.MakeAchievementView.as_view()),
    path('achievements/remove/', views.DeleteAchievementView.as_view()),
]
