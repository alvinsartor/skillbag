
# Django
from django.views.generic.edit import FormView
from django.http import JsonResponse
from django.forms import formset_factory
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# Local
from skills.models import Resource, Subskill
from skills.forms import ResourceFormSet

@method_decorator(login_required, name='dispatch')
class ResourcesFormView(FormView):
    form_class = ResourceFormSet
    template_name = 'skills/forms/formset-resource.html'
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)       
        initial = Resource.objects.filter(subskill__pk=self.kwargs['pk'])
        context['formset'] = ResourceFormSet(queryset=initial)
        return context

    def post(self, request, *args, **kwargs):
        initial = Resource.objects.filter(subskill__pk=self.kwargs['pk'])
        formset = ResourceFormSet(request.POST, queryset=initial)
        
        if formset.is_valid():          
            subskill = Subskill.objects.get(pk=self.kwargs['pk'])
            for form in formset:
                form.save(request.user, subskill)
            return JsonResponse({})
            
        return render(request, self.template_name, {'formset': formset}) 

