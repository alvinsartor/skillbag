
# Django
from django.views.generic import TemplateView
from django.views.generic.edit import UpdateView, CreateView
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# Local
from skills.models import Subskill, Skill, Achievement
from skills.forms import SubskillForm

class SubskillDetailView(TemplateView):
    template_name = 'skills/fragments/subskill-detail-tile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        subskill = Subskill.objects.get(pk=str(kwargs.get('pk')))
        context['subskill'] = subskill
        context['resources'] = list(subskill.resource_set.all())
        context['editable'] =  'PageCategorySkills' in self.request.headers.get('Referer', 'PageCategorySkills')
        return context


@method_decorator(login_required, name='dispatch')
class EditSubskilsFormView(UpdateView):
    template_name = 'nexus/forms/generic_form.html'
    model = Subskill
    form_class = SubskillForm
    success_url = '/'
    
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        subskill_pk = self.kwargs.get('pk')
        kwargs["skill_pk"] = Skill.objects.get(subskill=subskill_pk).pk
        return kwargs

    def form_valid(self, form):
        form.save()
        return JsonResponse({})

    
@method_decorator(login_required, name='dispatch')
class AddSubskillFormView(CreateView):
    template_name = 'nexus/forms/generic_form.html'
    model = Subskill
    form_class = SubskillForm
    success_url = '/'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["skill_pk"] = self.kwargs.get('pk')
        return kwargs
        
    def get_initial(self):
        initial = super().get_initial()
        initial['skill'] = self.kwargs.get('pk')
        return initial

    def form_valid(self, form):
        form.save()
        return JsonResponse({})


class SubskillsSearchView(TemplateView):
    template_name = 'skills/fragments/subskills-search-result.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)        
        if 'query' in kwargs:
            query = kwargs.get('query', '').strip()
            subskills = Subskill.objects.filter(name__icontains=query).order_by('name')
            empty_message = "It seems there are no subskills corresponding to your search."
        else:
            subskills = Subskill.objects.none()
            empty_message = "Start typing to see matching subskills."
            
        if self.request.user.is_authenticated:
            user = self.request.user
            achievements = Achievement.objects.filter(user=user).values('subskill', 'type')
            context['subskills_achievements'] = { achievement['subskill']:achievement['type'] for achievement in achievements }
        else:
            context['subskills_achievements'] = {subskill.id:'anonymous' for subskill in subskills}

        context['elements'] = subskills
        context['empty_message'] = empty_message
        return context
