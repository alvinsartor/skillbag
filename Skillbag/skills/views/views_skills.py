# Python
from abc import ABC, abstractmethod

# Django
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Count
from django.db.models import FloatField
from django.db.models import Max
from django.db.models.functions import Cast
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.generic.edit import UpdateView, CreateView
from django.views.generic.base import TemplateView


# Local
from accounts.models import Team, TeamMember
from accounts.views.teams.views_team_dashboard import TeamBasicPermissionsTestMixin
from categories.models import Category
from skills.models import Skill, Subskill, SkillLevels, Achievement
from skills.forms import SkillForm


class BaseSkillsView(TemplateView, ABC):
    
    def initialize(self, **kwargs):
        """ Function used to cache values. """
        pass

    @property
    @abstractmethod
    def page_title(self):
        """ Specifies the title shown on this page. """
    
    @property
    def nothing_to_show_text(self):
        """ Specifies the text that will be shown in case there are no skills. """
        return None

    @property
    @abstractmethod
    def allow_edit(self):
        """ Specifies whether it is allowed to edit skills and subskills on this page. """
        
    @abstractmethod
    def get_skills(self):
        """ The skills that will be shown in this view """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        self.initialize(**kwargs)
        skills = self.get_skills().prefetch_related('subskill_set')
        context['skills'] = skills
        context['skills_list'] = list(skills.values_list('pk', flat=True))
        context['skill_level_names'] = { choice[0]:choice[1] for choice in SkillLevels.choices }
        context['title'] = self.page_title
        context['nothing_to_show_text'] = self.nothing_to_show_text
        context['allow_edit'] = self.allow_edit
                
        if self.request.user.is_authenticated:
            user = self.request.user
            achievements = Achievement.objects.filter(user=user).values('subskill', 'type')
            context['subskills_achievements'] = { achievement['subskill']:achievement['type'] for achievement in achievements }
        else:
            subskills = Subskill.objects.filter(skill__in=list(skills))
            context['subskills_achievements'] = {subskill.id:'anonymous' for subskill in subskills}
                
        return context


@method_decorator(login_required, name='dispatch')
class CategorySkillsView(BaseSkillsView):
    template_name = 'skills/page-category-skills.html'
    allow_edit = True    

    def initialize(self, **kwargs):
        category_pk = str(kwargs.get('pk'))
        self.category = get_object_or_404(Category, pk=category_pk)

    @property
    def page_title(self, **kwargs):
        return self.category.name

    def get_skills(self):
        return Skill.objects.filter(category=self.category)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = self.category
        return context


class PopularSkillsView(BaseSkillsView):
    template_name = 'skills/page-generic-skills.html'
    allow_edit = False
    page_title = "10 Most Popular Skills"

    def get_skills(self):
        return Skill.objects.all().annotate(
            popularity=Cast(Count('subskill__achievement'), FloatField()) / Cast((Count('subskill') + 1), FloatField())
            ).order_by('-popularity')[:10]
    

@method_decorator(login_required, name='dispatch')
class MySkillsView(BaseSkillsView):
    template_name = 'skills/page-generic-skills.html'
    allow_edit = False
    page_title = "My Skills"

    def get_skills(self):
        return Skill.objects.filter(subskill__achievement__user=self.request.user).annotate(latest_update=Max('subskill__achievement__updated_at')).order_by('-latest_update')
    

@method_decorator(login_required, name='dispatch')
class TeamSkillsView(TeamBasicPermissionsTestMixin, BaseSkillsView):
    template_name = 'skills/page-team-skills.html'
    allow_edit = False

    def initialize(self, **kwargs):
        team_url_name = str(kwargs.get('team_name'))
        self.team = Team.objects.get(url_name=team_url_name)

    @property
    def page_title(self, **kwargs):
        return self.team.name

    @property
    def nothing_to_show_text(self, **kwargs):
        return _('There are no skills here yet! Administrators can add skills to the team through the admin panel.')

    def get_skills(self):
        return self.team.skills.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)        
        context['team_name'] = self.team.url_name
        context['is_member_admin'] = TeamMember.objects.filter(user=self.request.user, team__url_name=self.team.url_name, administrator=True, pending=False).exists()

        return context



class SkillsSearchView(BaseSkillsView):
    template_name = 'skills/page-search-skills.html'
    allow_edit = False
    page_title = ""
    
    def initialize(self, **kwargs):
        self.query = kwargs.get('query', None)        

    @property
    def nothing_to_show_text(self, **kwargs):
        if self.query:
            return _("It seems there are no skills containing '{}'.".format(self.query))
        else:
            return _('Start typing to see matching skills.')

    def get_skills(self):
        return Skill.objects.filter(name__icontains=self.query).order_by('name') if self.query else Skill.objects.none()


@method_decorator(login_required, name='dispatch')
class EditSkilsFormView(UpdateView):
    template_name = 'nexus/forms/generic_form.html'
    model = Skill
    form_class = SkillForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return JsonResponse({})

    
@method_decorator(login_required, name='dispatch')
class AddSkillFormView(CreateView):
    template_name = 'nexus/forms/generic_form.html'
    model = Skill
    form_class = SkillForm
    success_url = '/'

    def get_initial(self):
        initial = super().get_initial()
        category = self.kwargs.get('pk')
        initial['category'] = category
        return initial

    def form_valid(self, form):
        form.save()
        return JsonResponse({})
