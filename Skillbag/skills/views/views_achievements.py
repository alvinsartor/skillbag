
from accounts.models import User
from skills.models import Subskill, Achievement, AchievementType

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

@method_decorator(login_required, name='dispatch')
class MakeAchievementView(View):
    """
        View used to create/update achievements.
        POST:
            'user': the user the achievement belongs to
            'subskill': the subskill the achievement refers to
            'type': the achievement type. It must belong to the set {'planned', 'achieved'}
    """
    def post(self, request, *args, **kwargs):
        user = get_object_or_404(User, pk=request.POST.get('user', -1))
        subskill = get_object_or_404(Subskill, pk=request.POST.get('subskill', -1))
        
        type = request.POST.get('type', '')
        if type not in AchievementType.values:
            raise Http404("The selected type is not allowed: " + type)

        Achievement.objects.update_or_create(user=user, subskill=subskill, defaults={'type': type})
        return HttpResponse(status=200)


@method_decorator(login_required, name='dispatch')
class DeleteAchievementView(View):
    """
        View used to remove achievements.
        POST:
            'user': the user the achievement belongs to
            'subskill': the subskill the achievement refers to
    """
    def post(self, request, *args, **kwargs):
        achievement = get_object_or_404(Achievement, user=request.POST.get('user', -1), subskill=request.POST.get('subskill', -1))
        achievement.delete()
        return HttpResponse(status=200)
