# Python
import json

# Django
from django.http import JsonResponse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.generic import FormView, TemplateView
from django.contrib.auth.views import PasswordChangeView
from django.contrib.auth import update_session_auth_hash

# Local
from accounts.models import User
from accounts.forms import RegistrationForm, ClassedPasswordChangeForm


class EmailPreferencesView(TemplateView):
    template_name = 'accounts/page-email-preferences.html'


class ChangePasswordView(PasswordChangeView):
    template_name = 'accounts/page-change-password.html'    
    form_class = ClassedPasswordChangeForm

    def form_invalid(self, form):
        errors = [error for key, value in form.errors.items() for error in value]
        return JsonResponse({'errors': errors})

    def form_valid(self, form):
        user = form.save()
        update_session_auth_hash(self.request, user)
        return JsonResponse({})