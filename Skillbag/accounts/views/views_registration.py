
# Django
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.generic import FormView

# Local
from accounts.models import User
from accounts.forms import RegistrationForm


class SignupView(FormView):
    form_class = RegistrationForm
    template_name = 'accounts/register.html'
    success_url = '/'

    def form_valid(self, form):
        user = User()
        user.email = form.cleaned_data['username']
        user.set_password(form.cleaned_data['password'])
        user.save()

        user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        login(self.request, user)

        return super().form_valid(form)
