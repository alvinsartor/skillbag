
# Python
from collections import defaultdict 

# Django
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, View
from django.shortcuts import render

# Local
from accounts.models import Team, TeamMember, User
from skills.models import Skill, Achievement

    
class TeamAdminPermissionsTestMixin(UserPassesTestMixin):

    def handle_no_permission(self):
        raise PermissionDenied()

    def test_func(self):                 
        url_name = self.kwargs.get('team_name', '')
        return TeamMember.objects.filter(user=self.request.user, team__url_name=url_name, administrator=True, pending=False).exists()        


@method_decorator(login_required, name='dispatch')
class TeamAdminView(TeamAdminPermissionsTestMixin, TemplateView):
    template_name = 'accounts/page-team-admin.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        url_name = self.kwargs.get('team_name', '')
        team = Team.objects.get(url_name=url_name)

        context['team_name'] = url_name
        context['title'] = "{} - Admin".format(team.name)
        
        members = TeamMember.objects.filter(team=team).order_by('-administrator').select_related('user')
        context['members'] = [member for member in members if not member.pending]
        context['pending_members'] = [member for member in members if member.pending]
        context['skills'] = team.skills.all().prefetch_related('subskill_set')                
       
        return context

@method_decorator(login_required, name='dispatch')
class TeamInvitableMembersFetcherView(TeamAdminPermissionsTestMixin, View):

    def get(self, request, *args, **kwargs):
        query = self.kwargs.get('query', '')
        url_name = self.kwargs.get('team_name', '')
        members = User.objects.filter(email__icontains=query).exclude(teammember__team__url_name=url_name)[:4].values_list('email', flat=True)
        return JsonResponse(list(members), safe=False)


@method_decorator(login_required, name='dispatch')
class TeamInviteUserView(TeamAdminPermissionsTestMixin, View):
    
    def post(self, request, *args, **kwargs):
        user_to_invite = request.POST.get('user_email', '')
        url_name = self.kwargs.get('team_name', '')
        
        if TeamMember.objects.filter(user__email=user_to_invite, team__url_name=url_name).exists():
            return HttpResponse("{} has already been invited".format(user_to_invite), status=400)

        try:
            user = User.objects.get(email=user_to_invite)
        except User.DoesNotExist:
            return HttpResponse("The email {} is not associated to any known user".format(user_to_invite), status=400)

        team = Team.objects.get(url_name=url_name)
        new_member = TeamMember(team=team, user=user, administrator=True, pending=True)
        new_member.save()      
        new_member.generate_code_and_send_email(request)

        return render(
            request, 
            template_name='accounts/fragments/admin-member-fragment.html',
            context=
            { 
                'member' : new_member,
                'team_name': url_name,
            })


@method_decorator(login_required, name='dispatch')
class TeamInvitationAcceptView(View):

    def get(self, request, *args, **kwargs):
        url_name = self.kwargs.get('team_name', '')
        invitation_code = self.kwargs.get('invitation_code', '')

        try:
            invited_member = TeamMember.objects.get(team__url_name=url_name, pending=True, invitation_code=invitation_code)
            invited_member.pending = False
            invited_member.invitation_code = None
            invited_member.save()
            return HttpResponseRedirect('/bag/PageTeamSkills/{}'.format(url_name))
        except TeamMember.DoesNotExist:
            return HttpResponseRedirect('/404/')


@method_decorator(login_required, name='dispatch')
class TeamRemoveUserView(TeamAdminPermissionsTestMixin, View):

    def post(self, request, *args, **kwargs):
        user_to_remove = request.POST.get('user_email', '')
        url_name = self.kwargs.get('team_name', '')
        
        try:
            TeamMember.objects.get(user__email=user_to_remove, team__url_name=url_name).delete()
        except User.DoesNotExist:
            return HttpResponse("{} is not part of the team".format(user_to_invite), status=400)

        # if there are no admins left, all the remaining users are promoted to admin
        if TeamMember.objects.filter(team__url_name=url_name, administrator=True, pending=False).count() == 0:
            TeamMember.objects.filter(team__url_name=url_name, administrator=False, pending=False).update(administrator=True)

        # if there are no members left, the group is destroyed
        if TeamMember.objects.filter(team__url_name=url_name, pending=False).count() == 0:
            TeamMember.objects.filter(team__url_name=url_name).delete()
            Team.objects.get(url_name=url_name).delete()

        # if the user removed itself, a signal asking to exit from the team is sent
        if request.user.email == user_to_remove:
            return HttpResponse("exit", status=200)

        return HttpResponse(status=200)

    
@method_decorator(login_required, name='dispatch')
class TeamToggleAdministratorStateView(TeamAdminPermissionsTestMixin, View):

    def post(self, request, *args, **kwargs):
        user_to_remove = request.POST.get('user_email', '')
        url_name = self.kwargs.get('team_name', '')
        
        try:
            member = TeamMember.objects.get(user__email=user_to_remove, team__url_name=url_name)
            member.administrator = not member.administrator
            member.save()
        except User.DoesNotExist:
            return HttpResponse("{} is not part of the team".format(user_to_invite), status=400)

        # if there are no admins left, all the remaining users are promoted to admin
        if TeamMember.objects.filter(team__url_name=url_name, administrator=True, pending=False).count() == 0:
            TeamMember.objects.filter(team__url_name=url_name, administrator=False, pending=False).update(administrator=True)

        # if the user removed itself, a signal asking to exit from the admin view of the team is sent
        if request.user.email == user_to_remove:
            return HttpResponse("exit", status=200)

        return HttpResponse(status=200)



@method_decorator(login_required, name='dispatch')
class TeamAddeableSkillsFetcherView(TeamAdminPermissionsTestMixin, View):

    def get(self, request, *args, **kwargs):
        query = self.kwargs.get('query', '')
        url_name = self.kwargs.get('team_name', '')
        members = (
            Skill.objects.select_related('category')
                .filter(name__icontains=query)
                .exclude(team__url_name=url_name)[:4]
                .values('pk', 'name', 'category__name')
            )
        return JsonResponse(list(members), safe=False)


@method_decorator(login_required, name='dispatch')
class TeamAddSkillView(TeamAdminPermissionsTestMixin, View):
    
    def post(self, request, *args, **kwargs):
        skill_to_add = request.POST.get('skill_pk', '')
        url_name = self.kwargs.get('team_name', '')
        
        if Team.objects.filter(url_name=url_name, skills__pk=skill_to_add).exists():
            return HttpResponse("Skill {} has already been added".format(skill_to_add), status=400)

        skill = Skill.objects.get(pk=skill_to_add)
        team = Team.objects.get(url_name=url_name)
        team.skills.add(skill)
      
        return render(
            request, 
            template_name='accounts/fragments/admin-skill-fragment.html',
            context=
            { 
                'skill' : skill,
                'team_name': url_name,
            })


@method_decorator(login_required, name='dispatch')
class TeamRemoveSkillView(TeamAdminPermissionsTestMixin, View):
    
    def post(self, request, *args, **kwargs):
        skill_to_remove = request.POST.get('skill_pk', '')
        url_name = self.kwargs.get('team_name', '')
        
        if not Team.objects.filter(url_name=url_name, skills__pk=skill_to_remove).exists():
            return HttpResponse("The skill {} is not part of the team skillset".format(skill_to_remove), status=400)

        skill = Skill.objects.get(pk=skill_to_remove)
        team = Team.objects.get(url_name=url_name)
        team.skills.remove(skill)

        return HttpResponse(status=200)

