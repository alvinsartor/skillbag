

# Python
from collections import defaultdict 
from collections import Counter
import random

# Django
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, View
from django.shortcuts import render
from django.db.models import Avg, Count, Q

# Local
from accounts.models import Team, TeamMember, User
from accounts.views.teams.helpers.user_top_skills import UserTopSkills
from skills.models import Skill, Achievement, SkillLevels, Subskill


class ColorsPicker:
    
    COLORS = [
        "#9fa8da",
        "#f48fb1",
        "#80cbc4",
        "#e6ee9c",
        "#ce93d8",
        "#eeeeee",
        "#c5e1a5",
        "#a5d6a7",
        "#ef9a9a",
        "#90caf9",
        "#80deea",
        "#ffab91",
        "#b39ddb",
        "#81d4fa",
        "#fff59d",
        "#ffe082",
        "#bcaaa4",
        "#b0bec5",
        "#ffcc80",
    ]

    @classmethod
    def associate_list_with_colors(cls, elements):
        """ Returns a dictionary in which element of the passed collection is associated to a color. """
        i = 0
        colors_association = {} 
        for element in elements:
            colors_association[element] = cls.COLORS[i]
            i = (i + 1) % len(cls.COLORS)

        return colors_association

    
class TeamBasicPermissionsTestMixin(UserPassesTestMixin):

    def handle_no_permission(self):
        raise PermissionDenied()

    def test_func(self):                 
        url_name = self.kwargs.get('team_name', '')
        return TeamMember.objects.filter(user=self.request.user, team__url_name=url_name, pending=False).exists()        


@method_decorator(login_required, name='dispatch')
class TeamDashboardView(TeamBasicPermissionsTestMixin, TemplateView):
    template_name = 'accounts/page-team-dashboard.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        url_name = self.kwargs.get('team_name', '')
        team = Team.objects.get(url_name=url_name)
        skills = team.skills.all().prefetch_related('subskill_set').select_related('category').annotate(nr_subskills = Count('subskill')).order_by('-nr_subskills')

        context['team_name'] = url_name
        context['title'] = "{} - Dashboard".format(team.name)                
        context['members'] = members = TeamMember.objects.filter(team=team, pending=False).select_related('user')
        context['skills'] = skills
        context['skills_ids'] = { skill.id:skill.name for skill in skills }
        
        context = TeamDashboardView.add_members_related_context(context, team, members, skills)
        context = TeamDashboardView.add_skills_related_context(context, team, members, skills)
        
        return context

    @classmethod
    def add_members_related_context(cls, context, team, members, skills):
        context['members_skills'] = cls.get_skills_members_are_experts_and_learning(team, members, skills, 3)
        context['is_member_admin'] = any(member.administrator and not member.pending for member in members)
       
        return context

    @classmethod
    def get_skills_members_are_experts_and_learning(cls, team, members, skills, max_skills_per_achievement_type):
        """ Returns a dictionary containing the users and the list of their top skills (owned and planned) """

        achievements_count = (
            members
            .filter(user__achievement__subskill__skill__in=list(skills))
            .values('user', 'user__achievement__subskill__skill', 'user__achievement__subskill__skill__category', 'user__achievement__type')
            .annotate(count = Count('user__achievement__subskill__skill'))  
            .order_by('user', '-count')
        )

        members_skills = {}        
        for record in achievements_count:
            user = record['user']
            skill = record['user__achievement__subskill__skill']
            category = record['user__achievement__subskill__skill__category']
            achievement_type = record['user__achievement__type']

            # initiate dictionary for users
            if user not in members_skills:
                members_skills[user] = UserTopSkills(user, max_skills_per_achievement_type)
            members_skills[user].add_skill(skill, category, achievement_type)
       
        return members_skills


    @classmethod
    def add_skills_related_context(cls, context, team, members, skills):        
        num_members = len(members)
        skills_and_subskills, all_subskills_pks = TeamDashboardView.extract_skills_and_subskills_dictionary(skills)
        subskill_owners = TeamDashboardView.retrieve_team_members_owning_subskill(team, all_subskills_pks)
        subskill_completion = TeamDashboardView.calculate_subskill_completion_percentages(subskill_owners, num_members)
        skill_completion = TeamDashboardView.calculate_skill_completion_percentages(skills_and_subskills, subskill_completion)
        skills_categories = set(skills.values_list('category_id', flat=True))
        skill_top_experts = TeamDashboardView.extract_top_experts(skills_and_subskills, subskill_owners, max_experts_per_skill=3)

        context['subskill_owners'] = subskill_owners
        context['skill_top_experts'] = skill_top_experts
        context['subskill_completion'] = TeamDashboardView.prepare_percentages(subskill_completion)
        context['skill_completion'] = TeamDashboardView.prepare_percentages(skill_completion)        
        context['categories_colors'] = ColorsPicker.associate_list_with_colors(skills_categories)
        context['skill_level_names'] = { choice[0]:choice[1] for choice in SkillLevels.choices }
        context['members_medals'] = TeamDashboardView.extract_medals_from_top_experts(members, skill_top_experts)
        return context


    @classmethod
    def retrieve_team_members_owning_subskill(cls, team, all_subskills_pks):
        """ 
            Returns a dictionary containing the emails of the users that own and have planned each subskill:
                {
                    1: [['achieved1@mail.com', 'achieved2@mail.com'], []],
                    2: [['achieved3@mail.com'], ['planner1@mail.com']],
                    3: [[], []],
                }
        """        
        def extract_team_members_with_subskill(team, subskill_type):
            achievements = (
                Achievement.objects.filter(
                    subskill__skill__team=team, user__team=team, user__teammember__pending=False, type=subskill_type
                )
                .select_related('user__email')
                .values('subskill', 'user__email')
            )

            subskill_owners = {}
            for item in achievements:
                subskill_id = item['subskill']
                user_email = item['user__email']
                if subskill_id in subskill_owners:
                    subskill_owners[subskill_id].append(user_email)
                else:
                    subskill_owners[subskill_id] = [user_email]
            return subskill_owners

        owned_subskills = extract_team_members_with_subskill(team, 'achieved')
        planned_subskills = extract_team_members_with_subskill(team, 'planned')

        return {
            subskill:[
                owned_subskills[subskill] if subskill in owned_subskills else [], 
                planned_subskills[subskill] if subskill in planned_subskills else []] 
            for subskill in all_subskills_pks 
            }


    @classmethod
    def calculate_subskill_completion_percentages(cls, subskill_owners, num_members):
        """
            It returns a dictionary containing the percentage of users owning and planning a subskill:
            {
                1: [1, 0],
                2: [0.5, 0.5],
                3: [0, 0],
            }
        """
        return { 
            subskill:[
                len(subskill_owners[subskill][0]) / num_members * 100, 
                len(subskill_owners[subskill][1]) / num_members * 100]
            for subskill in subskill_owners 
            }
    
    @classmethod
    def extract_skills_and_subskills_dictionary(cls, skills):
        """ Returns a dictionary containing the skills pks as keys and list the relative subksill pks as values. """
        skills_and_subskills = {}
        all_subskills = []

        skills_ids = list(skills.values_list('pk', flat=True))
        for id in skills_ids:
            skills_and_subskills[id] = []

        
        subskills = Subskill.objects.filter(skill__pk__in=skills_ids).values('skill__pk', 'pk')
        for result in subskills:      
            skill_pk = result['skill__pk']
            subskill_pk = result['pk']
            
            if not subskill_pk:
                continue

            skills_and_subskills[skill_pk].append(subskill_pk)
            all_subskills.append(subskill_pk)

        return skills_and_subskills, all_subskills

    @classmethod
    def calculate_skill_completion_percentages(cls, skills_and_subskills, subskill_completion_percentages):       
        """
            It returns a dictionary containing the percentage of users owning and planning a skill,
            based on the total amount of users owning and planning its subskills:
            {
                1: [1, 0],
                2: [0.5, 0.5],
                3: [0, 0],
            }
        """        
        def get_average_from_subskills_list(subskill_pks, completion):
            if len(subskill_pks) == 0:
                return 0
            
            index = 0 if completion else 1
            percentages = [subskill_completion_percentages[pk][index] for pk in subskill_pks]
            return int(sum(percentages) / len(subskill_pks))

        return {
            skill:[
                get_average_from_subskills_list(skills_and_subskills[skill], True),
                get_average_from_subskills_list(skills_and_subskills[skill], False),
                ]
            for skill in skills_and_subskills
            }

    @classmethod
    def extract_top_experts(cls, skills_and_subskills, subskill_owners, max_experts_per_skill=3):
        """ 
            It returns a dictionary containing the skills and the top experts for that skill. 
            example:
            {
                1: [['user1', "2/3"], ['user2', "2/3"], ['user3', "2/3"]], 
                2: [['user3', "7/9"], ['user1', "4/9"], ['user2', "1/9"]], 
            }
            
        """
        def get_most_recurrent_names(users_list, number_subskills):
            owners_count = Counter(users_list)
            experts_current_skill = [owner[0] for owner in owners_count.most_common()][:max_experts_per_skill]
            experts_with_amounts = [[expert, "{}/{}".format(owners_count[expert], number_subskills)] for expert in experts_current_skill]
            return experts_with_amounts
            
        top_experts = {}
        for skill in skills_and_subskills:
            subskills_list = skills_and_subskills[skill]
            all_owners = []
            for subskill in subskills_list:
                all_owners += subskill_owners[subskill][0]
            top_experts[skill] = get_most_recurrent_names(all_owners, len(subskills_list))          

        return top_experts

    @classmethod
    def prepare_percentages(cls, dict_with_percentages):
        return { 
            s:[
                "{}%".format(dict_with_percentages[s][0]),
                "{}%".format(dict_with_percentages[s][0] + dict_with_percentages[s][1]),
                ] 
            for s in dict_with_percentages }

    @classmethod
    def extract_medals_from_top_experts(cls, members, skills_top_experts):
        """ 
            Count how many medals each member has and places it to a dictionary 
            example (3 users, 3 skills):
            
            {
                'user1': [2,1,0],
                'user2': [1,0,2],
                'user3': [0,2,1]
            }            
        """

        members_medals = {member:[0,0,0] for member in members.values_list('user__email', flat=True)}
        for skill in skills_top_experts:
            position = 0
            for expert in skills_top_experts[skill]:            
                user = expert[0]
                members_medals[user][position] += 1
                position += 1

        return members_medals

