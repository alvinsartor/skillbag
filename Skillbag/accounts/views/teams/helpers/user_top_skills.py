

class UserTopSkills:

    def __init__(self, user, max_skills=3):
        self.user = user
        self.max_skills = max_skills
        self.owned_skills = []
        self.planned_skills = []

    def add_skill(self, skill, category, achievement_type):
        if achievement_type == 'achieved':
            collection = self.owned_skills
        elif achievement_type == 'planned':
            collection = self.planned_skills
        else:
            raise Exception("The achivement type '{}' was not recognized.".format(achievement_type))        
        self._add_skill_to_list(collection, skill, category)

    def _add_skill_to_list(self, collection, skill, category):
        if len(collection) >= self.max_skills:
            return
        
        union = SkillAndCategory(skill, category)
        collection.append(union)

    def __str__(self):
        result = "Owned: " + ", ".join([str(s.skill) for s in self.owned_skills]) + "\n"
        result += "Planned: " + ", ".join([str(s.skill) for s in self.planned_skills]) + "\n"
        return result

class SkillAndCategory:
    def __init__(self, skill, category):
        self.skill = skill
        self.category = category