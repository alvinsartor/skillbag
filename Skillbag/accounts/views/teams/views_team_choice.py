# Django
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.http import JsonResponse

# Local
from accounts.models import Team, TeamMember
from accounts.forms import TeamForm


@method_decorator(login_required, name='dispatch')
class TeamChoiceView(TemplateView):
    template_name = 'accounts/page-team-selection.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['teams'] = Team.objects.filter(members=self.request.user, teammember__pending=False)
        return context


@method_decorator(login_required, name='dispatch')
class AddTeamFormView(CreateView):
    template_name = 'nexus/forms/generic_form.html'
    model = Team
    form_class = TeamForm
    success_url = '/'

    def form_valid(self, form):
        team = form.save()
        team_member = TeamMember(team=team, user=self.request.user, administrator=True, pending=False)
        team_member.save()
        return JsonResponse({})
