# 3rd Party
import factory
from factory.django import DjangoModelFactory
from django.contrib.auth.hashers import make_password

# Local
from accounts.models import User, Team, TeamMember

class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    email = factory.Faker('email')
    password = factory.PostGenerationMethodCall('set_password', '123456789')


class TeamFactory(DjangoModelFactory):
    class Meta:
        model = Team

    name = factory.Faker('word')
    url_name = factory.LazyAttribute(lambda team: '{}_{}'.format(team.name.replace(' ', '_'), 12345).lower())

    @factory.post_generation
    def skills(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for skill in extracted:
                self.skills.add(skill)


class TeamMemberFactory(DjangoModelFactory):
    class Meta:
        model = TeamMember

    user = factory.SubFactory(UserFactory)
    team = factory.SubFactory(TeamFactory)