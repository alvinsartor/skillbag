
# Django
from django.forms import ModelForm
from django.utils.translation import gettext as _

# Local
from accounts.models import Team

class TeamForm(ModelForm):   

    class Meta:
        model = Team
        fields = ['name', 'url_name']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['required'] = True
        self.fields['name'].widget.attrs['class'] = "editor-form__text-field editor-form__text-field--title"
        self.fields['name'].widget.attrs['placeholder'] = _('Team Name')
        self.fields['name'].widget.attrs['maxlength'] = "64"
        self.fields['name'].widget.attrs['autocomplete'] = "off"
        
        self.fields['url_name'].widget.attrs['required'] = True
        self.fields['url_name'].widget.attrs['class'] = "editor-form__text-field"
        self.fields['url_name'].widget.attrs['placeholder'] = _('Url Name')
        self.fields['url_name'].widget.attrs['maxlength'] = "64"
        self.fields['url_name'].widget.attrs['autocomplete'] = "off"
        self.fields['url_name'].help_text = _('Used to create a personalized URL for the team page. It must be unique.')       