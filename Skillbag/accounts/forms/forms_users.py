
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import PasswordChangeForm
from django.utils.translation import ugettext_lazy as _

from accounts.models import User

class BasicUserForm(forms.Form):

    username = forms.CharField(label=_('Email'), max_length=254)
    password = forms.CharField(label=_("Password"), max_length=254)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget = forms.EmailInput()
        self.fields['username'].widget.attrs['placeholder'] = _('Email')
        self.fields['username'].widget.attrs['class'] = "login-form__email-field"

        self.fields['password'].widget = forms.PasswordInput()
        self.fields['password'].widget.attrs['placeholder'] = _('Password')
        self.fields['password'].widget.attrs['class'] = "login-form__password-field"

class LoginForm(BasicUserForm, AuthenticationForm):
    pass

class RegistrationForm(BasicUserForm):
    
    def clean_password(self):
        password = self.cleaned_data['password']
        if len(password) < 8:
            raise forms.ValidationError(_('The password should be at least 8 characters long.'))       
        return password

    def clean_username(self):
        email = self.cleaned_data['username']
        if '@' not in email:
            raise forms.ValidationError(_('Are you sure this is an email?'))        
        
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError(_('This email already exists. Did you want to login?'))        
        
        return email 


class ClassedPasswordChangeForm(PasswordChangeForm):

    def __init__(self, user, *args, **kwargs):
        super().__init__(user, *args, **kwargs)        
        self.fields['old_password'].widget.attrs['class'] = 'account-form__input-field'
        self.fields['new_password1'].widget.attrs['class'] = 'account-form__input-field'
        self.fields['new_password2'].widget.attrs['class'] = 'account-form__input-field'
        self.fields['new_password2'].label = "Repeat password"