
from django.test import TestCase

from accounts.models import User, Team, TeamMember
from accounts.factories import UserFactory, TeamFactory, TeamMemberFactory


class UserFactoryTest(TestCase):
    def test_factory_creates_proper_user(self):
       user = UserFactory()
       
       self.assertNotEqual(user.email, '')
       self.assertEqual(User.objects.all().count(), 1)


class TeamFactoryTest(TestCase):
    def test_factory_creates_team(self):
        team = TeamFactory()

        self.assertNotEqual(team.name, '')
        self.assertNotEqual(team.url_name, '')
        self.assertEqual(Team.objects.all().count(), 1)

    def test_factory_can_receive_set_of_skills_as_argument(self):
        from skills.factories import SkillFactory
        skills = SkillFactory.create_batch(10)
        team = TeamFactory(name='The Coolest', skills=skills)
        
        self.assertEqual(len(skills), team.skills.all().count())
