
# Python 
import uuid 

# Django
from django.core.mail import send_mail
from django.db import models
from django.template.loader import render_to_string
from django.urls import reverse


# Local
from accounts.models import User
from nexus.models.abstract import TimeStampMixin


class Team(TimeStampMixin):
    name = models.CharField(max_length=64)
    url_name = models.CharField(max_length=64, unique=True)
    members = models.ManyToManyField(User, through='TeamMember')
    skills = models.ManyToManyField('skills.Skill')
    
    class Meta:
        indexes = [
            models.Index(fields=['url_name'])
        ]

    def __str__(self):
        return self.name


class TeamMember(TimeStampMixin):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    administrator = models.BooleanField('Admin', default=True)
    
    pending = models.BooleanField('Pending', default=False)
    invitation_code = models.CharField(max_length=16, null=True, blank=True, default=None)

    class Meta:
        unique_together = ['user', 'team']

    def generate_code_and_send_email(self, request):
        if not self.pending:
            return

        self.invitation_code = uuid.uuid4().hex[:16]
        self.save()

        url = reverse('accounts:invitation-accept', args=[self.team.url_name, self.invitation_code])

        context = {'url': request.build_absolute_uri(url), 'team_name': self.team.name}
        msg_plain = render_to_string('accounts/emails/team-invitation.txt', context)
        msg_html = render_to_string('accounts/emails/team-invitation.html', context)

        send_mail(
            "You have been invited to the team '{}'".format(self.team.name), 
            msg_plain,
            'ananasproject@gmail.com', 
            [self.user.email],            
            html_message=msg_html,
            fail_silently=False
            )