
from django.contrib import admin
from accounts.models import User, Team, TeamMember
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    
    fieldsets = [
        ('Basic Info', {'fields': ['email', 'password']}),
        #('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    ]

    list_display = ['email', 'date_joined', 'last_login']
    ordering = ['-last_login']

class TeamMemberInline(admin.TabularInline):
    model = TeamMember

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    model = Team

    filter_horizontal = ['skills']
    inlines = [TeamMemberInline]
    list_display = ['name', 'url_name', 'members_count']
    
    def members_count(self, team):
        return team.members.all().count()
