
# Django
from django.contrib.auth import views as auth_views
from django.urls import path

# Local
from accounts.views import views_account
from accounts.views import views_registration
from accounts.views import teams as views_teams
from accounts import forms
from skills.views import views_skills

app_name = 'accounts'

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(template_name='accounts/login.html',authentication_form=forms.LoginForm), name='login'),
    path('register/', views_registration.SignupView.as_view(), name='register'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),

    path('email-preferences/', views_account.EmailPreferencesView.as_view()),
    path('change-password/', views_account.ChangePasswordView.as_view()),

    path('teams/', views_teams.TeamChoiceView.as_view()),
    path('teams/add/', views_teams.AddTeamFormView.as_view()),
    path('teams/<team_name>/', views_skills.TeamSkillsView.as_view()),
    path('teams/<team_name>/admin/', views_teams.TeamAdminView.as_view()),
    path('teams/<team_name>/dashboard/', views_teams.TeamDashboardView.as_view()),
    
    path('teams/<team_name>/query-member/<query>/', views_teams.TeamInvitableMembersFetcherView.as_view()),
    path('teams/<team_name>/invite-member/', views_teams.TeamInviteUserView.as_view()),
    path('teams/<team_name>/accept-invitation/<invitation_code>/', views_teams.TeamInvitationAcceptView.as_view(), name='invitation-accept'),
    path('teams/<team_name>/remove-member/', views_teams.TeamRemoveUserView.as_view()),
    path('teams/<team_name>/toggle-admin/', views_teams.TeamToggleAdministratorStateView.as_view()),

    path('teams/<team_name>/query-skill/<query>/', views_teams.TeamAddeableSkillsFetcherView.as_view()),
    path('teams/<team_name>/add-skill/', views_teams.TeamAddSkillView.as_view()),
    path('teams/<team_name>/remove-skill/', views_teams.TeamRemoveSkillView.as_view()),
]
