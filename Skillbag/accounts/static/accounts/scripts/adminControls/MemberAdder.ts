
class MemberAdder extends GenericAdder {
    private readonly _teamName: string;

    constructor(teamName: string) {
        super("member-adder-input");
        this._teamName = teamName;
    }

    /** The number of characters that will trigger the appearance of the container. */
    protected minInputLength = 4;

    /** The class applied to the suggestion container. */
    protected containerClass = "team-admin__new-member-suggestion-panel";

    /** The class applied to each suggestion item. */
    protected suggestionClass = "team-admin__new-member-suggestion";

    /** @inheritdoc */
    protected async fetchSuggestions(queryText: string): Promise<IKeyValue[]> {
        const encodedQuery = encodeURIComponent(queryText);
        const suggestions: string[] = await $.get(`/accounts/teams/${this._teamName}/query-member/${encodedQuery}/`);
        return suggestions.map(suggestion => { return {key: suggestion, value: suggestion}});
    }

    /** @inheritdoc */
    protected async selectSuggestions(userEmail: string): Promise<void> {
        try {
            const data = {
                csrfmiddlewaretoken: Poster.getCrsf(),
                user_email: userEmail
            };
            const memberHtml = await $.post(`/accounts/teams/${this._teamName}/invite-member/`, data);
            MessageManager.info(`An email has been sent to ${userEmail}`);
            AllMembersHandler.instance().addPendingMemberToView(memberHtml);
        } catch (e) {
            MessageManager.error(`Something went wrong: ${e.statusText}`);
            console.log(e.responseText);
            return;
        }
    }
}