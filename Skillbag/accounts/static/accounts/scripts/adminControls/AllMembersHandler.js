"use strict";
class AllMembersHandler {
    constructor() {
        this._groupMembersContainer = document.getElementById("group-members-list");
        this._groupMembers = Array.from(this._groupMembersContainer.querySelectorAll(".team-admin__single-member"))
            .map(memberPanel => memberPanel)
            .map(memberPanel => new MemberController(memberPanel, false));
        this._pendingMembersContainer = document.getElementById("pending-members-list");
        this._pendingMembers = Array.from(this._pendingMembersContainer.querySelectorAll(".team-admin__single-member"))
            .map(memberPanel => memberPanel)
            .map(memberPanel => new MemberController(memberPanel, true));
    }
    static instance() {
        if (!this.inst)
            throw new Error("AllMembersHandler has not been initialized");
        return this.inst;
    }
    static initialize() {
        this.inst = new AllMembersHandler();
    }
    /**
     * Adds a member to the list of pending members.
     * @param memberHtml the member HTML created by the backend.
     */
    addPendingMemberToView(memberHtml) {
        const memberTile = this.elementFromHtmlString(memberHtml);
        this._pendingMembersContainer.appendChild(memberTile);
        this._pendingMembersContainer.style.display = "flex";
        this._pendingMembers.push(new MemberController(memberTile, true));
    }
    elementFromHtmlString(html) {
        const tmp = document.createElement("div");
        tmp.innerHTML = html;
        return tmp.firstElementChild;
    }
    /**
     * Removes a member from the list.
     * @param member the member to remove.
     * @param isPending a boolean indicating if the member is a pending member or a group member.
     */
    removeMemberFromView(member, isPending) {
        const array = isPending ? this._pendingMembers : this._groupMembers;
        const container = isPending ? this._pendingMembersContainer : this._groupMembersContainer;
        const memberIndex = array.indexOf(member);
        if (memberIndex === -1)
            throw Error("Could not find the specified element.");
        array.splice(memberIndex, 1);
        member.memberPanel.remove();
        if (array.length === 0)
            container.style.display = "none";
    }
}
AllMembersHandler.inst = null;
