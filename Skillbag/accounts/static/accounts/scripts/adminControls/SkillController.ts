
class SkillController {

    private readonly _skillPk: string;
    private readonly _teamName: string;
    public readonly skillPanel: HTMLElement;

    constructor(skillPanel: HTMLElement) {
        this.skillPanel = skillPanel;
        this._skillPk = skillPanel.dataset.skillPk!;
        this._teamName = skillPanel.dataset.teamName!;

        const deleteButton = skillPanel.querySelector(".team-admin__skill-remove-button") as HTMLDivElement;
        deleteButton.onclick = (e) =>
        {
            e.stopPropagation();
            this.removeSkillFromTeam();
        };
    }

    /** @inheritdoc */
    protected async removeSkillFromTeam(): Promise<void>
    {
        try
        {
            const data = {
                csrfmiddlewaretoken: Poster.getCrsf(),
                skill_pk: this._skillPk,
            };
            await $.post(`/accounts/teams/${this._teamName}/remove-skill/`, data);
            MessageManager.success(`The skill has been removed.`);
            AllSkillsHandler.instance().removeSkillFromView(this);
        } catch (e)
        {
            MessageManager.error(`Something went wrong: ${e.statusText}`);
            console.log(e.responseText);
            return;
        }
    }
}