"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class MemberController {
    constructor(memberPanel, isPending) {
        this._member = memberPanel.dataset.userEmail;
        this._teamName = memberPanel.dataset.teamName;
        this._isPending = isPending;
        this.memberPanel = memberPanel;
        this._adminButton = memberPanel.querySelector("[name='status-button--admin']");
        this._adminButton.onclick = () => this.toggleMemberAdminStatus(true);
        this._userButton = memberPanel.querySelector("[name='status-button--user']");
        this._userButton.onclick = () => this.toggleMemberAdminStatus(false);
        const deleteButton = memberPanel.querySelector(".team-admin__member-remove-button");
        deleteButton.onclick = () => this.removeMemberFromGroup();
    }
    toggleMemberAdminStatus(isAdmin) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = {
                    csrfmiddlewaretoken: Poster.getCrsf(),
                    user_email: this._member,
                };
                const removeResult = yield $.post(`/accounts/teams/${this._teamName}/toggle-admin/`, data);
                if (removeResult === "exit")
                    SiteManager.instance().load("PageTeamSkills", this._teamName);
            }
            catch (e) {
                MessageManager.error(`It is not possible to toggle this member's admin state. Check the console for more information.`);
                console.log(e);
                return;
            }
            const message = `${this._member} is now ${isAdmin ? "a normal user" : "an admin"}.`;
            MessageManager.success(message);
            this._adminButton.style.display = isAdmin ? "none" : "block";
            this._userButton.style.display = !isAdmin ? "none" : "block";
        });
    }
    removeMemberFromGroup() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = {
                    csrfmiddlewaretoken: Poster.getCrsf(),
                    user_email: this._member,
                };
                const removeResult = yield $.post(`/accounts/teams/${this._teamName}/remove-member/`, data);
                if (removeResult === "exit")
                    SiteManager.instance().goHome();
            }
            catch (e) {
                MessageManager.error(`It is not possible to remove this member from the team. Check the console for more information.`);
                console.log(e);
                return;
            }
            MessageManager.success(`${this._member} has been removed from the team`);
            AllMembersHandler.instance().removeMemberFromView(this, this._isPending);
        });
    }
}
