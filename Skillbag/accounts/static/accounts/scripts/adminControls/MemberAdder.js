"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class MemberAdder extends GenericAdder {
    constructor(teamName) {
        super("member-adder-input");
        /** The number of characters that will trigger the appearance of the container. */
        this.minInputLength = 4;
        /** The class applied to the suggestion container. */
        this.containerClass = "team-admin__new-member-suggestion-panel";
        /** The class applied to each suggestion item. */
        this.suggestionClass = "team-admin__new-member-suggestion";
        this._teamName = teamName;
    }
    /** @inheritdoc */
    fetchSuggestions(queryText) {
        return __awaiter(this, void 0, void 0, function* () {
            const encodedQuery = encodeURIComponent(queryText);
            const suggestions = yield $.get(`/accounts/teams/${this._teamName}/query-member/${encodedQuery}/`);
            return suggestions.map(suggestion => { return { key: suggestion, value: suggestion }; });
        });
    }
    /** @inheritdoc */
    selectSuggestions(userEmail) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = {
                    csrfmiddlewaretoken: Poster.getCrsf(),
                    user_email: userEmail
                };
                const memberHtml = yield $.post(`/accounts/teams/${this._teamName}/invite-member/`, data);
                MessageManager.info(`An email has been sent to ${userEmail}`);
                AllMembersHandler.instance().addPendingMemberToView(memberHtml);
            }
            catch (e) {
                MessageManager.error(`Something went wrong: ${e.statusText}`);
                console.log(e.responseText);
                return;
            }
        });
    }
}
