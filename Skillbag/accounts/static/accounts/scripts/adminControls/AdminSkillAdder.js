"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class AdminSkillAdder extends GenericAdder {
    constructor(teamName) {
        super("skill-adder-input");
        /** The number of characters that will trigger the appearance of the container. */
        this.minInputLength = 1;
        /** The class applied to the suggestion container. */
        this.containerClass = "team-admin__new-member-suggestion-panel";
        /** The class applied to each suggestion item. */
        this.suggestionClass = "team-admin__new-member-suggestion";
        this._teamName = teamName;
    }
    /** @inheritdoc */
    fetchSuggestions(queryText) {
        return __awaiter(this, void 0, void 0, function* () {
            const encodedQuery = encodeURIComponent(queryText);
            const suggestions = yield $.get(`/accounts/teams/${this._teamName}/query-skill/${encodedQuery}/`);
            return suggestions.map(data => { return { key: data.pk.toString(), value: `${data.name} (${data.category__name})` }; });
        });
    }
    /** @inheritdoc */
    selectSuggestions(skillPk) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = {
                    csrfmiddlewaretoken: Poster.getCrsf(),
                    skill_pk: skillPk,
                };
                const htmlSkill = yield $.post(`/accounts/teams/${this._teamName}/add-skill/`, data);
                MessageManager.success(`The skill has been added.`);
                AllSkillsHandler.instance().addSkillToView(htmlSkill);
            }
            catch (e) {
                MessageManager.error(`Something went wrong: ${e.statusText}`);
                console.log(e.responseText);
                return;
            }
        });
    }
}
