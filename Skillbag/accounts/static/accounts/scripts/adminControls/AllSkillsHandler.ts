
class AllSkillsHandler {
    private static inst: AllSkillsHandler | null = null;
    public static instance(): AllSkillsHandler
    {
        if (!this.inst) throw new Error("AllSkillsHandler has not been initialized");
        return this.inst;
    }

    public static initialize()
    {
        this.inst = new AllSkillsHandler();
    }

    private readonly _teamSkillsList: HTMLElement;
    private readonly _teamSkills: Array<SkillController>;

    private constructor()
    {
        this._teamSkillsList = document.getElementById("team-skills-list") as HTMLElement;
        this._teamSkills = Array.from(this._teamSkillsList.querySelectorAll(".team-admin__single-skill"))
            .map(memberPanel => memberPanel as HTMLElement)
            .map(skillPanel => new SkillController(skillPanel));
    }

    /**
    * Adds a skill to the team.
    * @param skillHtml the skill HTML created in the backend.
    */
    public addSkillToView(skillHtml: string)
    {
        const skillTile = this.elementFromHtmlString(skillHtml);

        this._teamSkillsList.appendChild(skillTile);
        this._teamSkills.push(new SkillController(skillTile));
    }

    private elementFromHtmlString(html: string): HTMLElement
    {
        const tmp = document.createElement("div");
        tmp.innerHTML = html;
        return tmp.firstElementChild as HTMLElement;
    }

    public removeSkillFromView(skill: SkillController): void {
        const skillIndex = this._teamSkills.indexOf(skill);
        if (skillIndex === -1) throw Error("Could not find the specified element.");

        this._teamSkills.splice(skillIndex, 1);
        skill.skillPanel.remove();
    }
}