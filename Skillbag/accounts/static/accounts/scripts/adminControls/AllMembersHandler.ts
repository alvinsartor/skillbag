
class AllMembersHandler {

    private static inst: AllMembersHandler | null = null;

    public static instance(): AllMembersHandler {
        if (!this.inst) throw new Error("AllMembersHandler has not been initialized");
        return this.inst;
    }

    public static initialize() {
        this.inst = new AllMembersHandler();
    }

    private readonly _groupMembersContainer: HTMLElement;
    private readonly _pendingMembersContainer: HTMLElement;
    private readonly _groupMembers: Array<MemberController>;
    private readonly _pendingMembers: Array<MemberController>;

    private constructor() {
        this._groupMembersContainer = document.getElementById("group-members-list") as HTMLElement;
        this._groupMembers = Array.from(this._groupMembersContainer.querySelectorAll(".team-admin__single-member"))
            .map(memberPanel => memberPanel as HTMLElement)
            .map(memberPanel => new MemberController(memberPanel, false));

        this._pendingMembersContainer = document.getElementById("pending-members-list") as HTMLElement;
        this._pendingMembers = Array.from(this._pendingMembersContainer.querySelectorAll(".team-admin__single-member"))
            .map(memberPanel => memberPanel as HTMLElement)
            .map(memberPanel => new MemberController(memberPanel, true));
    }


    /**
     * Adds a member to the list of pending members.
     * @param memberHtml the member HTML created by the backend.
     */
    public addPendingMemberToView(memberHtml: string) {
        const memberTile = this.elementFromHtmlString(memberHtml);

        this._pendingMembersContainer.appendChild(memberTile);
        this._pendingMembersContainer.style.display = "flex";
        this._pendingMembers.push(new MemberController(memberTile, true));
    }

    private elementFromHtmlString(html: string): HTMLElement {
        const tmp = document.createElement("div");
        tmp.innerHTML = html;
        return tmp.firstElementChild as HTMLElement;
    }

    /**
     * Removes a member from the list.
     * @param member the member to remove.
     * @param isPending a boolean indicating if the member is a pending member or a group member.
     */
    public removeMemberFromView(member: MemberController, isPending: boolean): void {
        const array = isPending ? this._pendingMembers : this._groupMembers;
        const container = isPending ? this._pendingMembersContainer : this._groupMembersContainer;
        const memberIndex = array.indexOf(member);
        if (memberIndex === -1) throw Error("Could not find the specified element.");

        array.splice(memberIndex, 1);
        member.memberPanel.remove();
        if (array.length === 0) container.style.display = "none";
    }
}