"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class SkillController {
    constructor(skillPanel) {
        this.skillPanel = skillPanel;
        this._skillPk = skillPanel.dataset.skillPk;
        this._teamName = skillPanel.dataset.teamName;
        const deleteButton = skillPanel.querySelector(".team-admin__skill-remove-button");
        deleteButton.onclick = (e) => {
            e.stopPropagation();
            this.removeSkillFromTeam();
        };
    }
    /** @inheritdoc */
    removeSkillFromTeam() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = {
                    csrfmiddlewaretoken: Poster.getCrsf(),
                    skill_pk: this._skillPk,
                };
                yield $.post(`/accounts/teams/${this._teamName}/remove-skill/`, data);
                MessageManager.success(`The skill has been removed.`);
                AllSkillsHandler.instance().removeSkillFromView(this);
            }
            catch (e) {
                MessageManager.error(`Something went wrong: ${e.statusText}`);
                console.log(e.responseText);
                return;
            }
        });
    }
}
