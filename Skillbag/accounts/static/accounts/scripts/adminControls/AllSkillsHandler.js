"use strict";
class AllSkillsHandler {
    constructor() {
        this._teamSkillsList = document.getElementById("team-skills-list");
        this._teamSkills = Array.from(this._teamSkillsList.querySelectorAll(".team-admin__single-skill"))
            .map(memberPanel => memberPanel)
            .map(skillPanel => new SkillController(skillPanel));
    }
    static instance() {
        if (!this.inst)
            throw new Error("AllSkillsHandler has not been initialized");
        return this.inst;
    }
    static initialize() {
        this.inst = new AllSkillsHandler();
    }
    /**
    * Adds a skill to the team.
    * @param skillHtml the skill HTML created in the backend.
    */
    addSkillToView(skillHtml) {
        const skillTile = this.elementFromHtmlString(skillHtml);
        this._teamSkillsList.appendChild(skillTile);
        this._teamSkills.push(new SkillController(skillTile));
    }
    elementFromHtmlString(html) {
        const tmp = document.createElement("div");
        tmp.innerHTML = html;
        return tmp.firstElementChild;
    }
    removeSkillFromView(skill) {
        const skillIndex = this._teamSkills.indexOf(skill);
        if (skillIndex === -1)
            throw Error("Could not find the specified element.");
        this._teamSkills.splice(skillIndex, 1);
        skill.skillPanel.remove();
    }
}
AllSkillsHandler.inst = null;
