
interface IFetchSkillsResponse {
    pk: number;
    name: string;
    category__name: string;
}

class AdminSkillAdder extends GenericAdder {
    private readonly _teamName: string;

    constructor(teamName: string) {
        super("skill-adder-input");
        this._teamName = teamName;
    }

    /** The number of characters that will trigger the appearance of the container. */
    protected minInputLength = 1;

    /** The class applied to the suggestion container. */
    protected containerClass = "team-admin__new-member-suggestion-panel";

    /** The class applied to each suggestion item. */
    protected suggestionClass = "team-admin__new-member-suggestion";

    /** @inheritdoc */
    protected async fetchSuggestions(queryText: string): Promise<IKeyValue[]> {
        const encodedQuery = encodeURIComponent(queryText);
        const suggestions: IFetchSkillsResponse[] = await $.get(`/accounts/teams/${this._teamName}/query-skill/${encodedQuery}/`);
        return suggestions.map(data => { return { key: data.pk.toString(), value: `${data.name} (${data.category__name})` }});
    }

    /** @inheritdoc */
    protected async selectSuggestions(skillPk: string): Promise<void> {
        try {
            const data = {
                csrfmiddlewaretoken: Poster.getCrsf(),
                skill_pk: skillPk,
            };
            const htmlSkill: string = await $.post(`/accounts/teams/${this._teamName}/add-skill/`, data);
            MessageManager.success(`The skill has been added.`);
            AllSkillsHandler.instance().addSkillToView(htmlSkill);
        } catch (e) {
            MessageManager.error(`Something went wrong: ${e.statusText}`);
            console.log(e.responseText);
            return;
        }
    }
}