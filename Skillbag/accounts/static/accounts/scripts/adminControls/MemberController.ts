
class MemberController {
    private readonly _member: string;
    private readonly _isPending: boolean;
    private readonly _teamName: string;
    public readonly memberPanel: HTMLElement;
    private readonly _adminButton: HTMLElement;
    private readonly _userButton: HTMLElement;

    constructor(memberPanel: HTMLElement, isPending: boolean) {
        this._member = memberPanel.dataset.userEmail!;
        this._teamName = memberPanel.dataset.teamName!;
        this._isPending = isPending;
        this.memberPanel = memberPanel;

        this._adminButton = memberPanel.querySelector("[name='status-button--admin']") as HTMLDivElement;
        this._adminButton.onclick = () => this.toggleMemberAdminStatus(true);

        this._userButton = memberPanel.querySelector("[name='status-button--user']") as HTMLDivElement;
        this._userButton.onclick = () => this.toggleMemberAdminStatus(false);

        const deleteButton = memberPanel.querySelector(".team-admin__member-remove-button") as HTMLDivElement;
        deleteButton.onclick = () => this.removeMemberFromGroup();
    }

    private async toggleMemberAdminStatus(isAdmin: boolean): Promise<void> {
        try {
            const data = {
                csrfmiddlewaretoken: Poster.getCrsf(),
                user_email: this._member,
            };
            const removeResult = await $.post(`/accounts/teams/${this._teamName}/toggle-admin/`, data);
            if (removeResult === "exit") SiteManager.instance().load("PageTeamSkills", this._teamName);
        } catch (e) {
            MessageManager.error(
                `It is not possible to toggle this member's admin state. Check the console for more information.`);
            console.log(e);
            return;
        }

        const message = `${this._member} is now ${isAdmin ? "a normal user" : "an admin"}.`;
        MessageManager.success(message);

        this._adminButton.style.display = isAdmin ? "none" : "block";
        this._userButton.style.display = !isAdmin ? "none" : "block";
    }

    private async removeMemberFromGroup(): Promise<void> {
        try {
            const data = {
                csrfmiddlewaretoken: Poster.getCrsf(),
                user_email: this._member,
            };
            const removeResult = await $.post(`/accounts/teams/${this._teamName}/remove-member/`, data);
            if (removeResult === "exit") SiteManager.instance().goHome();
        } catch (e) {
            MessageManager.error(
                `It is not possible to remove this member from the team. Check the console for more information.`);
            console.log(e);
            return;
        }

        MessageManager.success(`${this._member} has been removed from the team`);
        AllMembersHandler.instance().removeMemberFromView(this, this._isPending);
    }

}