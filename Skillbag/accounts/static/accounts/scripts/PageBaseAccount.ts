
abstract class PageBaseAccount extends Page
{
    /** The identifier of the tab associated to this account page. */
    protected abstract tabId: string;

    /** The url used to fetch this tab. */
    protected abstract tabUrl: string;

    public async initialize(): Promise<void>
    {
        await this.loadPage();
    }

    /** @inheritdoc  */
    protected initializeScripts(): Promise<void>
    {
        const currentTab = document.getElementById(this.tabId) as HTMLElement;
        if (!currentTab) throw Error(`Cannot find tab with the following ID: '${this.tabId}'`);
        currentTab.classList.add("account-edit__single-tab--selected");
        this.addListenersToTabs();
        this.addTabScripts();

        return Promise.resolve();
    }

    private addListenersToTabs(): void
    {
        Array.from(document.querySelectorAll(".account-edit__single-tab"))
            .forEach(tab =>
            {
                if (!tab.classList.contains("account-edit__single-tab--selected"))
                {
                    const tabElement = tab as HTMLElement;
                    const destinationPage = tabElement.dataset.page as string;
                    tabElement.onclick = () => { SiteManager.instance().load(destinationPage); };
                }
            });
    }

    /** Function implemented by each tab that allows individual components to be initialized */
    protected abstract addTabScripts(): void;

    /** @inheritdoc  */
    protected async loadHtml(): Promise<string>
    {
        return await $.get(this.tabUrl);
    }

    /** @inheritdoc  */
    protected internalDispose(): Promise<void>
    {
        return Promise.resolve();
    }
}