"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageChangePassword extends PageBaseAccount {
    constructor() {
        super(...arguments);
        this.tabId = "tab-change-password";
        this.tabUrl = "/accounts/change-password/";
    }
    /** @inheritdoc */
    addTabScripts() {
        const form = document.getElementById("change-password-form");
        form.onsubmit = (e) => this.submitForm(e);
    }
    submitForm(e) {
        return __awaiter(this, void 0, void 0, function* () {
            e.preventDefault();
            const result = yield $.post("/accounts/change-password/", $("#change-password-form").serialize());
            // no errors, we reload the page
            if (result.errors === undefined) {
                MessageManager.success("The password has been updated");
                SiteManager.instance().reload();
                return;
            }
            // errors, we display them
            const formErrors = document.getElementById("form-errors");
            formErrors.innerHTML = "";
            const errors = result.errors;
            errors.forEach(error => formErrors.innerHTML += `<div class="account-form__single-error">- ${error}</div>`);
        });
    }
}
