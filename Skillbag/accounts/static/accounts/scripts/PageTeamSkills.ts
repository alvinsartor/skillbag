
class PageTeamSkills extends PageSkills {

    private _teamUrlName: string = "NotFound";

    /** @inheritdoc */
    protected get url(): string { return `/accounts/teams/${this._teamUrlName}`; }

    /** @inheritdoc  */
    public async initialize(...args: any[]): Promise<void> {
        [this._teamUrlName] = args;
        await this.loadPage();
    }
}