
class PageTeamAdmin extends Page {
    private _teamUrlName: string = "NotFound";
    private _memberAdder: MemberAdder | null = null;
    private _skillAdder: AdminSkillAdder | null = null;

    public async initialize(...args: any[]): Promise<void> {
        [this._teamUrlName] = args;
        await this.loadPage();
    }

    /** @inheritdoc  */
    protected initializeScripts(): Promise<void> {
        this._memberAdder = new MemberAdder(this._teamUrlName);
        this._skillAdder = new AdminSkillAdder(this._teamUrlName);
        AllMembersHandler.initialize();
        AllSkillsHandler.initialize();

        return Promise.resolve();
    }

    /** @inheritdoc  */
    protected async loadHtml(): Promise<string> {
        return await $.get(`/accounts/teams/${this._teamUrlName}/admin`);
    }

    /** @inheritdoc  */
    protected internalDispose(): Promise<void> {
        return Promise.resolve();
    }
}