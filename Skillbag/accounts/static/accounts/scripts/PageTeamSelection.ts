
class PageTeamSelection extends Page {

    private _teamAdder: TeamAdder | null = null;

    /** @inheritdoc  */
    public async initialize(...args: any[]): Promise<void> {
        await this.loadPage();
    }

    /** @inheritdoc  */
    protected initializeScripts(): Promise<void> {
        this.addTeamButtonsListeners();
        this.addNewTeamButtonListener();
        return Promise.resolve();
    }

    /** @inheritdoc  */
    protected async loadHtml(): Promise<string> {
        return await $.get("/accounts/teams/");
    }

    /** @inheritdoc  */
    protected internalDispose(): Promise<void> {
        return Promise.resolve();
    }

    private addTeamButtonsListeners(): void {
        Array.from(document.querySelectorAll(".team-selection__team-button"))
            .map(e => e as HTMLDivElement)
            .forEach(e => {
                const teamUrlName = e.getAttribute("data-team-url-name") as string;
                e.onclick = () => SiteManager.instance().load("PageTeamSkills", teamUrlName);
            });
    }

    private addNewTeamButtonListener(): void {
        const newTeamButton = document.getElementById("add-new-team-button") as HTMLDivElement;
        newTeamButton.onclick = () => {
            this._teamAdder = new TeamAdder();
            this._teamAdder.loadEditorForm();
        };
    }
}