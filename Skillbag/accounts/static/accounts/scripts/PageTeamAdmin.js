"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageTeamAdmin extends Page {
    constructor() {
        super(...arguments);
        this._teamUrlName = "NotFound";
        this._memberAdder = null;
        this._skillAdder = null;
    }
    initialize(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            [this._teamUrlName] = args;
            yield this.loadPage();
        });
    }
    /** @inheritdoc  */
    initializeScripts() {
        this._memberAdder = new MemberAdder(this._teamUrlName);
        this._skillAdder = new AdminSkillAdder(this._teamUrlName);
        AllMembersHandler.initialize();
        AllSkillsHandler.initialize();
        return Promise.resolve();
    }
    /** @inheritdoc  */
    loadHtml() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.get(`/accounts/teams/${this._teamUrlName}/admin`);
        });
    }
    /** @inheritdoc  */
    internalDispose() {
        return Promise.resolve();
    }
}
