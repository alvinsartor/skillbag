"use strict";
class PageEmailPreferences extends PageBaseAccount {
    constructor() {
        super(...arguments);
        this.tabId = "tab-email-preferences";
        this.tabUrl = "/accounts/email-preferences/";
    }
    /** @inheritdoc */
    addTabScripts() {
    }
}
