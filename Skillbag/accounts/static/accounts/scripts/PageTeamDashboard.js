"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageTeamDashboard extends Page {
    constructor() {
        super(...arguments);
        this._teamUrlName = "NotFound";
        /** The masonry script, if this is loaded. */
        this.masonry = null;
    }
    initialize(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            [this._teamUrlName] = args;
            yield this.loadPage();
        });
    }
    /** @inheritdoc  */
    initializeScripts() {
        // on mobile we do not load masonry as the tiles are just displayed in column.
        if (window.matchMedia("(min-width: 640px)").matches) {
            this.masonry = new Masonry(".team-overview__skills-container", {
                itemSelector: ".team-dashboard-skill",
                fitWidth: true,
                horizontalOrder: true,
                transitionDuration: "0s",
            });
        }
        return Promise.resolve();
    }
    /** @inheritdoc  */
    loadHtml() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.get(`/accounts/teams/${this._teamUrlName}/dashboard`);
        });
    }
    /** @inheritdoc  */
    internalDispose() {
        return Promise.resolve();
    }
}
