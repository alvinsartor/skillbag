
class PageTeamDashboard extends Page {
    private _teamUrlName: string = "NotFound";

    /** The masonry script, if this is loaded. */
    protected masonry: Masonry | null = null;

    public async initialize(...args: any[]): Promise<void> {
        [this._teamUrlName] = args;
        await this.loadPage();
    }

    /** @inheritdoc  */
    protected initializeScripts(): Promise<void> {
        // on mobile we do not load masonry as the tiles are just displayed in column.
        if (window.matchMedia("(min-width: 640px)").matches)
        {
            this.masonry = new Masonry(".team-overview__skills-container",
                {
                    itemSelector: ".team-dashboard-skill",
                    fitWidth: true,
                    horizontalOrder: true,
                    transitionDuration: "0s",
                });
        }

        return Promise.resolve();
    }

    /** @inheritdoc  */
    protected async loadHtml(): Promise<string> {
        return await $.get(`/accounts/teams/${this._teamUrlName}/dashboard`);
    }

    /** @inheritdoc  */
    protected internalDispose(): Promise<void> {
        return Promise.resolve();
    }
}