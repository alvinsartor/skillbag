"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageBaseAccount extends Page {
    initialize() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.loadPage();
        });
    }
    /** @inheritdoc  */
    initializeScripts() {
        const currentTab = document.getElementById(this.tabId);
        if (!currentTab)
            throw Error(`Cannot find tab with the following ID: '${this.tabId}'`);
        currentTab.classList.add("account-edit__single-tab--selected");
        this.addListenersToTabs();
        this.addTabScripts();
        return Promise.resolve();
    }
    addListenersToTabs() {
        Array.from(document.querySelectorAll(".account-edit__single-tab"))
            .forEach(tab => {
            if (!tab.classList.contains("account-edit__single-tab--selected")) {
                const tabElement = tab;
                const destinationPage = tabElement.dataset.page;
                tabElement.onclick = () => { SiteManager.instance().load(destinationPage); };
            }
        });
    }
    /** @inheritdoc  */
    loadHtml() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.get(this.tabUrl);
        });
    }
    /** @inheritdoc  */
    internalDispose() {
        return Promise.resolve();
    }
}
