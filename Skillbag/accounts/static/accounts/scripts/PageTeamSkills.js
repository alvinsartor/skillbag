"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageTeamSkills extends PageSkills {
    constructor() {
        super(...arguments);
        this._teamUrlName = "NotFound";
    }
    /** @inheritdoc */
    get url() { return `/accounts/teams/${this._teamUrlName}`; }
    /** @inheritdoc  */
    initialize(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            [this._teamUrlName] = args;
            yield this.loadPage();
        });
    }
}
