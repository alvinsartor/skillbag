"use strict";
class TeamAdder extends BaseEditor {
    constructor() {
        super();
        /** @inheritdoc */
        this.title = "Add Team";
    }
    /** @inheritdoc */
    get formUrl() { return `/accounts/teams/add/`; }
    /** @inheritdoc */
    setAdditionalListeners(form) {
        const teamNameInput = form.querySelector('input[name="name"]');
        const teamUrlNameInput = form.querySelector('input[name="url_name"]');
        const urlPreview = this.appendUrlPreviewToForm(form);
        teamNameInput.oninput = () => {
            const sanitizedTeamName = this.sanitizeUrlName(teamNameInput.value);
            teamUrlNameInput.value = sanitizedTeamName;
            urlPreview.innerText = this.composeFullUrl(sanitizedTeamName);
        };
        teamUrlNameInput.oninput = () => {
            const sanitizedTeamName = this.sanitizeUrlName(teamUrlNameInput.value);
            teamUrlNameInput.value = sanitizedTeamName;
            urlPreview.innerText = this.composeFullUrl(sanitizedTeamName);
        };
    }
    appendUrlPreviewToForm(form) {
        const urlPreview = document.createElement("div");
        urlPreview.style.marginTop = "25px";
        urlPreview.innerText = this.composeFullUrl("");
        form.appendChild(urlPreview);
        return urlPreview;
    }
    sanitizeUrlName(urlName) {
        return encodeURIComponent(urlName.toLowerCase().split(" ").join("_")).slice(0, 60);
    }
    composeFullUrl(urlName) {
        return `${location.host}/bag/PageTeamSkills/${urlName}/`;
    }
}
