
class TeamAdder extends BaseEditor {

    /** @inheritdoc */
    protected title: string = "Add Team";

    /** @inheritdoc */
    protected get formUrl() { return `/accounts/teams/add/`; }

    public constructor() {
        super();
    }

    /** @inheritdoc */
    protected setAdditionalListeners(form: HTMLFormElement): void {
        const teamNameInput = form.querySelector('input[name="name"]') as HTMLInputElement;
        const teamUrlNameInput = form.querySelector('input[name="url_name"]') as HTMLInputElement;
        const urlPreview = this.appendUrlPreviewToForm(form);

        teamNameInput.oninput = () => {
            const sanitizedTeamName = this.sanitizeUrlName(teamNameInput.value);
            teamUrlNameInput.value = sanitizedTeamName;
            urlPreview.innerText = this.composeFullUrl(sanitizedTeamName);
        }

        teamUrlNameInput.oninput = () => {
            const sanitizedTeamName = this.sanitizeUrlName(teamUrlNameInput.value);
            teamUrlNameInput.value = sanitizedTeamName;
            urlPreview.innerText = this.composeFullUrl(sanitizedTeamName);
        }
    }

    private appendUrlPreviewToForm(form: HTMLFormElement): HTMLDivElement {
        const urlPreview = document.createElement("div");
        urlPreview.style.marginTop = "25px";
        urlPreview.innerText = this.composeFullUrl("");
        form.appendChild(urlPreview);
        return urlPreview;
    }

    private sanitizeUrlName(urlName: string): string {
        return encodeURIComponent(urlName.toLowerCase().split(" ").join("_")).slice(0, 60);
    }

    private composeFullUrl(urlName: string): string {
        return `${location.host}/bag/PageTeamSkills/${urlName}/`;
    }
}