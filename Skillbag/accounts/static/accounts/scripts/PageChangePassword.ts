
class PageChangePassword extends PageBaseAccount {

    protected tabId = "tab-change-password";
    protected tabUrl = "/accounts/change-password/"

    /** @inheritdoc */
    protected addTabScripts(): void
    {        
        const form = document.getElementById("change-password-form") as HTMLFormElement;
        form.onsubmit = (e) => this.submitForm(e);
    }

    private async submitForm(e: Event): Promise<void>
    {
        e.preventDefault();
        const result = await $.post("/accounts/change-password/", $("#change-password-form").serialize());

        // no errors, we reload the page
        if (result.errors === undefined)
        {
            MessageManager.success("The password has been updated");
            SiteManager.instance().reload();
            return;
        } 

        // errors, we display them
        const formErrors = document.getElementById("form-errors") as HTMLElement;
        formErrors.innerHTML = "";

        const errors = result.errors as string[];
        errors.forEach(error => formErrors.innerHTML += `<div class="account-form__single-error">- ${error}</div>`);
    }
}