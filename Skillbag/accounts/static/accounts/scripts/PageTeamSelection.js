"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageTeamSelection extends Page {
    constructor() {
        super(...arguments);
        this._teamAdder = null;
    }
    /** @inheritdoc  */
    initialize(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.loadPage();
        });
    }
    /** @inheritdoc  */
    initializeScripts() {
        this.addTeamButtonsListeners();
        this.addNewTeamButtonListener();
        return Promise.resolve();
    }
    /** @inheritdoc  */
    loadHtml() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.get("/accounts/teams/");
        });
    }
    /** @inheritdoc  */
    internalDispose() {
        return Promise.resolve();
    }
    addTeamButtonsListeners() {
        Array.from(document.querySelectorAll(".team-selection__team-button"))
            .map(e => e)
            .forEach(e => {
            const teamUrlName = e.getAttribute("data-team-url-name");
            e.onclick = () => SiteManager.instance().load("PageTeamSkills", teamUrlName);
        });
    }
    addNewTeamButtonListener() {
        const newTeamButton = document.getElementById("add-new-team-button");
        newTeamButton.onclick = () => {
            this._teamAdder = new TeamAdder();
            this._teamAdder.loadEditorForm();
        };
    }
}
