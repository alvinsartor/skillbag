
from django.urls import path
from search import views

app_name = 'search'

urlpatterns = [
    path('', views.SearchView.as_view()),    

    path('skills/count/<str:query>/', views.SkillsSearchCountView.as_view()),    
    path('subskills/count/<str:query>/', views.SubskillsSearchCountView.as_view()),    
    path('categories/count/<str:query>/', views.CategoriesSearchCountView.as_view()),    
    path('macrocategories/count/<str:query>/', views.MacrocategoriesSearchCountView.as_view()),    
]
