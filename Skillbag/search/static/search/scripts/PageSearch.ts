
class PageSearch extends Page {

    private _searchArea: HTMLElement | null = null;
    private _searchContainer: HTMLElement | null = null;

    private _skillsSearchElement: SearchElement | null = null;
    private _subskillsSearchElement: SearchElement | null = null;
    private _categoriesSearchElement: SearchElement | null = null;
    private _macrocategoriesSearchElement: SearchElement | null = null;
    private _searchTypes: SearchElement[] = [];

    /** @inheritdoc  */
    public async initialize(...args: any[]): Promise<void> {
        await this.loadPage();
    }

    /** @inheritdoc  */
    protected initializeScripts(): Promise<void> {
        this._searchArea = document.getElementById("search-area") as HTMLElement;
        this._searchContainer = document.getElementById("search-area-results") as HTMLElement;

        this.addSearchElements();
        this.addSearchBox();
        return Promise.resolve();
    }

    /** @inheritdoc  */
    protected async loadHtml(): Promise<string> {
        return await $.get("/search/");
    }

    /** @inheritdoc  */
    protected internalDispose(): Promise<void> {

        return Promise.resolve();
    }

    private addSearchElements(): void {
        this.addSkillsSearchBox();
        this.addSubskillsSearchBox();
        this.addCategoriesSearchBox();
        this.addMacrocategoriesSearchBox();

        this._searchTypes = [
            this._skillsSearchElement!,
            this._subskillsSearchElement!,
            this._categoriesSearchElement!,
            this._macrocategoriesSearchElement!,
        ];

        const unsubscribeOthers = (id: string) => this._searchTypes.forEach(s => { if (s.id !== id) s.deselect(); });
        this._searchTypes.forEach(s => s.elementSelected.subscribe((id: string) => unsubscribeOthers(id)));
    }

    private addSearchBox() {
        const searchBox = new SearchHandler(350);
        searchBox.focus();
        searchBox.searchTextChanged.subscribe(async (value: string) => {
            if (value.trim() !== "") this._searchArea!.style.marginTop = "0";
            await Promise.all(this._searchTypes.filter(s => s.queryResults(value)));
            if (value.trim() === "") this._searchArea!.style.marginTop = "25vh";
        });
    }

    private addSkillsSearchBox(): void {
        const urlComposer = (query: string) => `/skills/search/${query}` + (query ? "/" : "");
        const resultFetcher = new SkillsResultFetcher(this._searchContainer!, urlComposer);
        resultFetcher.onAsyncProgress.subscribe(progressPercentage => this.setLoaderPercentage(progressPercentage));

        const countUrlComposer = (query: string) => `/search/skills/count/${query}/`;
        this._skillsSearchElement = new SearchElement("js-skills-number-result", countUrlComposer, resultFetcher, true);
    }

    private addSubskillsSearchBox(): void {
        const urlComposer = (query: string) => `/skills/subskills/search/${query}` + (query ? "/" : "");
        const resultFetcher = new SubskillsResultFetcher(this._searchContainer!, urlComposer);
        resultFetcher.onAsyncProgress.subscribe(progressPercentage => this.setLoaderPercentage(progressPercentage));

        const countUrlComposer = (query: string) => `/search/subskills/count/${query}/`;
        this._subskillsSearchElement = new SearchElement("js-subskills-number-result", countUrlComposer, resultFetcher);
    }

    private addCategoriesSearchBox(): void {
        const urlComposer = (query: string) => `/categories/search/${query}` + (query ? "/" : "");
        const resultFetcher = new CategoriesResultFetcher(this._searchContainer!, urlComposer, "PageCategorySkills");
        resultFetcher.onAsyncProgress.subscribe(progressPercentage => this.setLoaderPercentage(progressPercentage));

        const countUrlComposer = (query: string) => `/search/categories/count/${query}/`;
        this._categoriesSearchElement =
            new SearchElement("js-categories-number-result", countUrlComposer, resultFetcher);
    }

    private addMacrocategoriesSearchBox(): void {
        const urlComposer = (query: string) => `/categories/macrocategories/search/${query}` + (query ? "/" : "");
        const resultFetcher = new CategoriesResultFetcher(this._searchContainer!, urlComposer, "PageCategories");
        resultFetcher.onAsyncProgress.subscribe(progressPercentage => this.setLoaderPercentage(progressPercentage));

        const countUrlComposer = (query: string) => `/search/macrocategories/count/${query}/`;
        this._macrocategoriesSearchElement =
            new SearchElement("js-macrocategories-number-result", countUrlComposer, resultFetcher);
    }
}