
abstract class SearchResultFetcher implements IDisposable {

    protected readonly searchContainer: HTMLElement;
    private readonly _searchUrlBuilder: (query: string) => string;

    private readonly _onAsyncProgress = new LiteEvent<number>();

    /** Event raised whenever the completion state of this fetcher changes. */
    public get onAsyncProgress(): ILiteEvent<number> { return this._onAsyncProgress.expose(); }

    protected constructor(searchContainer: HTMLElement, searchUrlBuilder: (query: string) => string) {
        this.searchContainer = searchContainer;
        this._searchUrlBuilder = searchUrlBuilder;
    }

    /** Fetches a search result using the given query and displays it. */
    public async searchAndDisplayResults(query: string): Promise<void> {
        this._onAsyncProgress.trigger(0);
        const url = this._searchUrlBuilder(query.trim());

        const fetchResult = await $.get(url) as string;
        this.searchContainer.innerHTML = fetchResult;
        this._onAsyncProgress.trigger(75);

        await this.postFetchOperations();
        this._onAsyncProgress.trigger(100);
    }

    /** Operations on retrieved data, after this has been fetched. */
    protected abstract postFetchOperations(): Promise<void>;

    /** Function called whenever this search-result is deactivated in favor of another. */
    public abstract dispose(): Promise<void>;
}