"use strict";
class SkillsResultFetcher extends SearchResultFetcher {
    constructor(searchContainer, searchUrlBuilder) {
        super(searchContainer, searchUrlBuilder);
        /** The skills loaded by the page. */
        this.initializedSkills = {};
        /** The masonry script, if this is loaded. */
        this.masonry = null;
    }
    postFetchOperations() {
        const skillsListContainer = document.querySelector('[name="loaded-skills"]');
        const skillsList = JSON.parse(skillsListContainer.innerText);
        skillsList.forEach((pk) => {
            if (!this.initializedSkills[pk]) {
                this.initializedSkills[pk] = new SkillTile(pk);
            }
        });
        // on mobile we do not load masonry as the tiles are just displayed in column.
        if (window.matchMedia("(min-width: 640px)").matches && skillsList.length > 0) {
            this.masonry = new Masonry(".skills-container", {
                itemSelector: ".tile-skill",
                fitWidth: true,
                horizontalOrder: true,
                transitionDuration: "0.2s",
            });
        }
        return Promise.resolve();
    }
    dispose() {
        const keys = Object.keys(this.initializedSkills);
        keys.forEach(k => delete this.initializedSkills[k]);
        return Promise.resolve();
    }
}
