"use strict";
class CategoriesResultFetcher extends SearchResultFetcher {
    constructor(searchContainer, searchUrlBuilder, pageToLoadOnClick) {
        super(searchContainer, searchUrlBuilder);
        this._pageToLoadOnClick = pageToLoadOnClick;
    }
    /** @inheritdoc */
    postFetchOperations() {
        const isUserAuthenticated = SiteManager.instance().isUserAuthenticated();
        Array.from(document.querySelectorAll("[name='category-tile']"))
            .map(e => e)
            .forEach(e => {
            e.onclick = () => {
                isUserAuthenticated
                    ? SiteManager.instance().load(this._pageToLoadOnClick, e.dataset.id)
                    : SiteManager.toLoginScreen();
            };
        });
        return Promise.resolve();
    }
    /** @inheritdoc */
    dispose() {
        return Promise.resolve();
    }
}
