
class SubskillsResultFetcher extends SearchResultFetcher {

    private readonly _subskills: { [id: string]: SubskillTile; } = {}

    public constructor(searchContainer: HTMLElement, searchUrlBuilder: (query: string) => string)
    {
        super(searchContainer, searchUrlBuilder);
    }

    public postFetchOperations(): Promise<void> {

        Array.from(this.searchContainer.querySelectorAll('div[name="subskill"]'))
            .map(tile => tile as HTMLElement)
            .forEach(tile =>
            {
                const subskill = new SubskillTile(tile);
                this._subskills[subskill.pk] = subskill;
            });

        return Promise.resolve();
    }

    public dispose(): Promise<void> {
        return Promise.resolve();
    }
}