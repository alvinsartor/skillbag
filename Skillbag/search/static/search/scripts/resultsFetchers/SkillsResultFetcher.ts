
class SkillsResultFetcher extends SearchResultFetcher {

    /** The skills loaded by the page. */
    protected readonly initializedSkills: { [pk: string]: SkillTile; } = {};

    /** The masonry script, if this is loaded. */
    protected masonry: Masonry | null = null;

    public constructor(searchContainer: HTMLElement, searchUrlBuilder: (query: string) => string)
    {
        super(searchContainer, searchUrlBuilder);
    }

    public postFetchOperations(): Promise<void> {

        const skillsListContainer = document.querySelector('[name="loaded-skills"]') as HTMLElement;
        const skillsList = JSON.parse(skillsListContainer.innerText) as string[];
        skillsList.forEach((pk: string) =>
        {
            if (!this.initializedSkills[pk])
            {
                this.initializedSkills[pk] = new SkillTile(pk);
            }
        });

        // on mobile we do not load masonry as the tiles are just displayed in column.
        if (window.matchMedia("(min-width: 640px)").matches && skillsList.length > 0)
        {
            this.masonry = new Masonry(".skills-container",
                {
                    itemSelector: ".tile-skill",
                    fitWidth: true,
                    horizontalOrder: true,
                    transitionDuration: "0.2s",
                });
        }

        return Promise.resolve();
    }

    public dispose(): Promise<void> {
        const keys = Object.keys(this.initializedSkills);
        keys.forEach(k => delete this.initializedSkills[k]);
        return Promise.resolve();
    }
}