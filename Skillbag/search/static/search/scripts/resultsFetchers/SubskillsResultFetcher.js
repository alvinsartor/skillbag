"use strict";
class SubskillsResultFetcher extends SearchResultFetcher {
    constructor(searchContainer, searchUrlBuilder) {
        super(searchContainer, searchUrlBuilder);
        this._subskills = {};
    }
    postFetchOperations() {
        Array.from(this.searchContainer.querySelectorAll('div[name="subskill"]'))
            .map(tile => tile)
            .forEach(tile => {
            const subskill = new SubskillTile(tile);
            this._subskills[subskill.pk] = subskill;
        });
        return Promise.resolve();
    }
    dispose() {
        return Promise.resolve();
    }
}
