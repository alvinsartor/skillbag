
class SearchElement implements IDisposable {
    public readonly id: string;
    private readonly _typeContainer: HTMLElement;
    private readonly _typeSearchCount: HTMLElement;
    private readonly _resultFetcher: SearchResultFetcher;
    private readonly _searchUrlBuilder: (query: string) => string;
    private _lastQuery: string = "";

    private readonly _onElementSelected = new LiteEvent<string>();

    /** Event raised whenever this element is selected. */
    public get elementSelected(): ILiteEvent<string> { return this._onElementSelected.expose(); }

    /**
     * Creates a new instance of SearchElement.
     * @param searchTypeBoxId the ID of the HTML container for this SearchElement.
     * @param searchUrlBuilder the function used to build the url used by this searchElement to retrieve the number of results with the given query.
     * @param resultFetcher the SearchResultFetcher associated to this searchElement.
     * @param preSelected defines whether this element should be selected by default.
     */
    constructor(
        searchTypeBoxId: string,
        searchUrlBuilder: (query: string) => string,
        resultFetcher: SearchResultFetcher,
        preSelected: boolean = false) {
        this.id = searchTypeBoxId;
        this._resultFetcher = resultFetcher;
        this._searchUrlBuilder = searchUrlBuilder;

        this._typeContainer = document.getElementById(this.id) as HTMLElement;
        this._typeContainer.onclick = () => this.select();
        this._typeSearchCount = this._typeContainer.querySelector(".l-search-type__item-value") as HTMLElement;

        if (preSelected) this.select();
    }

    /** Uses the specified query to count the results and, if this element is selected, to fetch and display them. */
    public async queryResults(query: string): Promise<void> {
        if (this._lastQuery === query.trim()) return;

        this._lastQuery = query.trim();
        this._lastQuery === "" ? this.setNumberOfResults() : void this.retrieveAndSetResultsCount(query);

        if (!this.isSelected) return;
        await this._resultFetcher.dispose();
        await this._resultFetcher.searchAndDisplayResults(query);
    }

    private async retrieveAndSetResultsCount(query: string): Promise<void> {
        const url = this._searchUrlBuilder(query);
        const resultsCount = await $.get(url) as string;
        this.setNumberOfResults(Number(resultsCount));
    }

    private setNumberOfResults(resultsCount?: number): void {
        this._typeSearchCount.innerText = resultsCount === undefined ? "0" : resultsCount.toString();

        const noResults = resultsCount ? false : true;
        this._typeContainer.classList.toggle("l-search-type__item--no-results", noResults);
    }

    private select(): void {
        if (this.isSelected) return;

        this._typeContainer.classList.toggle("l-search-type__item--selected", true);
        this._onElementSelected.trigger(this.id);
        void this._resultFetcher.searchAndDisplayResults(this._lastQuery);
    }

    /** Sets the current SearchElement as deselected. */
    public deselect(): void {
        this._typeContainer.classList.toggle("l-search-type__item--selected", false);
        this.dispose();
    }

    private get isSelected(): boolean {
        return this._typeContainer.classList.contains("l-search-type__item--selected");
    }

    /** @inheritdoc */
    public async dispose(): Promise<void> {
        return await this._resultFetcher.dispose();
    }
}