"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class SearchElement {
    /**
     * Creates a new instance of SearchElement.
     * @param searchTypeBoxId the ID of the HTML container for this SearchElement.
     * @param searchUrlBuilder the function used to build the url used by this searchElement to retrieve the number of results with the given query.
     * @param resultFetcher the SearchResultFetcher associated to this searchElement.
     * @param preSelected defines whether this element should be selected by default.
     */
    constructor(searchTypeBoxId, searchUrlBuilder, resultFetcher, preSelected = false) {
        this._lastQuery = "";
        this._onElementSelected = new LiteEvent();
        this.id = searchTypeBoxId;
        this._resultFetcher = resultFetcher;
        this._searchUrlBuilder = searchUrlBuilder;
        this._typeContainer = document.getElementById(this.id);
        this._typeContainer.onclick = () => this.select();
        this._typeSearchCount = this._typeContainer.querySelector(".l-search-type__item-value");
        if (preSelected)
            this.select();
    }
    /** Event raised whenever this element is selected. */
    get elementSelected() { return this._onElementSelected.expose(); }
    /** Uses the specified query to count the results and, if this element is selected, to fetch and display them. */
    queryResults(query) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this._lastQuery === query.trim())
                return;
            this._lastQuery = query.trim();
            this._lastQuery === "" ? this.setNumberOfResults() : void this.retrieveAndSetResultsCount(query);
            if (!this.isSelected)
                return;
            yield this._resultFetcher.dispose();
            yield this._resultFetcher.searchAndDisplayResults(query);
        });
    }
    retrieveAndSetResultsCount(query) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = this._searchUrlBuilder(query);
            const resultsCount = yield $.get(url);
            this.setNumberOfResults(Number(resultsCount));
        });
    }
    setNumberOfResults(resultsCount) {
        this._typeSearchCount.innerText = resultsCount === undefined ? "0" : resultsCount.toString();
        const noResults = resultsCount ? false : true;
        this._typeContainer.classList.toggle("l-search-type__item--no-results", noResults);
    }
    select() {
        if (this.isSelected)
            return;
        this._typeContainer.classList.toggle("l-search-type__item--selected", true);
        this._onElementSelected.trigger(this.id);
        void this._resultFetcher.searchAndDisplayResults(this._lastQuery);
    }
    /** Sets the current SearchElement as deselected. */
    deselect() {
        this._typeContainer.classList.toggle("l-search-type__item--selected", false);
        this.dispose();
    }
    get isSelected() {
        return this._typeContainer.classList.contains("l-search-type__item--selected");
    }
    /** @inheritdoc */
    dispose() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._resultFetcher.dispose();
        });
    }
}
