"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class SearchResultFetcher {
    constructor(searchContainer, searchUrlBuilder) {
        this._onAsyncProgress = new LiteEvent();
        this.searchContainer = searchContainer;
        this._searchUrlBuilder = searchUrlBuilder;
    }
    /** Event raised whenever the completion state of this fetcher changes. */
    get onAsyncProgress() { return this._onAsyncProgress.expose(); }
    /** Fetches a search result using the given query and displays it. */
    searchAndDisplayResults(query) {
        return __awaiter(this, void 0, void 0, function* () {
            this._onAsyncProgress.trigger(0);
            const url = this._searchUrlBuilder(query.trim());
            const fetchResult = yield $.get(url);
            this.searchContainer.innerHTML = fetchResult;
            this._onAsyncProgress.trigger(75);
            yield this.postFetchOperations();
            this._onAsyncProgress.trigger(100);
        });
    }
}
