
class CategoriesResultFetcher extends SearchResultFetcher {
    private readonly _pageToLoadOnClick: string;

    public constructor(
        searchContainer: HTMLElement,
        searchUrlBuilder: (query: string) => string,
        pageToLoadOnClick: string) {
        super(searchContainer, searchUrlBuilder);
        this._pageToLoadOnClick = pageToLoadOnClick;
    }

    /** @inheritdoc */
    public postFetchOperations(): Promise<void> {
        const isUserAuthenticated = SiteManager.instance().isUserAuthenticated();

        Array.from(document.querySelectorAll("[name='category-tile']"))
            .map(e => e as HTMLDivElement)
            .forEach(e => {
                e.onclick = () => {
                    isUserAuthenticated
                        ? SiteManager.instance().load(this._pageToLoadOnClick, e.dataset.id)
                        : SiteManager.toLoginScreen();
                };
            });
        return Promise.resolve();
    }

    /** @inheritdoc */
    public dispose(): Promise<void> {
        return Promise.resolve();
    }
}