"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageSearch extends Page {
    constructor() {
        super(...arguments);
        this._searchArea = null;
        this._searchContainer = null;
        this._skillsSearchElement = null;
        this._subskillsSearchElement = null;
        this._categoriesSearchElement = null;
        this._macrocategoriesSearchElement = null;
        this._searchTypes = [];
    }
    /** @inheritdoc  */
    initialize(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.loadPage();
        });
    }
    /** @inheritdoc  */
    initializeScripts() {
        this._searchArea = document.getElementById("search-area");
        this._searchContainer = document.getElementById("search-area-results");
        this.addSearchElements();
        this.addSearchBox();
        return Promise.resolve();
    }
    /** @inheritdoc  */
    loadHtml() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.get("/search/");
        });
    }
    /** @inheritdoc  */
    internalDispose() {
        return Promise.resolve();
    }
    addSearchElements() {
        this.addSkillsSearchBox();
        this.addSubskillsSearchBox();
        this.addCategoriesSearchBox();
        this.addMacrocategoriesSearchBox();
        this._searchTypes = [
            this._skillsSearchElement,
            this._subskillsSearchElement,
            this._categoriesSearchElement,
            this._macrocategoriesSearchElement,
        ];
        const unsubscribeOthers = (id) => this._searchTypes.forEach(s => { if (s.id !== id)
            s.deselect(); });
        this._searchTypes.forEach(s => s.elementSelected.subscribe((id) => unsubscribeOthers(id)));
    }
    addSearchBox() {
        const searchBox = new SearchHandler(350);
        searchBox.focus();
        searchBox.searchTextChanged.subscribe((value) => __awaiter(this, void 0, void 0, function* () {
            if (value.trim() !== "")
                this._searchArea.style.marginTop = "0";
            yield Promise.all(this._searchTypes.filter(s => s.queryResults(value)));
            if (value.trim() === "")
                this._searchArea.style.marginTop = "25vh";
        }));
    }
    addSkillsSearchBox() {
        const urlComposer = (query) => `/skills/search/${query}` + (query ? "/" : "");
        const resultFetcher = new SkillsResultFetcher(this._searchContainer, urlComposer);
        resultFetcher.onAsyncProgress.subscribe(progressPercentage => this.setLoaderPercentage(progressPercentage));
        const countUrlComposer = (query) => `/search/skills/count/${query}/`;
        this._skillsSearchElement = new SearchElement("js-skills-number-result", countUrlComposer, resultFetcher, true);
    }
    addSubskillsSearchBox() {
        const urlComposer = (query) => `/skills/subskills/search/${query}` + (query ? "/" : "");
        const resultFetcher = new SubskillsResultFetcher(this._searchContainer, urlComposer);
        resultFetcher.onAsyncProgress.subscribe(progressPercentage => this.setLoaderPercentage(progressPercentage));
        const countUrlComposer = (query) => `/search/subskills/count/${query}/`;
        this._subskillsSearchElement = new SearchElement("js-subskills-number-result", countUrlComposer, resultFetcher);
    }
    addCategoriesSearchBox() {
        const urlComposer = (query) => `/categories/search/${query}` + (query ? "/" : "");
        const resultFetcher = new CategoriesResultFetcher(this._searchContainer, urlComposer, "PageCategorySkills");
        resultFetcher.onAsyncProgress.subscribe(progressPercentage => this.setLoaderPercentage(progressPercentage));
        const countUrlComposer = (query) => `/search/categories/count/${query}/`;
        this._categoriesSearchElement =
            new SearchElement("js-categories-number-result", countUrlComposer, resultFetcher);
    }
    addMacrocategoriesSearchBox() {
        const urlComposer = (query) => `/categories/macrocategories/search/${query}` + (query ? "/" : "");
        const resultFetcher = new CategoriesResultFetcher(this._searchContainer, urlComposer, "PageCategories");
        resultFetcher.onAsyncProgress.subscribe(progressPercentage => this.setLoaderPercentage(progressPercentage));
        const countUrlComposer = (query) => `/search/macrocategories/count/${query}/`;
        this._macrocategoriesSearchElement =
            new SearchElement("js-macrocategories-number-result", countUrlComposer, resultFetcher);
    }
}
