
# Python
from abc import abstractmethod

# Django
from django.views.generic import View
from django.views.generic.base import TemplateView
from django.http import HttpResponse

# Local
from skills.models import Skill, Subskill
from categories.models import Category, Macrocategory

class SearchView(TemplateView):
    template_name = 'search/page-search.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Skillbag"
        return context

class BaseSearchCountView(View):

    @property
    @abstractmethod
    def model(self):
        """The model this view will use to search"""

    def get(self, request, query):        
        query = query.strip() if query else ''
        if query:
            count = self.model.objects.filter(name__icontains=query).count()
        else:
            count = self.model.objects.all().count()

        return HttpResponse(count)
    
class SkillsSearchCountView(BaseSearchCountView):
    model = Skill

class SubskillsSearchCountView(BaseSearchCountView):
    model = Subskill

class CategoriesSearchCountView(BaseSearchCountView):
    model = Category

class MacrocategoriesSearchCountView(BaseSearchCountView):
    model = Macrocategory