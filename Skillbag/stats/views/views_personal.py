
# Python
import json
from datetime import timedelta
from itertools import accumulate, groupby

# Django
from django.http import JsonResponse
from django.views.generic.base import View, TemplateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.db.models import Count, Q, Max
from django.utils import timezone

# Local
from categories.models import Macrocategory, Category
from categories.forms import CategoryForm
from skills.models import Achievement, Skill
from skills.models import SkillLevels


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days) + 1):
        yield start_date + timedelta(n)


@method_decorator(login_required, name='dispatch')
class PersonalStatsView(TemplateView):
    template_name = 'stats/page-personal-statistics.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Personal Statistics"
        return context


@method_decorator(login_required, name='dispatch')
class MacrocategoryRadarView(View):
    
    def get(self, request, *args, **kwargs):
        user = request.user       
        
        macrocategories = (
            Macrocategory.objects.filter(category__skill__subskill__achievement__user=user)
            .annotate(achieved=Count('category__skill__subskill__achievement', filter=Q(category__skill__subskill__achievement__type='achieved')))
            .annotate(planned=Count('category__skill__subskill__achievement', filter=Q(category__skill__subskill__achievement__type='planned')))
            .order_by('name')
        )[:10]

        labels = [macrocategory.name for macrocategory in macrocategories]
        achieved = [macrocategory.achieved for macrocategory in macrocategories]
        planned = [macrocategory.planned for macrocategory in macrocategories]

        return JsonResponse({'labels': labels, 'datasets' : [achieved, planned] }, safe=False)


@method_decorator(login_required, name='dispatch')
class CategoryRadarView(View):
    
    def get(self, request, *args, **kwargs):
        user = request.user       
        
        categories = (
            Category.objects.filter(skill__subskill__achievement__user=user)
            .annotate(achieved=Count('skill__subskill__achievement', filter=Q(skill__subskill__achievement__type='achieved')))
            .annotate(planned=Count('skill__subskill__achievement', filter=Q(skill__subskill__achievement__type='planned')))
            .order_by('name')
        )[:10]

        labels = [category.name for category in categories]
        achieved = [category.achieved for category in categories]
        planned = [category.planned for category in categories]

        return JsonResponse({'labels': labels, 'datasets' : [achieved, planned] }, safe=False)

    

@method_decorator(login_required, name='dispatch')
class CompletionTrendView(View):
    
    def get(self, request, *args, **kwargs):
        sixty_days_ago = timezone.now() - timedelta(days=60)
        
        # get recent achievements
        recent_achievements = Achievement.objects.filter(user=request.user, updated_at__gte=sixty_days_ago).order_by('updated_at')
        achieved_subskills = [a for a in recent_achievements if a.type == 'achieved']
        planned_subskills = [a for a in recent_achievements if a.type == 'planned']

        # group them by day and count
        achieved_subskills = self.count_daily_achievements(achieved_subskills)
        planned_subskills = self.count_daily_achievements(planned_subskills)

        # add days with no achievements to array
        achieved_subskills = self.fill_days_array(achieved_subskills, sixty_days_ago)
        planned_subskills = self.fill_days_array(planned_subskills, sixty_days_ago)
        
        # add baseline
        baseline_achieved, baseline_planned = self.get_baseline(sixty_days_ago, request.user)
        achieved_subskills[0][1] += baseline_achieved
        planned_subskills[0][1] += baseline_planned

        # split arrays
        labels = [record[0] for record in achieved_subskills]
        achieved = [record[1] for record in achieved_subskills]
        planned = [record[1] for record in planned_subskills]

        # aggregate arrays
        achieved = list(accumulate(achieved))
        planned = list(accumulate(planned))

        return JsonResponse({'labels': labels, 'datasets' : [achieved, planned] }, safe=False)

    def get_baseline(self, date, user):
        achieved_subskills = Achievement.objects.filter(user=user, updated_at__lt=date, type='achieved').count()
        planned_subskills = Achievement.objects.filter(user=user, updated_at__lt=date, type='planned').count()       
        return achieved_subskills, planned_subskills

    def count_daily_achievements(self, achievements_list):
        grouped = groupby(achievements_list, lambda record: record.updated_at.strftime("%Y-%m-%d"))
        achievements_by_day = {day: len(list(achievements_this_day)) for day, achievements_this_day in grouped}
        return achievements_by_day

    def fill_days_array(self, achievements_dictionary, start_date):
        days_and_count = []
        for date in daterange(start_date, timezone.now()):
            days_and_count.append([date.strftime("%Y-%m-%d"), 0])

        for i in range(len(days_and_count)):
            day = days_and_count[i][0]
            days_and_count[i][1] = achievements_dictionary[day] if day in achievements_dictionary else 0

        return days_and_count


@method_decorator(login_required, name='dispatch')
class ProficiencyPolarView(View):
    
    def get(self, request, *args, **kwargs):
        user = request.user       
        user_skills_proficiency = (
            Skill.objects.filter(subskill__achievement__user=user, subskill__achievement__type='achieved')
            .annotate(proficiency=Max('subskill__level'))
            .order_by('proficiency')
            )
        grouped = groupby(user_skills_proficiency, lambda record: record.proficiency)
        skills_by_proficiency = [(proficiency, len(list(skills))) for proficiency, skills in grouped]
        
        levels = { level[0]:level[1] for level in SkillLevels.choices }
        labels = [levels[e[0]] for e in skills_by_proficiency]
        values = [e[1] for e in skills_by_proficiency]

        return JsonResponse({'labels': labels, 'values' : values }, safe=False)