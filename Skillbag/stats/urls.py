
from django.urls import path
from stats import views

app_name = 'stats'

urlpatterns = [
    path('', views.HubView.as_view()),        
    
    # Personal Statistics
    path('personal/', views.PersonalStatsView.as_view()),
    path('personal/macrocategory-radar/', views.MacrocategoryRadarView.as_view()),
    path('personal/category-radar/', views.CategoryRadarView.as_view()),
    path('personal/completion-trend/', views.CompletionTrendView.as_view()),
    path('personal/proficiency-polar/', views.ProficiencyPolarView.as_view()),
]
