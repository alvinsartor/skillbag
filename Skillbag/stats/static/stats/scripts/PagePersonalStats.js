"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PagePersonalStats extends Page {
    constructor() {
        super(...arguments);
        this._pageCharts = () => {
            return [
                MacrocategoryRadar,
                CategoryRadar,
                CompletionTrend,
                ProficiencyPolar,
            ];
        };
    }
    /** @inheritdoc  */
    initialize(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.loadPage();
        });
    }
    /** @inheritdoc  */
    initializeScripts() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.initializeChartsFromTemplate();
        });
    }
    /** @inheritdoc  */
    loadHtml() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.get("/statistics/personal/");
        });
    }
    /** @inheritdoc  */
    internalDispose() {
        return Promise.resolve();
    }
    initializeChartsFromTemplate() {
        return __awaiter(this, void 0, void 0, function* () {
            const container = document.getElementById("charts-container");
            const template = document.getElementById("chart-panel-template")
                .content.querySelector("div[class='charts__panel']");
            const chartPromises = this._pageCharts()
                .map(chartConstructor => this.createChartFromTemplateNode(container, template, chartConstructor));
            yield Promise.all(chartPromises);
        });
    }
    createChartFromTemplateNode(container, templateElement, chartConstructor) {
        return __awaiter(this, void 0, void 0, function* () {
            const chartPanel = document.importNode(templateElement, true);
            container.appendChild(chartPanel);
            const panelCanvas = chartPanel.querySelector("canvas");
            this.increaseCanvasHeightForMobile(panelCanvas);
            const chart = new chartConstructor(panelCanvas);
            chart.fetchDataAndAddChartToPage();
            const panelTitle = chartPanel.querySelector(".charts__panel-title");
            panelTitle.innerText = chart.chartTitle;
            const panelTooltip = chartPanel.querySelector(".charts__tooltip");
            panelTooltip.innerText = chart.chartDescription;
        });
    }
    increaseCanvasHeightForMobile(canvas) {
        if (window.matchMedia("(min-width: 640px)").matches)
            return;
        canvas.height = 300;
    }
}
