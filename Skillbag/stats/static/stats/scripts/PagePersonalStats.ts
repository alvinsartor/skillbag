
class PagePersonalStats extends Page {

    private _pageCharts: () => (new(canvas: HTMLCanvasElement) => IChartHandler)[] = () => {
        return [
            MacrocategoryRadar,
            CategoryRadar,
            CompletionTrend,
            ProficiencyPolar,
        ];
    };

    /** @inheritdoc  */
    public async initialize(...args: any[]): Promise<void> {
        await this.loadPage();
    }

    /** @inheritdoc  */
    protected async initializeScripts(): Promise<void> {
        await this.initializeChartsFromTemplate();
    }

    /** @inheritdoc  */
    protected async loadHtml(): Promise<string> {
        return await $.get("/statistics/personal/");
    }

    /** @inheritdoc  */
    protected internalDispose(): Promise<void> {
        return Promise.resolve();
    }

    private async initializeChartsFromTemplate(): Promise<void> {
        const container = document.getElementById("charts-container") as HTMLDivElement;
        const template = (document.getElementById("chart-panel-template") as HTMLTemplateElement)
            .content.querySelector("div[class='charts__panel']");

        const chartPromises = this._pageCharts()
            .map(chartConstructor => this.createChartFromTemplateNode(container, template!, chartConstructor));

        await Promise.all(chartPromises);
    }

    private async createChartFromTemplateNode(
        container: HTMLDivElement,
        templateElement: Element,
        chartConstructor: new(canvas: HTMLCanvasElement) => IChartHandler): Promise<void> {

        const chartPanel = document.importNode<any>(templateElement, true);
        container.appendChild(chartPanel);
        const panelCanvas = chartPanel.querySelector("canvas") as HTMLCanvasElement;
        this.increaseCanvasHeightForMobile(panelCanvas);
        const chart = new chartConstructor(panelCanvas);
        chart.fetchDataAndAddChartToPage();

        const panelTitle = chartPanel.querySelector(".charts__panel-title") as HTMLDivElement;
        panelTitle.innerText = chart.chartTitle;

        const panelTooltip = chartPanel.querySelector(".charts__tooltip") as HTMLDivElement;
        panelTooltip.innerText = chart.chartDescription;
    }

    private increaseCanvasHeightForMobile(canvas: HTMLCanvasElement): void {
        if (window.matchMedia("(min-width: 640px)").matches) return;
        canvas.height = 300;
    }
}