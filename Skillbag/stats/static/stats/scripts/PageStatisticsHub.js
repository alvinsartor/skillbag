"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageStatisticsHub extends Page {
    /** @inheritdoc  */
    initialize(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.loadPage();
        });
    }
    /** @inheritdoc  */
    initializeScripts() {
        this.attachPanelToChartAndPage("personal-stats-panel", CategoryRadar, "PagePersonalStats");
        this.stopPropagationForHelpTextButtons();
        return Promise.resolve();
    }
    /** @inheritdoc  */
    loadHtml() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.get("/statistics/");
        });
    }
    /** @inheritdoc  */
    internalDispose() {
        return Promise.resolve();
    }
    attachPanelToChartAndPage(panelId, chartHandler, page) {
        const panel = document.getElementById(panelId);
        const canvas = panel.querySelector("canvas");
        const chart = new chartHandler(canvas);
        chart.fetchDataAndAddChartToPage();
        panel.onclick = () => SiteManager.instance().load(page);
    }
    stopPropagationForHelpTextButtons() {
        Array.from(document.querySelectorAll(".statistics-hub__help-text"))
            .forEach(e => e.onclick = (ev) => ev.stopPropagation());
    }
}
