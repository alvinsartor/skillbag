
class ProficiencyPolar extends BaseChartHandler<IPolarData> {

    /** @inheritdoc */
    public get chartTitle(): string { return "Skills Proficiency"; }

    /** @inheritdoc */
    public get chartDescription(): string { return "How proficient are you with your skills? This chart shows the number of skills you brought each level."; }

    /** Creates a new instance of CategoryRadar */
    public constructor(canvas: HTMLCanvasElement) {
        super(canvas);
    }

    /** @inheritdoc */
    protected getChartData(): Promise<IPolarData> {
        return $.get("/statistics/personal/proficiency-polar/");
    }

    /** @inheritdoc */
    protected createChart(canvas: HTMLCanvasElement, data: IPolarData): Chart {
        const chartData = {
            labels: data.labels,
            datasets: [
                {
                    borderColor: "#06A77D",
                    backgroundColor: "rgba(6, 167, 125, 0.25)",
                    data: data.values,
                },
            ]
        };

        const chartOptions = {
            scale: {
                gridLines: {
                    color: "#525252",
                    circular: true,
                },
                pointLabels: {
                    fontColor: "#FAFAFA",
                },
                ticks: {
                    backdropColor: "transparent",
                },
            },
            legend: {
                display: false,
            },
        };

        return new Chart(canvas, { type: "polarArea", data: chartData, options: chartOptions as any });
    }
}