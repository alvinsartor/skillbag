"use strict";
class CategoryRadar extends BaseChartHandler {
    /** @inheritdoc */
    get chartTitle() { return "Categories Radar"; }
    /** @inheritdoc */
    get chartDescription() { return "How are your skills distributed through each category? This chart shows it!"; }
    /** Creates a new instance of CategoryRadar */
    constructor(canvas) {
        super(canvas);
    }
    /** @inheritdoc */
    getChartData() {
        return $.get("/statistics/personal/category-radar/");
    }
    /** @inheritdoc */
    createChart(canvas, data) {
        const chartData = {
            labels: data.labels,
            datasets: [
                {
                    label: "Achieved",
                    borderColor: "#06A77D",
                    backgroundColor: "rgba(6, 167, 125, 0.25)",
                    data: data.datasets[0]
                },
                {
                    label: "Planned",
                    borderColor: "#ECC759",
                    backgroundColor: "rgba(236, 199, 89, 0.25)",
                    data: data.datasets[1]
                },
            ]
        };
        const chartOptions = {
            scale: {
                gridLines: {
                    color: "#525252",
                    circular: true,
                },
                pointLabels: {
                    fontColor: "#FAFAFA",
                },
                ticks: {
                    backdropColor: "transparent",
                },
            },
            legend: {
                display: true,
                position: "bottom",
                labels: {
                    fontColor: "#FAFAFA"
                }
            },
            elements: {
                line: {
                    tension: 0.3
                },
            }
        };
        return new Chart(canvas, { type: "radar", data: chartData, options: chartOptions });
    }
}
