
class CompletionTrend extends BaseChartHandler<ISeries> {

    /** @inheritdoc */
    public get chartTitle(): string { return "Completion Trend"; }

    /** @inheritdoc */
    public get chartDescription(): string { return "Are your skills skyrocketing or staying still? This chart shows your progresses over time."; }

    /** Creates a new instance of CompletionTrend */
    public constructor(canvas: HTMLCanvasElement) {
        super(canvas);
    }

    /** @inheritdoc */
    protected getChartData(): Promise<ISeries> {
        return $.get("/statistics/personal/completion-trend/");
    }

    /** @inheritdoc */
    protected createChart(canvas: HTMLCanvasElement, data: ISeries): Chart {
        const chartData = {
            labels: data.labels,
            datasets: [
                {
                    label: "Achieved",
                    borderColor: "#06A77D",
                    backgroundColor: "rgba(6, 167, 125, 0.25)",
                    data: data.datasets[0]
                },
                {
                    label: "Planned",
                    borderColor: "#ECC759",
                    backgroundColor: "rgba(236, 199, 89, 0.25)",
                    data: data.datasets[1]
                },
            ]
        };

        const chartOptions = {
            legend: {
                position: "bottom",
                labels: {
                    fontColor: "#FAFAFA"
                }
            },
            elements: {
                point: {
                    radius: 0,
                    hitRadius: 4,
                },
            },
        };

        return new Chart(canvas, { type: "line", data: chartData, options: chartOptions as any });
    }
}