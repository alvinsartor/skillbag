
interface IChartHandler
{
    /** The title of this chart. */
    readonly chartTitle: string;

    /** The description of this chart. */
    readonly chartDescription: string;

    /** Fetches the data and adds the resulting chart to the current page. */
    fetchDataAndAddChartToPage(): Promise<void>;
}

abstract class BaseChartHandler<T extends IChartData> implements IChartHandler {

    /** @inheritdoc */
    public abstract get chartTitle(): string;

    /** @inheritdoc */
    public abstract get chartDescription(): string;

    private readonly _chartCanvas: HTMLCanvasElement;
    private _chart: Chart | null = null;

    protected constructor(canvas: HTMLCanvasElement) {
        this._chartCanvas = canvas;
    }

    /** Fetches the data and adds the resulting chart to the current page. */
    public async fetchDataAndAddChartToPage(): Promise<void> {
        const data = await this.getChartData();
        this._chart = this.createChart(this._chartCanvas, data);
    }

    /** Fetches the data needed for this particular chart. */
    protected abstract getChartData(): Promise<T>;

    /** Uses the fetched data to create a chart. */
    protected abstract createChart(canvas: HTMLCanvasElement, data: T): Chart;

}