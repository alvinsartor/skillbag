"use strict";
class CompletionTrend extends BaseChartHandler {
    /** @inheritdoc */
    get chartTitle() { return "Completion Trend"; }
    /** @inheritdoc */
    get chartDescription() { return "Are your skills skyrocketing or staying still? This chart shows your progresses over time."; }
    /** Creates a new instance of CompletionTrend */
    constructor(canvas) {
        super(canvas);
    }
    /** @inheritdoc */
    getChartData() {
        return $.get("/statistics/personal/completion-trend/");
    }
    /** @inheritdoc */
    createChart(canvas, data) {
        const chartData = {
            labels: data.labels,
            datasets: [
                {
                    label: "Achieved",
                    borderColor: "#06A77D",
                    backgroundColor: "rgba(6, 167, 125, 0.25)",
                    data: data.datasets[0]
                },
                {
                    label: "Planned",
                    borderColor: "#ECC759",
                    backgroundColor: "rgba(236, 199, 89, 0.25)",
                    data: data.datasets[1]
                },
            ]
        };
        const chartOptions = {
            legend: {
                position: "bottom",
                labels: {
                    fontColor: "#FAFAFA"
                }
            },
            elements: {
                point: {
                    radius: 0,
                    hitRadius: 4,
                },
            },
        };
        return new Chart(canvas, { type: "line", data: chartData, options: chartOptions });
    }
}
