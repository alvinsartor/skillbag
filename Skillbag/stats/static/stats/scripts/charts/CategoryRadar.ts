
class CategoryRadar extends BaseChartHandler<IRadarData> {

    /** @inheritdoc */
    public get chartTitle(): string { return "Categories Radar"; }

    /** @inheritdoc */
    public get chartDescription(): string { return "How are your skills distributed through each category? This chart shows it!"; }

    /** Creates a new instance of CategoryRadar */
    public constructor(canvas: HTMLCanvasElement) {
        super(canvas);
    }

    /** @inheritdoc */
    protected getChartData(): Promise<IRadarData> {
        return $.get("/statistics/personal/category-radar/");
    }

    /** @inheritdoc */
    protected createChart(canvas: HTMLCanvasElement, data: IRadarData): Chart {
        const chartData = {
            labels: data.labels,
            datasets: [
                {
                    label: "Achieved",
                    borderColor: "#06A77D",
                    backgroundColor: "rgba(6, 167, 125, 0.25)",
                    data: data.datasets[0]
                },
                {
                    label: "Planned",
                    borderColor: "#ECC759",
                    backgroundColor: "rgba(236, 199, 89, 0.25)",
                    data: data.datasets[1]
                },
            ]
        };

        const chartOptions = {
            scale: {
                gridLines: {
                    color: "#525252",
                    circular: true,
                },
                pointLabels: {
                    fontColor: "#FAFAFA",
                },
                ticks: {
                    backdropColor: "transparent",
                },
            },
            legend: {
                display: true,
                position: "bottom",
                labels: {
                    fontColor: "#FAFAFA"
                }
            },
            elements: {
                line: {
                    tension: 0.3
                },
            }
        }

        return new Chart(canvas, { type: "radar", data: chartData, options: chartOptions as any});
    }
}