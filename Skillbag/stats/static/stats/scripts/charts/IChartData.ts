
interface IChartData { }

interface IRadarData extends IChartData {
    labels: string[];
    datasets: number[][];
}

interface ISeries extends IChartData {
    labels: string[];
    datasets: number[][];
}

interface IPolarData extends IChartData {
    labels: string[];
    values: number[];
}