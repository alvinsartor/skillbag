"use strict";
class MacrocategoryRadar extends BaseChartHandler {
    /** @inheritdoc */
    get chartTitle() { return "Macrocategories Radar"; }
    /** @inheritdoc */
    get chartDescription() { return "Each one of us is stronger in certain fields. This chart shows which one are your strong ones."; }
    /** Creates a new instance of MacrocategoryRadar */
    constructor(canvas) {
        super(canvas);
    }
    /** @inheritdoc */
    getChartData() {
        return $.get("/statistics/personal/macrocategory-radar/");
    }
    /** @inheritdoc */
    createChart(canvas, data) {
        const chartData = {
            labels: data.labels,
            datasets: [
                {
                    label: "Achieved",
                    borderColor: "#06A77D",
                    backgroundColor: "rgba(6, 167, 125, 0.25)",
                    data: data.datasets[0]
                },
                {
                    label: "Planned",
                    borderColor: "#ECC759",
                    backgroundColor: "rgba(236, 199, 89, 0.25)",
                    data: data.datasets[1]
                },
            ]
        };
        const chartOptions = {
            scale: {
                gridLines: {
                    color: "#525252"
                },
                pointLabels: {
                    fontColor: "#FAFAFA",
                },
                ticks: {
                    backdropColor: "transparent",
                },
            },
            legend: {
                display: true,
                position: "bottom",
                labels: {
                    fontColor: "#FAFAFA"
                },
            },
        };
        return new Chart(canvas, { type: "radar", data: chartData, options: chartOptions });
    }
}
