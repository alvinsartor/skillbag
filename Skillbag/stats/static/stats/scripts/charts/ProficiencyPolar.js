"use strict";
class ProficiencyPolar extends BaseChartHandler {
    /** @inheritdoc */
    get chartTitle() { return "Skills Proficiency"; }
    /** @inheritdoc */
    get chartDescription() { return "How proficient are you with your skills? This chart shows the number of skills you brought each level."; }
    /** Creates a new instance of CategoryRadar */
    constructor(canvas) {
        super(canvas);
    }
    /** @inheritdoc */
    getChartData() {
        return $.get("/statistics/personal/proficiency-polar/");
    }
    /** @inheritdoc */
    createChart(canvas, data) {
        const chartData = {
            labels: data.labels,
            datasets: [
                {
                    borderColor: "#06A77D",
                    backgroundColor: "rgba(6, 167, 125, 0.25)",
                    data: data.values,
                },
            ]
        };
        const chartOptions = {
            scale: {
                gridLines: {
                    color: "#525252",
                    circular: true,
                },
                pointLabels: {
                    fontColor: "#FAFAFA",
                },
                ticks: {
                    backdropColor: "transparent",
                },
            },
            legend: {
                display: false,
            },
        };
        return new Chart(canvas, { type: "polarArea", data: chartData, options: chartOptions });
    }
}
