
class PageStatisticsHub extends Page {

    /** @inheritdoc  */
    public async initialize(...args: any[]): Promise<void> {
        await this.loadPage();
    }

    /** @inheritdoc  */
    protected initializeScripts(): Promise<void> {
        this.attachPanelToChartAndPage("personal-stats-panel", CategoryRadar, "PagePersonalStats");
        this.stopPropagationForHelpTextButtons();

        return Promise.resolve();
    }

    /** @inheritdoc  */
    protected async loadHtml(): Promise<string> {
        return await $.get("/statistics/");
    }

    /** @inheritdoc  */
    protected internalDispose(): Promise<void> {
        return Promise.resolve();
    }

    private attachPanelToChartAndPage<T>(
        panelId: string,
        chartHandler: new(canvas: HTMLCanvasElement) => BaseChartHandler<T>,
        page: string): void {

        const panel = document.getElementById(panelId) as HTMLDivElement;
        const canvas = panel.querySelector("canvas") as HTMLCanvasElement;
        const chart = new chartHandler(canvas);
        chart.fetchDataAndAddChartToPage();

        panel.onclick = () => SiteManager.instance().load(page);
    }

    private stopPropagationForHelpTextButtons(): void {
        Array.from(document.querySelectorAll(".statistics-hub__help-text"))
            .forEach(e => (e as HTMLDivElement).onclick = (ev) => ev.stopPropagation()); 
    }
}