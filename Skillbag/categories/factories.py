
# 3rd Party
import factory
from factory.django import DjangoModelFactory

# Local
from categories.models import Macrocategory, Category


class MacrocategoryFactory(DjangoModelFactory):
    class Meta:
        model = Macrocategory

    name = factory.Faker('word')
    description = factory.Faker('sentence')
    wikipedia_link = factory.Faker('url')
    
class CategoryFactory(DjangoModelFactory):
    class Meta:
        model = Category

    name = factory.Faker('word')
    macrocategory = factory.SubFactory(MacrocategoryFactory)
    description = factory.Faker('sentence')
    wikipedia_link = factory.Faker('url')
