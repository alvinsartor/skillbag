
# Django
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# Local
from categories.models import Macrocategory
from categories.forms import MacrocategoryForm


class MacrocategoriesView(TemplateView):
    template_name = 'categories/page-macrocategories.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Macro-Categories'
        context['elements'] = Macrocategory.objects.all().order_by('name')
        context['empty_message'] = "There are no macrocategory."
        context['is_categories'] = False
        return context


class MacrocategoriesSearchView(TemplateView):
    template_name = 'categories/fragments/snippet-categories-list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)        
        if 'query' in kwargs:
            query = kwargs.get('query', '').strip()
            macrocategories = Macrocategory.objects.filter(name__icontains=query).order_by('name')
            empty_message = "It seems there are no macrocategories corresponding to your search."
        else:
            macrocategories = Macrocategory.objects.none()
            empty_message = "Start typing to see matching macrocategories."
            
        context['elements'] = macrocategories
        context['empty_message'] = empty_message
        return context
        

@method_decorator(login_required, name='dispatch')
class EditMacrocategoryFormView(UpdateView):
    template_name = 'nexus/forms/generic_form.html'
    model = Macrocategory
    form_class = MacrocategoryForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return JsonResponse({})


@method_decorator(login_required, name='dispatch')
class AddMacrocategoryFormView(CreateView):
    template_name = 'nexus/forms/generic_form.html'
    model = Macrocategory
    form_class = MacrocategoryForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return JsonResponse({})
