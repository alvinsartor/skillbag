
# Python
import json

# Django
from django.http import JsonResponse
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# Local
from categories.models import Macrocategory, Category
from categories.forms import CategoryForm


class CategoriesView(TemplateView):
    template_name = 'categories/page-categories.html'
    
    def get_context_data(self, **kwargs):
        macrocategory_pk = kwargs.get('pk')
        macrocategory = get_object_or_404(Macrocategory, pk=str(macrocategory_pk))

        context = super().get_context_data(**kwargs)
        context['macrocategory'] = macrocategory
        context['title'] = macrocategory.name        
        context['elements'] = macrocategory.category_set.all().order_by('name')
        context['empty_message'] = "There are no categories in this macrocategory."
        context['is_categories'] = True
        return context

class CategoriesSearchView(TemplateView):
    template_name = 'categories/fragments/snippet-categories-list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)        
        if 'query' in kwargs:
            query = kwargs.get('query', '').strip()
            categories = Category.objects.filter(name__icontains=query).order_by('name')
            empty_message = "It seems there are no categories corresponding to your search."
        else:
            categories = Category.objects.none()
            empty_message = "Start typing to see matching categories."
            
        context['elements'] = categories
        context['empty_message'] = empty_message
        return context

@method_decorator(login_required, name='dispatch')
class EditCategoryFormView(UpdateView):
    template_name = 'nexus/forms/generic_form.html'
    model = Category
    form_class = CategoryForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return JsonResponse({})
    

@method_decorator(login_required, name='dispatch')
class AddCategoryFormView(CreateView):
    template_name = 'nexus/forms/generic_form.html'
    model = Category
    form_class = CategoryForm
    success_url = '/'

    def get_initial(self):
        initial = super().get_initial()
        macrocategory = self.kwargs.get('pk')
        initial['macrocategory'] = macrocategory
        return initial

    def form_valid(self, form):
        form.save()
        return JsonResponse({})
