"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageMacrocategories extends PageCategoriesBase {
    constructor() {
        super(...arguments);
        this._macrocategoryAdder = null;
    }
    /** @inheritdoc  */
    initialize() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.loadPage();
        });
    }
    /** @inheritdoc */
    loadHtml() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.get("/categories/macrocategories/");
        });
    }
    /** @inheritdoc */
    addCategoriesListeners() {
        Array.from(document.querySelectorAll("[name='category-tile']"))
            .map(e => e)
            .forEach(e => {
            this.categoriesControls.push(e);
            e.onclick = () => SiteManager.instance().load("PageCategories", e.dataset.id);
        });
    }
    /** @inheritdoc */
    addActionsListeners() {
        document.getElementById("add-new-action").onclick = () => {
            this._macrocategoryAdder = new MacrocategoryAdder();
            this._macrocategoryAdder.loadEditorForm();
        };
    }
}
