
class PageMacrocategories extends PageCategoriesBase {

    private _macrocategoryAdder: MacrocategoryAdder|null = null;

    /** @inheritdoc  */
    public async initialize(): Promise<void> {
        await this.loadPage();
    }

    /** @inheritdoc */
    protected async loadHtml(): Promise<string> {
        return await $.get("/categories/macrocategories/");
    }

    /** @inheritdoc */
    protected addCategoriesListeners(): void {
        Array.from(document.querySelectorAll("[name='category-tile']"))
            .map(e => e as HTMLDivElement)
            .forEach(e => {
                this.categoriesControls.push(e);
                e.onclick = () => SiteManager.instance().load("PageCategories", e.dataset.id);
            });
    }

    /** @inheritdoc */
    public addActionsListeners(): void
    {
        (document.getElementById("add-new-action") as HTMLElement).onclick = () =>
        {
            this._macrocategoryAdder = new MacrocategoryAdder();
            this._macrocategoryAdder.loadEditorForm();
        };
    }
}