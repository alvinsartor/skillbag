
class PageCategories extends PageCategoriesBase {

    private _categoryPk: string = "-1";
    private _macrocategoryEditor: MacrocategoryEditor | null = null;
    private _categoryAdder: CategoryAdder | null = null;

    /** @inheritdoc  */
    public async initialize(...args: string[]): Promise<void> {
        [this._categoryPk] = args;
        await this.loadPage();
    }

    /** @inheritdoc */
    protected async loadHtml(): Promise<string> {
        return await $.get(`/categories/macrocategories/${this._categoryPk}`);
    }

    /** @inheritdoc */
    protected addCategoriesListeners(): void {
        Array.from(document.querySelectorAll("[name='category-tile']"))
            .map(e => e as HTMLDivElement)
            .forEach(e => {
                this.categoriesControls.push(e);
                e.onclick = () => SiteManager.instance().load("PageCategorySkills", e.dataset.id);
            });

        this.shrinkDescriptionTileIfEmpty();
    }

    private shrinkDescriptionTileIfEmpty(): void {
        const descriptionTile = document.getElementById("description-tile") as HTMLDivElement;
        const descriptionPanel = descriptionTile.querySelector(".description-displayer__text") as HTMLElement;
        const padding = descriptionPanel.innerText.length ? "50px 25px" : "10px 25px";
        descriptionTile.style.padding = padding;
    }

    /** @inheritdoc */
    public addActionsListeners(): void {
        (document.getElementById("edit-action") as HTMLElement).onclick = () => {
            this._macrocategoryEditor = new MacrocategoryEditor(this._categoryPk);
            this._macrocategoryEditor.loadEditorForm();
        };

        (document.getElementById("add-new-action") as HTMLElement).onclick = () => {
            this._categoryAdder = new CategoryAdder(this._categoryPk);
            this._categoryAdder.loadEditorForm();
        };
    }
}