
abstract class PageCategoriesBase extends Page {

    protected categoriesControls: HTMLElement[] = [];

    /** @inheritdoc */
    protected initializeScripts(): Promise<void> {
        this.addCategoriesListeners();
        this.addActionsListeners();
        return Promise.resolve();
    }

    /** Adds the listeners to the categories tiles. */
    protected abstract addCategoriesListeners(): void;

    /** Function used to associate listeners to the action menu entries. Override if required. */
    protected addActionsListeners(): void {}

    /** @inheritdoc */
    protected internalDispose(): Promise<void> {
        return Promise.resolve();
    }
}