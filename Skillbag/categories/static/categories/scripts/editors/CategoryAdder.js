"use strict";
class CategoryAdder extends BaseEditor {
    constructor(macrocategoryPk) {
        super();
        /** @inheritdoc */
        this.title = "Add Category";
        this._macrocategoryPk = macrocategoryPk;
    }
    /** @inheritdoc */
    get formUrl() { return `/categories/macrocategories/${this._macrocategoryPk}/add/`; }
}
