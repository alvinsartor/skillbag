"use strict";
class MacrocategoryAdder extends BaseEditor {
    constructor() {
        super();
        /** @inheritdoc */
        this.title = "Add Macrocategory";
    }
    /** @inheritdoc */
    get formUrl() { return `/categories/macrocategories/add/`; }
}
