
class CategoryAdder extends BaseEditor {

    /** @inheritdoc */
    protected title: string = "Add Category";

    private readonly _macrocategoryPk: string;

    /** @inheritdoc */
    protected get formUrl() { return `/categories/macrocategories/${this._macrocategoryPk}/add/`; }

    public constructor(macrocategoryPk: string) {
        super();
        this._macrocategoryPk = macrocategoryPk;
    }
}