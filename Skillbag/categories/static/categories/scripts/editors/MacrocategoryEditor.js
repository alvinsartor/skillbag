"use strict";
class MacrocategoryEditor extends BaseEditor {
    constructor(macrocategoryPk) {
        super();
        /** @inheritdoc */
        this.title = "Edit Macrocategory";
        this._pk = macrocategoryPk;
    }
    /** @inheritdoc */
    get formUrl() { return `/categories/macrocategories/${this._pk}/edit/`; }
}
