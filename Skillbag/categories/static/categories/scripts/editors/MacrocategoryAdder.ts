
class MacrocategoryAdder extends BaseEditor {

    /** @inheritdoc */
    protected title: string = "Add Macrocategory";

    public constructor() {
        super();
    }

    /** @inheritdoc */
    protected get formUrl() { return `/categories/macrocategories/add/`; }
}