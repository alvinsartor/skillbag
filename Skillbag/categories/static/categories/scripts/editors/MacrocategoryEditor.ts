
class MacrocategoryEditor extends BaseEditor {
    /** @inheritdoc */
    protected title: string = "Edit Macrocategory";

    private readonly _pk: string;

    /** @inheritdoc */
    protected get formUrl() { return `/categories/macrocategories/${this._pk}/edit/`; }

    public constructor(macrocategoryPk: string) {
        super();
        this._pk = macrocategoryPk;
    }
}