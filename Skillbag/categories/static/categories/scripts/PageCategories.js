"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PageCategories extends PageCategoriesBase {
    constructor() {
        super(...arguments);
        this._categoryPk = "-1";
        this._macrocategoryEditor = null;
        this._categoryAdder = null;
    }
    /** @inheritdoc  */
    initialize(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            [this._categoryPk] = args;
            yield this.loadPage();
        });
    }
    /** @inheritdoc */
    loadHtml() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.get(`/categories/macrocategories/${this._categoryPk}`);
        });
    }
    /** @inheritdoc */
    addCategoriesListeners() {
        Array.from(document.querySelectorAll("[name='category-tile']"))
            .map(e => e)
            .forEach(e => {
            this.categoriesControls.push(e);
            e.onclick = () => SiteManager.instance().load("PageCategorySkills", e.dataset.id);
        });
        this.shrinkDescriptionTileIfEmpty();
    }
    shrinkDescriptionTileIfEmpty() {
        const descriptionTile = document.getElementById("description-tile");
        const descriptionPanel = descriptionTile.querySelector(".description-displayer__text");
        const padding = descriptionPanel.innerText.length ? "50px 25px" : "10px 25px";
        descriptionTile.style.padding = padding;
    }
    /** @inheritdoc */
    addActionsListeners() {
        document.getElementById("edit-action").onclick = () => {
            this._macrocategoryEditor = new MacrocategoryEditor(this._categoryPk);
            this._macrocategoryEditor.loadEditorForm();
        };
        document.getElementById("add-new-action").onclick = () => {
            this._categoryAdder = new CategoryAdder(this._categoryPk);
            this._categoryAdder.loadEditorForm();
        };
    }
}
