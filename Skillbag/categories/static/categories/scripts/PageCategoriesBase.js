"use strict";
class PageCategoriesBase extends Page {
    constructor() {
        super(...arguments);
        this.categoriesControls = [];
    }
    /** @inheritdoc */
    initializeScripts() {
        this.addCategoriesListeners();
        this.addActionsListeners();
        return Promise.resolve();
    }
    /** Function used to associate listeners to the action menu entries. Override if required. */
    addActionsListeners() { }
    /** @inheritdoc */
    internalDispose() {
        return Promise.resolve();
    }
}
