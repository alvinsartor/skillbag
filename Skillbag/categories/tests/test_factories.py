
from django.test import TestCase

from categories.models import Macrocategory, Category
from categories.factories import MacrocategoryFactory, CategoryFactory
from accounts.factories import UserFactory


class MacrocategoryFactoryTest(TestCase):
    def test_factory_create_proper_macrocategory(self):
        macrocategory = MacrocategoryFactory()
        
        self.assertNotEqual(macrocategory.name, '')
        self.assertEqual(Macrocategory.objects.all().count(), 1)


class CategoryFactoryTest(TestCase):
    def test_factory_create_proper_category(self):
        category = CategoryFactory()

        self.assertNotEqual(category.name, '')
        self.assertIsNotNone(category.macrocategory)        
        self.assertEqual(Category.objects.all().count(), 1)
        self.assertEqual(Macrocategory.objects.all().count(), 1)

