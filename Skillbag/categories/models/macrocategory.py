
from django.db import models
from nexus.models.abstract import DescriptableMixin

class Macrocategory(DescriptableMixin):
    name = models.CharField("Name", max_length=50, blank=False)

    class Meta:
        verbose_name_plural = "Macrocategories"

    def __str__(self):
        return self.name

    def count_categories(self):
        return self.category_set.count()
    
    def count_skills(self):
        from skills.models import Skill
        return Skill.objects.filter(category__macrocategory=self).count()

    def count_subskills(self):
        from skills.models import Subskill
        return Subskill.objects.filter(skill__category__macrocategory=self).count()