
# Django
from django.utils.functional import cached_property
from django.db import models

# Local
from nexus.models.abstract import DescriptableMixin
from categories.models.macrocategory import Macrocategory

class Category(DescriptableMixin):
    name = models.CharField("Name", max_length=50, blank=False)
    macrocategory = models.ForeignKey(Macrocategory, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self._str_

    @cached_property
    def _str_(self):
        return  "{} ({})".format(self.name, self.macrocategory.name)

    def count_skills(self):
        return self.skill_set.all().count()

    def count_subskills(self):
        from skills.models import Subskill
        return Subskill.objects.filter(skill__category=self).count()