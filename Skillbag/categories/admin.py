
from django.contrib import admin
from categories.models import Macrocategory, Category
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin


@admin.register(Macrocategory)
class MarcocategoryAdmin(admin.ModelAdmin):
    fieldsets = [(None, {'fields': ['name', 'description', 'wikipedia_link']})]
    

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fieldsets = [(None, {'fields': ['name', 'macrocategory', 'description', 'wikipedia_link']})]
