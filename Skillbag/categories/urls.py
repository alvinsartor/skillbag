
from django.urls import path

from categories import views as category_views
from skills import views as skill_views

app_name = 'categories'

urlpatterns = [

    path('macrocategories/', category_views.MacrocategoriesView.as_view(), name='macrocategories'),       
    path('macrocategories/add/', category_views.AddMacrocategoryFormView.as_view()),       
    path('macrocategories/<int:pk>/', category_views.CategoriesView.as_view(), name='macrocategory-content'),    
    path('macrocategories/<int:pk>/edit/', category_views.EditMacrocategoryFormView.as_view()),    
    path('macrocategories/<int:pk>/add/', category_views.AddCategoryFormView.as_view()),    
    path('macrocategories/search/', category_views.MacrocategoriesSearchView.as_view()),    
    path('macrocategories/search/<str:query>/', category_views.MacrocategoriesSearchView.as_view()),    
    
    path('<int:pk>/', skill_views.CategorySkillsView.as_view(), name='category-content'),    
    path('<int:pk>/edit/', category_views.EditCategoryFormView.as_view()),    
    path('<int:pk>/add/', skill_views.AddSkillFormView.as_view()),    
    path('search/', category_views.CategoriesSearchView.as_view()),    
    path('search/<str:query>/', category_views.CategoriesSearchView.as_view()),    
]
