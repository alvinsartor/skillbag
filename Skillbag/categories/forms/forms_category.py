
from django import forms

from nexus.forms import DescriptableForm
from categories.models import Category, Macrocategory

class CategoryForm(DescriptableForm):   

    macrocategory = forms.ModelChoiceField(queryset=Macrocategory.objects.all(), empty_label=None)

    class Meta(DescriptableForm.Meta):
        model = Category
        fields = DescriptableForm.Meta.fields + ['macrocategory']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['macrocategory'].widget.attrs['class'] = "editor-form__combobox"       