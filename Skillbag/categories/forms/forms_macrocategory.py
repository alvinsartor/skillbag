
from nexus.forms import DescriptableForm
from categories.models import Macrocategory

class MacrocategoryForm(DescriptableForm):   

    class Meta(DescriptableForm.Meta):
        model = Macrocategory