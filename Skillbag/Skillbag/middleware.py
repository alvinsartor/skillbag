
from django.db import connection
from django.http import HttpResponse

class QueryCountDebugMiddleware:
    """ 
        Middleware used to count number of queries and time used for each request. 
        It is activated only in DEBUG mode. 
    """

    MAX_NUMBER_OF_QUERIES_BEFORE_OF_SHAME_PAGE = 20
    
    def __init__(self, get_response):
        self.get_response = get_response
        
    def __call__(self, request):
        response = self.get_response(request)

        # skip count for admin views
        if request.path.startswith('/admin/'):
            return response

        # skip count for unsuccessful responses
        if response.status_code != 200:
            return response
        
        total_time = 0

        for query in connection.queries:
            query_time = query.get('time')
            if query_time is None:

                query_time = query.get('duration', 0) / 1000
            total_time += float(query_time)
            
        print ('** MIDDLEWARE QUERYCOUNT ** %s queries run, total %s seconds' % (len(connection.queries), total_time))
        
        if len(connection.queries) > self.MAX_NUMBER_OF_QUERIES_BEFORE_OF_SHAME_PAGE:
            html = self.compose_html(connection.queries, total_time)
            return HttpResponse(html)
        
        return response

    def compose_html(self, queries, total_time):
        result = '<html><body><div class="inefficient-middleware-text">'
        result += 'OMG! You this page is terribly inefficient!<br/>'
        result += 'It realized ' + str(len(queries)) + ' queries and it took ' + str(total_time) + ' seconds!<br/><br/>'
        result += 'SHAME ON WHO WROTE THIS S***T!<br/><br/><br/>'
        result += '<img src="https://i.giphy.com/media/m6tmCnGCNvTby/giphy.webp"/><br/><br/><br/>'
        for query in queries:            
                result += query['sql'] + '<br/><br/>'
        result += '</div></body></html>'
        return result