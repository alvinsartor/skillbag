"""
Django settings for Skillbag project.

Based on 'django-admin startproject' using Django 2.1.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os
import posixpath

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'a2e29f89-44af-4ddc-b7d2-291e4d7dea2d'

DEBUG = os.environ.get('DEBUG', 'True') == 'True'
DEVELOPMENT = os.environ.get('DEVELOPMENT', 'True') == 'True'

ALLOWED_HOSTS = [
    'localhost',
    'skillbag.pythonanywhere.com',
    ]

# Application references
# https://docs.djangoproject.com/en/2.1/ref/settings/#std:setting-INSTALLED_APPS
INSTALLED_APPS = [
    'nexus',
    'skills',
    'accounts',
    'categories',
    'stats',
    'search',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

# Middleware framework
# https://docs.djangoproject.com/en/2.1/topics/http/middleware/
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

DEBUG_MIDDLEWARE = [
    'Skillbag.middleware.QueryCountDebugMiddleware',
]

if DEBUG:
    MIDDLEWARE = MIDDLEWARE + DEBUG_MIDDLEWARE

ROOT_URLCONF = 'Skillbag.urls'

# Template configuration
# https://docs.djangoproject.com/en/2.1/topics/templates/
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'Skillbag.wsgi.application'
# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases
LIVE_DATABASES = {    
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'skillbag$default',
        'USER': 'skillbag',
        'PASSWORD': 'castell8!',
        'HOST': 'skillbag.mysql.pythonanywhere-services.com',
        'TEST': {
            'NAME': 'skillbag$test_default',
        },
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    },
}

DEV_DATABASES = {    
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'skillbag_db',
        'USER': 'skillbag',
        'PASSWORD': 'skillbag_is_the_best',
        'HOST': 'localhost',
        'PORT': '',
    }

}

DATABASES = DEV_DATABASES if DEVELOPMENT else LIVE_DATABASES

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
]

LOGIN_URL = 'accounts:login'
LOGIN_REDIRECT_URL = 'home'
AUTH_USER_MODEL = 'accounts.User'

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = posixpath.join(*(BASE_DIR.split(os.path.sep) + ['static']))


if DEBUG:
    # Email printed while in debug
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
else:
    # Use Gmail in production
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    DEFAULT_FROM_EMAIL = "ananasproject@gmail.com"
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_USE_TLS = True
    EMAIL_PORT = 587                                                                                                                                                                                                                                                                                    
    EMAIL_HOST_USER = 'ananasproject@gmail.com'
    EMAIL_HOST_PASSWORD = os.environ.get('MAIL_PASSWORD', 'dylfjahtjoqxlmzt')

ADMINS = [('Ananas', 'ananasproject@gmail.com')]
SERVER_EMAIL = 'ananasproject@gmail.com'