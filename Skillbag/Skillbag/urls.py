
# Django
from django.urls import include
from django.urls import path, re_path
from django.contrib import admin
from django.views.generic import TemplateView

# Local
from categories import views as category_views
from skills import views as skill_views
from nexus import views as nexus_views


urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('', TemplateView.as_view(template_name='nexus/page-nexus.html'), name='home'),
    re_path(r'^bag/(?P<slug>.+)', nexus_views.NexusWithArgumentsView.as_view(), name='smth'),
    path('403/', TemplateView.as_view(template_name='403.html'), name='403'),
    path('404/', TemplateView.as_view(template_name='404.html'), name='404'),
    path('500/', TemplateView.as_view(template_name='500.html'), name='500'),
     
    path('accounts/', include('accounts.urls')),
    path('categories/', include('categories.urls')),
    path('skills/', include('skills.urls')),
    path('statistics/', include('stats.urls')),
    path('search/', include('search.urls')),
]