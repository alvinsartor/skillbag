# Python
import random

# Django
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

# 3rd Party
import factory

# Local
from skills.models import Skill, Subskill, Achievement
from skills.factories import AchievementFactory, SkillFactory, SubskillFactory
from accounts.models import User, Team, TeamMember
from accounts.factories import UserFactory, TeamFactory, TeamMemberFactory
from categories.models import Category, Macrocategory
from categories.factories import CategoryFactory, MacrocategoryFactory

class Command(BaseCommand):
    help = 'Cleans the database and creates some test data'

    def get_randomized_list_for_iteration(self, model):
        return list(model.objects.all().order_by('?')) + list(model.objects.all().order_by('?')) + list(model.objects.all().order_by('?'))

    def print_recap(self):
        self.stdout.write("\nRecap:")
        self.stdout.write("Number of users:%s" % User.objects.all().count())
        self.stdout.write("Number of teams:%s" % Team.objects.all().count())
        self.stdout.write("Number of team members:%s" % TeamMember.objects.all().count())
        self.stdout.write("Number of macrocategories:%s" % Macrocategory.objects.all().count())
        self.stdout.write("Number of categories:%s" % Category.objects.all().count())
        self.stdout.write("Number of skills:%s" % Skill.objects.all().count())
        self.stdout.write("Number of subskills:%s" % Subskill.objects.all().count())
        self.stdout.write("Number of achievements:%s" % Achievement.objects.all().count())

    def handle(self, *args, **options):
        if not settings.DEVELOPMENT:
            raise CommandError('Cannot create fake data on this database.')
        
        self.stdout.write("Cleaning existing database...")
        User.objects.exclude(is_superuser=True).delete()
        Team.objects.all().delete()
        Macrocategory.objects.all().delete()

        self.stdout.write("Adding some users...")
        UserFactory.create_batch(10)       

        self.stdout.write("Adding a few macrocategories...")
        macrocategories = MacrocategoryFactory.create_batch(2)

        self.stdout.write("Adding a few categories...")
        CategoryFactory.create_batch(5, macrocategory=factory.Iterator(macrocategories))
        randomized_categories = self.get_randomized_list_for_iteration(Category)

        self.stdout.write("Adding a bunch of skills...")
        skills = SkillFactory.create_batch(15, category=factory.Iterator(randomized_categories))
        randomized_skills = self.get_randomized_list_for_iteration(Skill)

        self.stdout.write("Adding a random number of subskills to each skill...")
        for skill in skills:
            num_subskills = random.randint(2, 15)
            SubskillFactory.create_batch(num_subskills, skill=skill)        
        
        self.stdout.write("Adding a bunch of achievements...")
        for user in User.objects.all():
            subskills = Subskill.objects.all().order_by('?')
            AchievementFactory.create_batch(10, user=user, subskill=factory.Iterator(subskills))               

        self.stdout.write("Adding a few Teams...")
        nr_teams = 3
        for _ in range(nr_teams):
            random_skills = Skill.objects.order_by('?')[:4]
            TeamFactory(skills=random_skills)

        self.stdout.write("Adding members to teams...")
        teams = Team.objects.all()
        for team in teams:
            nr_users = 3
            random_users = User.objects.all().order_by('?')[:nr_users]
            TeamMemberFactory.create_batch(nr_users, team=team, user=factory.Iterator(random_users))

        self.print_recap()
        self.stdout.write(self.style.SUCCESS('\nSuccessfully created fake data'))