
from django.test import TestCase

from accounts.factories import UserFactory

class NexusWithArgumentsViewTest(TestCase):

    def setUp(self):
        user = UserFactory()
        login_result = self.client.login(email=user.email, password="123456789")
        self.assertTrue(login_result)

    def test_logged_out_user_is_always_redirected_to_home(self):
        self.client.logout()
        response = self.client.get('/bag/PageSomething')
        self.assertRedirects(response, '/accounts/login/?next=/bag/PageSomething')

    def test_url_with_page_without_argument_is_correctly_redirected(self):
        response = self.client.get('/bag/PageSomething')
        self.assertRedirects(response, '/?page=PageSomething')

    def test_url_with_page_with_arguments_is_correctly_redirected(self):
        response = self.client.get('/bag/PageSomething/1')
        self.assertRedirects(response, '/?page=PageSomething&args=1')

        response = self.client.get('/bag/PageSomething/arg1/arg2')
        self.assertRedirects(response, '/?page=PageSomething&args=arg1/arg2')
    
    def test_url_without_page_redirects_to_404(self):
        response = self.client.get('/bag/SomethingButNotAPage/arg1/arg2')
        self.assertRedirects(response, '/404/')
