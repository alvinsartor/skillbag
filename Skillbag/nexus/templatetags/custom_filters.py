from django.template.defaulttags import register
from django import template

register = template.Library()

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.filter
def get_item_or_none(dictionary, key):
    return dictionary.get(key, 'none')

@register.filter
def get_item_or_empty_list(dictionary, key):
    return dictionary.get(key, [])

@register.filter
def get_description_or_variable_summary(skill, sentences=3):
    sentences = sentences if sentences > 0 and sentences < 10 else 3
    return skill.get_description_or_summary(sentences)

@register.filter
def subtract(value, arg):
    return value - arg
