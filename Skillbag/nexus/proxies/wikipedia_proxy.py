
# Python
import re

# 3rd Party
import wikipedia

class PageParsingError(Exception):
    pass

class wikipediaProxy(object):
    
    @staticmethod
    def page_name_from_url(url):
        """ Given a Wikipedia url, it extracts the page name. """
        page = re.search("wiki\/(.+?)(?=\?|$)", url)
        if not page:
            raise PageParsingError()
        return page.group(1).replace("_", " ")

    @staticmethod
    def summary_from_url(url, sentences=3):
        """ Given a Wikipedia url, it extracts the first sentences (default: 3) of the summary. """
        page_name = wikipediaProxy.page_name_from_url(url)
        return wikipedia.summary(page_name, sentences=sentences, auto_suggest=False)
        
    
    @staticmethod
    def guess_wikipedia_url_from_name(name):
        """ Given a name, it gives back the (max 3) most probable wikipedia pages. """
        return wikipedia.search(name, results=3)
