/**
 * Object controlling a page.
 * It will take care of fetching the content, displaying the data and disposing of it.
 */
abstract class Page implements IDisposable {

    private static loader: HTMLDivElement;
    private getLoader(): HTMLDivElement {
        if (!Page.loader) Page.loader = document.getElementById("page-loader") as HTMLDivElement;
        return Page.loader;
    }

    /**
     * Sets the percentage of the loader. Used to show the user that work is being done in the background.
     * The loader fades out when sets to 100.
     * @param percentage the completion percentage.
     */
    protected setLoaderPercentage(percentage: number): void {
        const loader = this.getLoader();
        loader.style.width = `${percentage}vw`;
        loader.style.animationName = percentage === 100 ? "fadeOut-animation" : "none";
    }

    /**
     * Function called to initialize and load the page.
     * @param args The arguments necessary to load this page.
     */
    public abstract async initialize(...args: any[]): Promise<void>;

    /** Executes all the operations to load the current page. Called in the initialize function. */
    protected async loadPage(): Promise<void> {
        this.setLoaderPercentage(0);

        this.resetTitle();
        ActionsLoader.instance().destroyCurrentMenu();
        this.setLoaderPercentage(20);

        const html = await this.loadHtmlAndHandleErrors();
        (document.getElementById("dynamic-content-container") as HTMLElement).innerHTML = html;
        this.setLoaderPercentage(40);

        this.setTitleFromHtml();
        ActionsLoader.instance().reload();
        this.setLoaderPercentage(60);

        await this.loadScripts();
        this.setLoaderPercentage(80);

        await this.initializeScripts();
        this.setLoaderPercentage(100);
    }

    private async loadHtmlAndHandleErrors(): Promise<string> {
        try {
            return await this.loadHtml();
        } catch (e) {
            switch (e.status) {
            case 403:
                location.href = "/403/";
                break;
            case 404:
                location.href = "/404/";
                break;
            case 500:
                location.href = "/500/";
                break;
            default:
                throw Error("Something strange happened while loading a page..");
            }
            return "";
        }
    }

    private async loadScripts(): Promise<void> {
        const scriptsContainer = (document.getElementById("scripts") as HTMLElement);
        if (!scriptsContainer) return;

        const scriptBatches = scriptsContainer.innerText.split("#");
        for (let i = 0; i < scriptBatches.length; i++) {
            await this.loadBatchOfScripts(scriptBatches[i]);
        }
    }

    private async loadBatchOfScripts(scriptBatch: string): Promise<void> {
        const scriptPaths = scriptBatch.split("\n").map(s => s.trim()).filter(s => s !== "");
        await Promise.all(scriptPaths.map(script => this.loadScriptIfNeeded(script)));
    }

    private async loadScriptIfNeeded(script: string): Promise<void> {
        if (SiteManager.instance().hasAlreadyLoadedScript(script)) return;

        await this.getScript(script);
        SiteManager.instance().addLoadedScript(script);
    }

    private getScript(url: string) {
        return new Promise((resolve, reject) => {
            jQuery.getScript({ url: url, cache: true })
                .done(script => { resolve(script); });
        });
    };

    /**
     * Attaches listeners to the html page or does other operations
     * that should happen after everything else is loaded.
     */
    protected abstract async initializeScripts(): Promise<void>;

    /** Loads the data to display on the page */
    protected abstract async loadHtml(): Promise<string>;

    /** @inheritdoc  */
    public async dispose(): Promise<void> {
        (document.getElementById("dynamic-content-container") as HTMLElement).innerHTML = "";
        return await this.internalDispose();
    }

    /**
     * Function with the task of freeing up some references. Mainly used to help out the GC.
     * It can be left empty (simply returning 'Promise.resolve()').
     */
    protected abstract async internalDispose(): Promise<void>;

    private resetTitle(): void {
        const title = document.getElementById("page-title") as HTMLElement;
        title.innerHTML = "";
        title.style.opacity = "0";
    }

    private setTitleFromHtml(): void {
        const title = document.getElementById("page-title") as HTMLElement;
        const loadedTitle = document.getElementById("loaded-title") as HTMLInputElement;

        if (!loadedTitle || !loadedTitle.value) throw new Error("Page title could not be found");

        title.innerHTML = loadedTitle.value;
        title.style.opacity = "1";
    }
}