
/**
 * Class with the duty of holding references, disposing and instantiating
 * other objects necessary to run a page.
 */
class SiteManager {
    private static inst: SiteManager;

    private _currentPageName: string = "unset";
    private _currentPage: Page | undefined = undefined;
    private readonly _pages: { [name: string]: Page } = {
        "PageSearch": new PageSearch(),
        "PageAllSkills": new PageAllSkills(),
        "PageMySkills": new PageMySkills(),
        "PageCategorySkills": new PageCategorySkills(),
        "PageMacrocategories": new PageMacrocategories(),
        "PageCategories": new PageCategories(),
        "PageStatisticsHub": new PageStatisticsHub(),
        "PagePersonalStats": new PagePersonalStats(),
        "PageTeamSelection": new PageTeamSelection(),
        "PageTeamSkills": new PageTeamSkills(),
        "PageTeamAdmin": new PageTeamAdmin(),
        "PageTeamDashboard": new PageTeamDashboard(),
        "PageChangePassword": new PageChangePassword(),
        "PageEmailPreferences": new PageEmailPreferences(),
    };

    private readonly _home: string = "PageSearch";
    private readonly _loadedScripts: Set<string>;

    private constructor() {
        this._loadedScripts = new Set();

        $(window).on("popstate", () => { this.loadFromUrl(); });
        this.loadPageFromUrlQueryOrDefault();
    }

    private loadPageFromUrlQueryOrDefault(): void {
        const urlParams = new URLSearchParams(window.location.search);
        const page = urlParams.get("page");
        const args = urlParams.get("args") ? urlParams.get("args")!.split("/") : null;

        if (page) {
            args ? this.load(page, ...args) : this.load(page);
            return;
        }

        this.load(this._home);
    }

    /** Retrieves the instance of this singleton SiteManager class. */
    public static instance(): SiteManager {
        if (!this.inst) {
            this.inst = new SiteManager();
        }
        return this.inst;
    }

    /** Adds a new entry to the list of already loaded scripts. */
    public readonly addLoadedScript = (script: string) => { this._loadedScripts.add(script); };

    /** Checks whether the scripts has already been loaded. */
    public readonly hasAlreadyLoadedScript = (script: string) => this._loadedScripts.has(script);

    /**
     * Dynamically loads the specified page.
     * Note: the given string must be a key of the pages contained in this class.
     * @param page the page to load in a dynamic way.
     */
    public async load(page: string, ...args: any[]): Promise<void> {
        if (!(page in this._pages)) {
            location.href = "/404/";
        }

        if (this._currentPageName !== page) {
            if (this._currentPage) await this._currentPage.dispose();
            this._currentPage = this._pages[page];
            await this._currentPage.initialize(...args);
            this._currentPageName = page;
            this.pushState(page, ...args);
        }
    }

    /** Reloads the current url without adding it to the history. */
    public async reload(): Promise<void> {
        await this.loadFromUrl();
    }

    /** Loads the page associated to the HOME variable. */
    public goHome(): void {
        this.load(this._home);
    }

    private pushState(page: string, ...args: any[]): void {
        history.pushState(null, page, `/bag/${page}/${args.join("/")}`);
    }

    private async loadFromUrl(): Promise<void> {
        const urlParams = location.pathname.replace("/bag/", "");
        const [page, ...args] = urlParams.split("/");
        if (!(page in this._pages)) {
            location.href = "/404/";
        }

        const decodedArgs = args.map(a => decodeURIComponent(a));

        if (this._currentPage) await this._currentPage.dispose();
        this._currentPage = this._pages[page];
        await this._currentPage.initialize(...decodedArgs);
        this._currentPageName = page;
    }

    /** Indicates whether the user is authenticated. As this value can be faked, do not rely on it for security issues. */
    public isUserAuthenticated(): boolean {
        const userId = document.getElementById("user") as HTMLElement;
        if (userId) return true;

        return false;
    }

    /** Redirects the user to the login screen. */
    public static toLoginScreen(): void {
        location.href = "/accounts/login/";
    }
}