"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
/**
 * Object controlling a page.
 * It will take care of fetching the content, displaying the data and disposing of it.
 */
class Page {
    getLoader() {
        if (!Page.loader)
            Page.loader = document.getElementById("page-loader");
        return Page.loader;
    }
    /**
     * Sets the percentage of the loader. Used to show the user that work is being done in the background.
     * The loader fades out when sets to 100.
     * @param percentage the completion percentage.
     */
    setLoaderPercentage(percentage) {
        const loader = this.getLoader();
        loader.style.width = `${percentage}vw`;
        loader.style.animationName = percentage === 100 ? "fadeOut-animation" : "none";
    }
    /** Executes all the operations to load the current page. Called in the initialize function. */
    loadPage() {
        return __awaiter(this, void 0, void 0, function* () {
            this.setLoaderPercentage(0);
            this.resetTitle();
            ActionsLoader.instance().destroyCurrentMenu();
            this.setLoaderPercentage(20);
            const html = yield this.loadHtmlAndHandleErrors();
            document.getElementById("dynamic-content-container").innerHTML = html;
            this.setLoaderPercentage(40);
            this.setTitleFromHtml();
            ActionsLoader.instance().reload();
            this.setLoaderPercentage(60);
            yield this.loadScripts();
            this.setLoaderPercentage(80);
            yield this.initializeScripts();
            this.setLoaderPercentage(100);
        });
    }
    loadHtmlAndHandleErrors() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.loadHtml();
            }
            catch (e) {
                switch (e.status) {
                    case 403:
                        location.href = "/403/";
                        break;
                    case 404:
                        location.href = "/404/";
                        break;
                    case 500:
                        location.href = "/500/";
                        break;
                    default:
                        throw Error("Something strange happened while loading a page..");
                }
                return "";
            }
        });
    }
    loadScripts() {
        return __awaiter(this, void 0, void 0, function* () {
            const scriptsContainer = document.getElementById("scripts");
            if (!scriptsContainer)
                return;
            const scriptBatches = scriptsContainer.innerText.split("#");
            for (let i = 0; i < scriptBatches.length; i++) {
                yield this.loadBatchOfScripts(scriptBatches[i]);
            }
        });
    }
    loadBatchOfScripts(scriptBatch) {
        return __awaiter(this, void 0, void 0, function* () {
            const scriptPaths = scriptBatch.split("\n").map(s => s.trim()).filter(s => s !== "");
            yield Promise.all(scriptPaths.map(script => this.loadScriptIfNeeded(script)));
        });
    }
    loadScriptIfNeeded(script) {
        return __awaiter(this, void 0, void 0, function* () {
            if (SiteManager.instance().hasAlreadyLoadedScript(script))
                return;
            yield this.getScript(script);
            SiteManager.instance().addLoadedScript(script);
        });
    }
    getScript(url) {
        return new Promise((resolve, reject) => {
            jQuery.getScript({ url: url, cache: true })
                .done(script => { resolve(script); });
        });
    }
    ;
    /** @inheritdoc  */
    dispose() {
        return __awaiter(this, void 0, void 0, function* () {
            document.getElementById("dynamic-content-container").innerHTML = "";
            return yield this.internalDispose();
        });
    }
    resetTitle() {
        const title = document.getElementById("page-title");
        title.innerHTML = "";
        title.style.opacity = "0";
    }
    setTitleFromHtml() {
        const title = document.getElementById("page-title");
        const loadedTitle = document.getElementById("loaded-title");
        if (!loadedTitle || !loadedTitle.value)
            throw new Error("Page title could not be found");
        title.innerHTML = loadedTitle.value;
        title.style.opacity = "1";
    }
}
