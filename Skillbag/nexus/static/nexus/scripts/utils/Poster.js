"use strict";
class Poster {
    /** Fetches the CRSF token (cached after first hit). */
    static getCrsf() {
        if (Poster.crsf === null) {
            Poster.crsf = document.querySelector("[name=csrfmiddlewaretoken]").value;
        }
        return Poster.crsf;
    }
    /** Fetches the current user id (cached after first hit). */
    static getUserId() {
        if (Poster.userIdentifier === null) {
            Poster.userIdentifier = document.getElementById("user").innerText;
        }
        return Poster.userIdentifier;
    }
    /** Returns an object containing the user id and the crsf token (both cached after first hit). */
    static getBasicPostData() {
        return {
            user: Poster.getUserId(),
            csrfmiddlewaretoken: Poster.getCrsf()
        };
    }
}
Poster.crsf = null;
Poster.userIdentifier = null;
