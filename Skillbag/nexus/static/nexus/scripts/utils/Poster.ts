interface IBasicPostData {
    user: string;
    csrfmiddlewaretoken: string;
}

class Poster {
    private static crsf: string | null = null;
    private static userIdentifier: string | null = null;

    /** Fetches the CRSF token (cached after first hit). */
    public static getCrsf(): string {
        if (Poster.crsf === null) {
            Poster.crsf = (document.querySelector("[name=csrfmiddlewaretoken]") as HTMLInputElement).value;
        }
        return Poster.crsf;
    }

    /** Fetches the current user id (cached after first hit). */
    public static getUserId(): string {
        if (Poster.userIdentifier === null) {
            Poster.userIdentifier = document.getElementById("user")!.innerText;
        }
        return Poster.userIdentifier;
    }

    /** Returns an object containing the user id and the crsf token (both cached after first hit). */
    public static getBasicPostData(): IBasicPostData {
        return {
            user: Poster.getUserId(),
            csrfmiddlewaretoken: Poster.getCrsf()
        };
    }
}