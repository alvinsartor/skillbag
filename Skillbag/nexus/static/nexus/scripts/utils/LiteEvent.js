"use strict";
class LiteEvent {
    constructor() {
        this._handlers = [];
    }
    /** @inheritDoc */
    subscribe(handler) {
        this._handlers.push(handler);
    }
    /** @inheritDoc */
    unsubscribe(handler) {
        this._handlers = this._handlers.filter(h => h !== handler);
    }
    /** @inheritDoc */
    unsubscribeAll() { this._handlers = []; }
    /**
     * Executes the handlers that subscribed to the event.
     * @param data the data that will be passed to the handlers as argument.
     */
    trigger(data) {
        this._handlers.slice(0).forEach(h => h(data));
    }
    /** Exposes the accessible methods of LiteEvent (so the receiver cannot execute). */
    expose() { return this; }
}
