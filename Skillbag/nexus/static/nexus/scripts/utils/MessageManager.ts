
class MessageManager {

    private static messageContainer: HTMLDivElement | null = null;

    private static getContainer(): HTMLDivElement {
        if (MessageManager.messageContainer === null) {
            MessageManager.messageContainer =
                document.getElementsByClassName("messages-container")[0] as HTMLDivElement;
        }
        return MessageManager.messageContainer;
    }

    /**
     * Spawn a success message. It will automatically disappear after a set timeout.
     * @param text the message text.
     */
    public static success(text: string): void {
        MessageManager.createMessage(text, MessageType.Success);
    }

    /**
    * Spawn a warning message. It will automatically disappear after a set timeout.
    * @param text the message text.
    */
    public static warning(text: string): void {
        MessageManager.createMessage(text, MessageType.Warning);
    }

    /**
    * Spawn an error message. It will automatically disappear after a set timeout.
    * @param text the message text.
    */
    public static error(text: string): void {
        MessageManager.createMessage(text, MessageType.Error);
    }

    /**
    * Spawn an info message. It will automatically disappear after a set timeout.
    * @param text the message text.
    */
    public static info(text: string): void
    {
        MessageManager.createMessage(text, MessageType.Info);
    }

    private static createMessage(text: string, type: MessageType) {
        const message = new Message(text, type, MessageManager.getContainer());
        setTimeout(() => message.destroy(), 6050);
    }
}

enum MessageType { Success, Warning, Error, Info }

class Message {

    private readonly messageTile: HTMLDivElement;
    private readonly container: HTMLDivElement;

    /**
     * Creates a new instance of Message.
     * @param text the message text.
     * @param type the message type.
     */
    constructor(text: string, type: MessageType, container: HTMLDivElement) {
        const template = (document.getElementById("message-template") as HTMLTemplateElement)
            .content.querySelector("div[class='message']");

        this.container = container;
        this.messageTile = document.importNode<any>(template, true);
        this.messageTile.classList.add(this.messageClass(type));
        (this.messageTile.querySelector("span") as HTMLSpanElement).textContent = text;
        this.container.appendChild(this.messageTile);
    }

    /**
     * Remove the message from the container.
     */
    public destroy() {
        this.container.removeChild(this.messageTile);
    }

    private messageClass(type: MessageType): string {
        switch (type) {
        case MessageType.Success:
            return "message-success";
        case MessageType.Warning:
            return "message-warning";
        case MessageType.Error:
            return "message-error";
        case MessageType.Info:
            return "message-info";
        default:
            throw new Error(`Unknown message type: '${type}'`);
        }
    }

}