"use strict";
class ActionsLoader {
    constructor() {
        this._actionMenu = null;
        this._actionsContainer = document.getElementById("action-button");
        this._actionsButton = this._actionsContainer.querySelector(".page-actions__button");
        document.addEventListener("click", () => { if (this._actionMenu)
            this._actionMenu.style.display = "none"; });
    }
    static instance() {
        if (!this.inst)
            this.inst = new ActionsLoader();
        return this.inst;
    }
    /** Replaces the entries in the action menu with the actions found in the current page. */
    reload() {
        if (this._actionMenu)
            this.destroyCurrentMenu();
        const actions = this.extractActionsFromHtml();
        if (actions.length === 0)
            return;
        this.createMenu(actions);
        this._actionsButton.onclick = (event) => {
            event.stopPropagation();
            this._actionMenu.style.display = "block";
        };
        this.attachGoToActions(actions);
    }
    /** Removes the currently active action-menu. */
    destroyCurrentMenu() {
        if (!this._actionMenu)
            return;
        this._actionsContainer.style.display = "none";
        this._actionsButton.onclick = null;
        this._actionMenu.remove();
        this._actionMenu = null;
    }
    extractActionsFromHtml() {
        return Array
            .from(document.querySelectorAll(".js-action"))
            .map(htmlAction => htmlAction)
            .map(htmlAction => {
            return {
                id: htmlAction.dataset.actionId,
                text: htmlAction.dataset.actionText,
                html: htmlAction.dataset.actionIcon,
                args: htmlAction.dataset.actionArgs,
                fullClass: htmlAction.classList.value,
            };
        });
    }
    createMenu(actions) {
        if (this._actionMenu)
            throw Error("Trying to recreate already-initialized action menu.");
        this._actionsContainer.style.display = "flex";
        this._actionMenu = document.createElement("div");
        this._actionMenu.classList.add("page-actions__menu");
        this._actionsContainer.appendChild(this._actionMenu);
        actions.map(this.createEntry)
            .forEach(entry => this._actionMenu.appendChild(entry));
    }
    createEntry(action) {
        const entry = document.createElement("div");
        entry.classList.add("page-actions__menu-entry");
        entry.id = action.id;
        entry.innerHTML = `${action.html}\n<span>${action.text}</span>`;
        entry.dataset.actionArgs = action.args;
        return entry;
    }
    /** Attach the behavior of the actions that only trigger a page load */
    attachGoToActions(actions) {
        actions
            .filter(action => action.fullClass.includes("js-goto-action"))
            .forEach(action => {
            if (!action.args)
                throw Error("GoTo actions need the page in the arguments.");
            const args = action.args.split("|");
            if (!args[0].startsWith("Page"))
                throw Error(`'${args[0]}' does not seem a valid page.`);
            const page = args[0];
            const pageArgs = args.slice(1);
            const control = document.getElementById(action.id);
            if (!control)
                throw Error("It seems that the action has not been initialized.");
            control.onclick = () => SiteManager.instance().load(page, pageArgs);
        });
    }
}
ActionsLoader.inst = null;
