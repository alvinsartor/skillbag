"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
/**
 * Class with the duty of holding references, disposing and instantiating
 * other objects necessary to run a page.
 */
class SiteManager {
    constructor() {
        this._currentPageName = "unset";
        this._currentPage = undefined;
        this._pages = {
            "PageSearch": new PageSearch(),
            "PageAllSkills": new PageAllSkills(),
            "PageMySkills": new PageMySkills(),
            "PageCategorySkills": new PageCategorySkills(),
            "PageMacrocategories": new PageMacrocategories(),
            "PageCategories": new PageCategories(),
            "PageStatisticsHub": new PageStatisticsHub(),
            "PagePersonalStats": new PagePersonalStats(),
            "PageTeamSelection": new PageTeamSelection(),
            "PageTeamSkills": new PageTeamSkills(),
            "PageTeamAdmin": new PageTeamAdmin(),
            "PageTeamDashboard": new PageTeamDashboard(),
            "PageChangePassword": new PageChangePassword(),
            "PageEmailPreferences": new PageEmailPreferences(),
        };
        this._home = "PageSearch";
        /** Adds a new entry to the list of already loaded scripts. */
        this.addLoadedScript = (script) => { this._loadedScripts.add(script); };
        /** Checks whether the scripts has already been loaded. */
        this.hasAlreadyLoadedScript = (script) => this._loadedScripts.has(script);
        this._loadedScripts = new Set();
        $(window).on("popstate", () => { this.loadFromUrl(); });
        this.loadPageFromUrlQueryOrDefault();
    }
    loadPageFromUrlQueryOrDefault() {
        const urlParams = new URLSearchParams(window.location.search);
        const page = urlParams.get("page");
        const args = urlParams.get("args") ? urlParams.get("args").split("/") : null;
        if (page) {
            args ? this.load(page, ...args) : this.load(page);
            return;
        }
        this.load(this._home);
    }
    /** Retrieves the instance of this singleton SiteManager class. */
    static instance() {
        if (!this.inst) {
            this.inst = new SiteManager();
        }
        return this.inst;
    }
    /**
     * Dynamically loads the specified page.
     * Note: the given string must be a key of the pages contained in this class.
     * @param page the page to load in a dynamic way.
     */
    load(page, ...args) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!(page in this._pages)) {
                location.href = "/404/";
            }
            if (this._currentPageName !== page) {
                if (this._currentPage)
                    yield this._currentPage.dispose();
                this._currentPage = this._pages[page];
                yield this._currentPage.initialize(...args);
                this._currentPageName = page;
                this.pushState(page, ...args);
            }
        });
    }
    /** Reloads the current url without adding it to the history. */
    reload() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.loadFromUrl();
        });
    }
    /** Loads the page associated to the HOME variable. */
    goHome() {
        this.load(this._home);
    }
    pushState(page, ...args) {
        history.pushState(null, page, `/bag/${page}/${args.join("/")}`);
    }
    loadFromUrl() {
        return __awaiter(this, void 0, void 0, function* () {
            const urlParams = location.pathname.replace("/bag/", "");
            const [page, ...args] = urlParams.split("/");
            if (!(page in this._pages)) {
                location.href = "/404/";
            }
            const decodedArgs = args.map(a => decodeURIComponent(a));
            if (this._currentPage)
                yield this._currentPage.dispose();
            this._currentPage = this._pages[page];
            yield this._currentPage.initialize(...decodedArgs);
            this._currentPageName = page;
        });
    }
    /** Indicates whether the user is authenticated. As this value can be faked, do not rely on it for security issues. */
    isUserAuthenticated() {
        const userId = document.getElementById("user");
        if (userId)
            return true;
        return false;
    }
    /** Redirects the user to the login screen. */
    static toLoginScreen() {
        location.href = "/accounts/login/";
    }
}
