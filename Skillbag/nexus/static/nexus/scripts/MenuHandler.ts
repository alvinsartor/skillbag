﻿class MenuHandler {

    private readonly _menuButton: HTMLDivElement;
    private readonly _closeMenuButton: HTMLDivElement;
    private readonly _expander: HTMLDivElement;
    private readonly _menuContainer: HTMLDivElement;

    /** Creates a new instance of MenuHandler. */
    public constructor() {
        this._menuButton = document.getElementById("menu-button") as HTMLDivElement;
        this._closeMenuButton = document.getElementById("close-menu-button") as HTMLDivElement;
        this._expander = this._menuButton.querySelector(".menu-button-expander ") as HTMLDivElement;
        this._menuContainer = document.getElementById("menu-container") as HTMLDivElement;

        this._menuButton.onclick = async () => this.openMenu();
        this._closeMenuButton.onclick = async () => this._closeMenu();
        this.addListenersToMenuEntries();
    }

    private async openMenu(): Promise<void> {
        this._expander.classList.toggle("hidden", false);
        this._menuContainer.classList.toggle("hidden", false);

        this._expander.ontransitionend = () => {
            this._expander.ontransitionend = null;
            this._menuContainer.style.opacity = "1";
        }
        this._expander.style.transform = "scale(1)";
        this.addListenerForBackButton();
    }

    private _closeMenu = async () => {
        this._menuContainer.ontransitionend = () => {
            this._menuContainer.ontransitionend = null;
            this._expander.ontransitionend = () => {
                this._expander.ontransitionend = null;
                this._menuContainer.classList.toggle("hidden", true);
            }
            this._expander.style.transform = "scale(0)";
        }
        this._menuContainer.style.opacity = "0";
        this.removeListenerForBackButton();
    }

    private addListenersToMenuEntries(): void {
        Array.from(this._menuContainer.querySelectorAll(".side-menu__entry"))
            .map(e => e as HTMLElement)
            .filter(e => e.dataset.destination != null)
            .forEach(e => {
                e.onclick = () => {
                    SiteManager.instance().load(e.dataset.destination!);
                    this._closeMenu();
                };
            });

        const accountSettings = document.getElementById("account-settings-link");
        if (accountSettings) accountSettings.onclick = () =>
        {
            SiteManager.instance().load(accountSettings.dataset.destination!);
            this._closeMenu();
        }
    }

    private addListenerForBackButton() {
        document.addEventListener('backbutton', this._closeMenu);
        $(window).on("popstate", this._closeMenu);
    }

    private removeListenerForBackButton() {
        document.removeEventListener('backbutton', this._closeMenu);
        $(window).off("popstate", this._closeMenu);
    }
}