"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class MenuHandler {
    /** Creates a new instance of MenuHandler. */
    constructor() {
        this._closeMenu = () => __awaiter(this, void 0, void 0, function* () {
            this._menuContainer.ontransitionend = () => {
                this._menuContainer.ontransitionend = null;
                this._expander.ontransitionend = () => {
                    this._expander.ontransitionend = null;
                    this._menuContainer.classList.toggle("hidden", true);
                };
                this._expander.style.transform = "scale(0)";
            };
            this._menuContainer.style.opacity = "0";
            this.removeListenerForBackButton();
        });
        this._menuButton = document.getElementById("menu-button");
        this._closeMenuButton = document.getElementById("close-menu-button");
        this._expander = this._menuButton.querySelector(".menu-button-expander ");
        this._menuContainer = document.getElementById("menu-container");
        this._menuButton.onclick = () => __awaiter(this, void 0, void 0, function* () { return this.openMenu(); });
        this._closeMenuButton.onclick = () => __awaiter(this, void 0, void 0, function* () { return this._closeMenu(); });
        this.addListenersToMenuEntries();
    }
    openMenu() {
        return __awaiter(this, void 0, void 0, function* () {
            this._expander.classList.toggle("hidden", false);
            this._menuContainer.classList.toggle("hidden", false);
            this._expander.ontransitionend = () => {
                this._expander.ontransitionend = null;
                this._menuContainer.style.opacity = "1";
            };
            this._expander.style.transform = "scale(1)";
            this.addListenerForBackButton();
        });
    }
    addListenersToMenuEntries() {
        Array.from(this._menuContainer.querySelectorAll(".side-menu__entry"))
            .map(e => e)
            .filter(e => e.dataset.destination != null)
            .forEach(e => {
            e.onclick = () => {
                SiteManager.instance().load(e.dataset.destination);
                this._closeMenu();
            };
        });
        const accountSettings = document.getElementById("account-settings-link");
        if (accountSettings)
            accountSettings.onclick = () => {
                SiteManager.instance().load(accountSettings.dataset.destination);
                this._closeMenu();
            };
    }
    addListenerForBackButton() {
        document.addEventListener('backbutton', this._closeMenu);
        $(window).on("popstate", this._closeMenu);
    }
    removeListenerForBackButton() {
        document.removeEventListener('backbutton', this._closeMenu);
        $(window).off("popstate", this._closeMenu);
    }
}
