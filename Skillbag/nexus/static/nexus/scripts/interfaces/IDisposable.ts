/**
 * Interface implemented by object which content should be manually disposed.
 */
interface IDisposable
{
    /** Removes the reference of contained objects and cleans the HTML from controlled HTMLElements. */
    dispose(): Promise<void>;
}