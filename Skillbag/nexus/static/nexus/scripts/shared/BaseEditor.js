"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class BaseEditor {
    constructor() {
        this._editorForm = null;
        this._editDialog = document.getElementById("editor-container");
        this._dialogContent = document.querySelector(".editor-container__content");
        const innerDialog = document.querySelector(".editor-container__inner-dialog");
        innerDialog.onclick = (e) => e.stopPropagation();
        innerDialog.onmousedown = (e) => e.stopPropagation();
        innerDialog.ontouchstart = (e) => e.stopPropagation();
        this._editorTitle = innerDialog.querySelector(".editor-container__editor-title");
        this._outerDialog = document.querySelector(".editor-container__outer-dialog");
        this._outerDialog.onmousedown = () => this.endEditingMode();
        this._outerDialog.ontouchstart = () => this.endEditingMode();
        this._confirmButton = this._editDialog.querySelector(".editor-container__confirm-button");
        this._confirmButton.onclick = () => this.confirmEdits();
        this._backButton = this._editDialog.querySelector(".editor-container__back-button");
        this._backButton.onclick = () => this.endEditingMode();
    }
    /** Loads the form and displays it on the container. */
    loadEditorForm() {
        this._editorTitle.innerText = this.title;
        this.startEditingMode();
    }
    startEditingMode() {
        return __awaiter(this, void 0, void 0, function* () {
            this._dialogContent.innerHTML = yield $.get(this.formUrl);
            this._editDialog.style.display = "block";
            this.setFormListeners();
        });
    }
    setFormListeners() {
        this._editorForm = this._dialogContent.querySelector("form");
        this._editorForm.onsubmit = (e) => {
            e.preventDefault();
            e.stopPropagation();
            this.submitForm();
        };
        this.setAdditionalListeners(this._editorForm);
    }
    /**
     * Sets additional listeners that the form might require.
     * To inherit if needed.
     */
    setAdditionalListeners(form) { }
    endEditingMode() {
        this._editDialog.style.display = "none";
    }
    confirmEdits() {
        if (!this._editorForm)
            return;
        this.submitForm();
    }
    submitForm() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this._editorForm)
                return;
            try {
                const result = yield $.ajax({
                    url: this.formUrl,
                    type: "POST",
                    data: $(this._editorForm).serialize(),
                });
                if (result.length && result.length > 0) {
                    // something went wrong, the form has been sent back.
                    this._dialogContent.innerHTML = result;
                    this.setFormListeners();
                }
                else {
                    MessageManager.success("Successfully edited");
                    SiteManager.instance().reload();
                }
            }
            catch (e) {
                MessageManager.error(`Error during form submit with the following status: ${e.statusText}`);
                console.log(e.responseText);
            }
        });
    }
}
