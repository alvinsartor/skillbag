
abstract class BaseEditor {

    private readonly _editDialog: HTMLDivElement;
    private readonly _outerDialog: HTMLDivElement;
    private readonly _dialogContent: HTMLDivElement;
    private readonly _editorTitle: HTMLDivElement;
    private readonly _confirmButton: HTMLElement;
    private readonly _backButton: HTMLElement;
    private _editorForm: HTMLFormElement | null = null;

    /** The title that will be shown on top of the editor. */
    protected abstract title: string;

    /** The url used to get and post this form. */
    protected abstract get formUrl(): string;

    protected constructor() {
        this._editDialog = document.getElementById("editor-container") as HTMLDivElement;
        this._dialogContent = document.querySelector(".editor-container__content") as HTMLDivElement;

        const innerDialog = document.querySelector(".editor-container__inner-dialog") as HTMLDivElement;
        innerDialog.onclick = (e) => e.stopPropagation();
        innerDialog.onmousedown = (e) => e.stopPropagation();
        innerDialog.ontouchstart = (e) => e.stopPropagation();

        this._editorTitle = innerDialog.querySelector(".editor-container__editor-title") as HTMLDivElement;

        this._outerDialog = document.querySelector(".editor-container__outer-dialog") as HTMLDivElement;
        this._outerDialog.onmousedown = () => this.endEditingMode();
        this._outerDialog.ontouchstart = () => this.endEditingMode();

        this._confirmButton = this._editDialog.querySelector(".editor-container__confirm-button") as HTMLElement;
        this._confirmButton.onclick = () => this.confirmEdits();

        this._backButton = this._editDialog.querySelector(".editor-container__back-button") as HTMLElement;
        this._backButton.onclick = () => this.endEditingMode();
    }

    /** Loads the form and displays it on the container. */
    public loadEditorForm(): void {
        this._editorTitle.innerText = this.title;
        this.startEditingMode();
    }

    private async startEditingMode(): Promise<void> {
        this._dialogContent.innerHTML = await $.get(this.formUrl);
        this._editDialog.style.display = "block";
        this.setFormListeners();
    }

    private setFormListeners(): void {
        this._editorForm = this._dialogContent.querySelector("form") as HTMLFormElement;
        this._editorForm.onsubmit = (e) => {
            e.preventDefault();
            e.stopPropagation();
            this.submitForm();
        };
        this.setAdditionalListeners(this._editorForm);
    }

    /**
     * Sets additional listeners that the form might require.
     * To inherit if needed.
     */
    protected setAdditionalListeners(form: HTMLFormElement): void {}

    private endEditingMode(): void {
        this._editDialog.style.display = "none";
    }

    private confirmEdits(): void {
        if (!this._editorForm) return;
        this.submitForm();
    }

    private async submitForm(): Promise<void> {
        if (!this._editorForm) return;

        try {
            const result = await $.ajax({
                url: this.formUrl,
                type: "POST",
                data: $(this._editorForm).serialize(),
            });

            if (result.length && result.length > 0) {
                // something went wrong, the form has been sent back.
                this._dialogContent.innerHTML = result;
                this.setFormListeners();
            } else {
                MessageManager.success("Successfully edited");
                SiteManager.instance().reload();
            }
        } catch (e) {
            MessageManager.error(`Error during form submit with the following status: ${e.statusText}`);
            console.log(e.responseText);
        }
    }
}