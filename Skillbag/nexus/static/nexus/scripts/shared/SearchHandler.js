"use strict";
class SearchHandler {
    /**
     * Creates a new instance of SearchHandler.
     * @param millisecondsDelay the delay between the input changing and the event being fired.
     * Useful whenever the event triggers heavy computations and it is wise not to fire too many requests.
     */
    constructor(millisecondsDelay = 100) {
        this._onSearchTextChanged = new LiteEvent();
        const searchBar = document.querySelector(".search-bar");
        this._searchBox = searchBar.querySelector("input");
        this._searchIcon = searchBar.querySelector(".search-bar__search-icon");
        this._cancelIcon = searchBar.querySelector(".search-bar__cancel-icon");
        this._millisecondsDelay = millisecondsDelay;
        this._searchBox.oninput = () => {
            this.delayedInput();
        };
        this._cancelIcon.onclick = () => {
            this._searchBox.value = "";
            this._onSearchTextChanged.trigger(this._searchBox.value);
            this.handleIcons();
        };
    }
    /** Event raised whenever the text in the search box is updated. */
    get searchTextChanged() { return this._onSearchTextChanged.expose(); }
    delayedInput() {
        if (this._timerId)
            clearTimeout(this._timerId);
        this._timerId = setTimeout(() => {
            this._onSearchTextChanged.trigger(this._searchBox.value);
            this.handleIcons();
        }, this._millisecondsDelay);
    }
    handleIcons() {
        const hasSearchText = this._searchBox.value !== "";
        const isSearchIconShown = this._searchIcon.style.zIndex === "1";
        if (hasSearchText && isSearchIconShown) {
            this._searchIcon.style.zIndex = "-1";
            this._searchIcon.style.transform = "scale(0)";
            this._cancelIcon.style.zIndex = "1";
            this._cancelIcon.style.transform = "scale(1)";
        }
        else if (!hasSearchText && !isSearchIconShown) {
            this._cancelIcon.style.zIndex = "-1";
            this._cancelIcon.style.transform = "scale(0)";
            this._searchIcon.style.zIndex = "1";
            this._searchIcon.style.transform = "scale(1)";
        }
    }
    /** Focuses the cursor to the search box. */
    focus() {
        this._searchBox.focus();
    }
}
