
class SearchHandler {

    private readonly _searchBox: HTMLInputElement;
    private readonly _searchIcon: HTMLElement;
    private readonly _cancelIcon: HTMLElement;

    private readonly _onSearchTextChanged = new LiteEvent<string>();
    private readonly _millisecondsDelay: number;

    /** Event raised whenever the text in the search box is updated. */
    public get searchTextChanged(): ILiteEvent<string> { return this._onSearchTextChanged.expose(); }

    /**
     * Creates a new instance of SearchHandler.
     * @param millisecondsDelay the delay between the input changing and the event being fired.
     * Useful whenever the event triggers heavy computations and it is wise not to fire too many requests. 
     */
    constructor(millisecondsDelay: number = 100) {
        const searchBar = document.querySelector(".search-bar") as HTMLElement;
        this._searchBox = searchBar.querySelector("input") as HTMLInputElement;
        this._searchIcon = searchBar.querySelector(".search-bar__search-icon") as HTMLElement;
        this._cancelIcon = searchBar.querySelector(".search-bar__cancel-icon") as HTMLElement;
        this._millisecondsDelay = millisecondsDelay;

        this._searchBox.oninput = () => {
            this.delayedInput();
        };
        this._cancelIcon.onclick = () => {
            this._searchBox.value = "";
            this._onSearchTextChanged.trigger(this._searchBox.value);
            this.handleIcons();
        };
    }

    private _timerId: number | undefined;

    private delayedInput(): void {
        if (this._timerId) clearTimeout(this._timerId);

        this._timerId = setTimeout(() => {
                this._onSearchTextChanged.trigger(this._searchBox.value);
                this.handleIcons();
            },
            this._millisecondsDelay);
    }

    private handleIcons(): void {
        const hasSearchText = this._searchBox.value !== "";
        const isSearchIconShown = this._searchIcon.style.zIndex === "1";

        if (hasSearchText && isSearchIconShown) {
            this._searchIcon.style.zIndex = "-1";
            this._searchIcon.style.transform = "scale(0)";
            this._cancelIcon.style.zIndex = "1";
            this._cancelIcon.style.transform = "scale(1)";
        } else if (!hasSearchText && !isSearchIconShown) {
            this._cancelIcon.style.zIndex = "-1";
            this._cancelIcon.style.transform = "scale(0)";
            this._searchIcon.style.zIndex = "1";
            this._searchIcon.style.transform = "scale(1)";
        }
    }

    /** Focuses the cursor to the search box. */
    public focus(): void {
        this._searchBox.focus();
    }
}