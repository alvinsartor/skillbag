"use strict";
/**
 * Component used in those pages that are not handled by the SiteManager.
 *
 * Used to set the title, remove the spinner and so on.
 */
class SimplePageOverrider {
    /**
     * Sets the title for the current page
     * @param title The title that will be used.
     */
    static setTitle(title) {
        document.getElementById("page-title").innerText = title;
    }
}
