
# Python
import random
import datetime

# Django
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

# Local
from nexus.proxies import wikipediaProxy, PageParsingError 

class TimeStampMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class DescriptableMixin(models.Model):
    description = models.CharField(_("Description"), max_length=300, blank=True, null=True, default=None)
    wikipedia_link = models.CharField(_("Wikipedia Link"), max_length=200, blank=True, null=True, default=None)
    
    cached_wikipedia_summary = models.CharField(max_length=512, blank=True, null=True, default=None)
    cached_wikipedia_link = models.CharField(max_length=200, blank=True, null=True, default=None)
    wikipedia_cache_validity_date = models.DateTimeField(null=True, default=None)

    class Meta:
        abstract = True

    def get_description_or_summary(self, sentences=3):
        if self.description:
            return self.description
        if self.wikipedia_link:
            return self.get_description_from_wikipedia_or_cache(sentences=sentences)    
        return ""

    def get_description_from_wikipedia_or_cache(self, sentences=3):
        if self.cached_wikipedia_summary and timezone.now() < self.wikipedia_cache_validity_date and self.cached_wikipedia_link == self.wikipedia_link:
            return self.cached_wikipedia_summary
        else:
            try:
                wiki_summary = wikipediaProxy.summary_from_url(self.wikipedia_link, sentences=sentences)
                self.cached_wikipedia_summary = (wiki_summary[:497] + '..') if len(wiki_summary) > 500 else wiki_summary
                self.cached_wikipedia_link = self.wikipedia_link
                self.wikipedia_cache_validity_date = timezone.now() + datetime.timedelta(minutes=random.randint(43000, 47000))
                self.save()

                return self.cached_wikipedia_summary 
            except PageParsingError:
                return _("Error while parsing the Wikipedia page :(.\nIt seems the URL is not correct.")

        return ""