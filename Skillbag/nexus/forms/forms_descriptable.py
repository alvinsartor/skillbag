    
from django import forms
from django.forms import ModelForm
from django.utils.translation import gettext as _

class DescriptableForm(ModelForm): 

    class Meta:
        fields = ['name', 'wikipedia_link', 'description']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['required'] = True
        self.fields['name'].widget.attrs['class'] = "editor-form__text-field editor-form__text-field--title"
        self.fields['name'].widget.attrs['placeholder'] = _('Name')
        
        self.fields['wikipedia_link'].widget.attrs['required'] = False
        self.fields['wikipedia_link'].widget.attrs['class'] = "editor-form__text-field"
        self.fields['wikipedia_link'].widget.attrs['placeholder'] = _('Wikipedia Link')
        self.fields['wikipedia_link'].widget.attrs['maxlength'] = "200"
        self.fields['wikipedia_link'].help_text = _('Always fill this field if possible. In case there is no description, the Wikipedia article will be used.')
        
        self.fields['description'].widget = forms.Textarea()
        self.fields['description'].widget.attrs['required'] = False
        self.fields['description'].widget.attrs['class'] = "editor-form__long-text-field"
        self.fields['description'].widget.attrs['placeholder'] = _('Description')
        self.fields['description'].widget.attrs['maxlength'] = "300"
        self.fields['description'].widget.attrs['rows'] = "6"        
        self.fields['description'].help_text = _('Fill this field only if there is no Wikipedia link or you feel you can write a better description.') 
    
    def clean_wikipedia_link(self):
        link = self.cleaned_data.get('wikipedia_link', None)
        if link and "wikipedia.org/wiki" not in link:
            raise forms.ValidationError("Are you sure this is a Wikipedia link?")
        return link