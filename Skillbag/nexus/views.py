from django.views.generic.base import TemplateView
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

@method_decorator(login_required, name='dispatch')
class NexusWithArgumentsView(TemplateView):
    template_name = 'categories/categories.html'
    
    def get(self, request, *args, **kwargs):
        parameters = kwargs['slug']
        if not parameters:
            return redirect("/404/")
                
        parameters = parameters.split("/")
        page = parameters[0]
        arguments = parameters[1:]

        if not page.startswith("Page"):
            return redirect("/404/")
        
        query_params = "?page=" + page
        if arguments:
            query_params += "&args=" + "/".join(arguments)

        url = reverse("home") + query_params
        return redirect(url)